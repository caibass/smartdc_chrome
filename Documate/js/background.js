var appid;

chrome.app.runtime.onLaunched.addListener(function() {
	chrome.app.window.create('Main.html', {
		id: 'mainwin',
		frame: "none",	
		bounds: {
			width: 840,
			height: 660
		},
		minWidth: 840,
		minHeight: 660
	});
});

chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
	if(request.launch == true && request.senderName == "DocumateOcr")
	{	  
		if (appid =='mainwin')
		{
			chrome.app.window.get(appid).focus();
		}
	  	sendResponse();
	}	
});


