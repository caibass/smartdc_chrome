var targetPoints;

const removeCropBox = function() {
    $("#cropBox").remove();
}

const attachCropBox = function (imgWidth,imgHeight) { 
    
    var margin = {top: 150, right: 150, bottom: 150, left: 150},
    width = imgWidth - margin.left - margin.right,
    height = imgHeight - margin.top - margin.bottom;

    targetPoints = [[150, 150], [width+150, 150], [width+150, height+150], [150, height+150]];

    var svg = d3.select("#imageDisplayArea").append("svg")
        .attr("id", "cropBox")
        .attr("width", $("#displayCanvas").width())
        .attr("height", $("#displayCanvas").height())
        .attr("style", "position: abdsolute; top: "+$("#displayCanvas").offset().top+"px; left: "+$("#displayCanvas").offset().left+"px;")
        //.attr("width", width + margin.left + margin.right)
        //.attr("height", height + margin.top + margin.bottom)
        .append("g")
        //.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .attr("id","window_g");

    var line2 = d3.svg.line()
        .x(function(d){return d.x;})
        .y(function(d){return d.y;})
        .interpolate('linear-closed');

    svg.append('path')
        .attr({
          'id': 'croppath',
          'd': line2(MakeData(targetPoints)),
          'y': 0,
          'stroke': '#00F',
          'stroke-width': '1px',
          'fill': 'none'
        });

    function MakeData(tpt)
    {
        var data = [
            {x:tpt[0][0], y:tpt[0][1]},
            {x:tpt[1][0], y:tpt[1][1]},
            {x:tpt[2][0], y:tpt[2][1]},
            {x:tpt[3][0], y:tpt[3][1]}
        ]
        return data;
    }

    var handle = svg.selectAll(".handle")
        .data(targetPoints)
        .enter().append("circle")
        .attr("class", "handle")
        .attr("transform", function(d) { return "translate(" + d + ")"; })
        .attr("r", 7)
        .call(d3.behavior.drag()
            .origin(function(d) {return {x: d[0], y: d[1]}; })
            .on("drag", dragged));

    function dragged(d) {
        d3.event.x = Math.min(Math.max(d3.event.x, 0), $("#displayCanvas").width());
        d3.event.y = Math.min(Math.max(d3.event.y, 0), $("#displayCanvas").height());
        d3.select(this).attr("transform", "translate(" + (d[0] = d3.event.x) + "," + (d[1] = d3.event.y) + ")");
        svg.select('path')
        .attr({
          'd': line2(MakeData(targetPoints)),
          'y': 0,
          'stroke': '#00F',
          'stroke-width': '2px',
          'fill': 'none'
        });
    }
}