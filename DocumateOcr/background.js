var app = {};
app.id = {"main": '', "parent": ''};
// app.storage = (function () {
//   var objs = {};
//   window.setTimeout(function () {
//     chrome.storage.local.get(null, function (o) {objs = o});
//   }, 0);
//   /*  */
//   return {
//     "read": function (id) {return objs[id]},
//     "write": function (id, data) {
//       var tmp = {};
//       tmp[id] = data;
//       objs[id] = data;
//       chrome.storage.local.set(tmp, function () {});
//     }
//   }
// })();

app.UI = (function () {
  var r = {};
  chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.path === 'ui-to-background') {
      for (var id in r) {
        if (r[id] && (typeof r[id] === "function")) {
          if (request.method === id) r[id](request.data);
        }
      }
    }
  });
  /*  */
  return {
    "close": function () {chrome.windows.remove(app.id.main)},
    "create": function () {
      chrome.storage.local.get({"width": 600, "height": 600}, function (storage) {
        chrome.windows.getAll(function (data){
          //console.log(data);

          if ( data.length == 0 )
          {
            var width = storage.width;
            var height = storage.height;
            var url = chrome.runtime.getURL("index.html");
            var top = 20;
            var left = 20;
            chrome.windows.create({'url': url, 'type': 'popup', 'width': width, 'height': height, 'top': top, 'left': left}, function (w) {app.id.main = w.id});
          }
          else
          {
            chrome.windows.getCurrent(function (win) {
              //console.log(win);
              app.id.parent = win.id;
              var width = storage.width;
              var height = storage.height;
              var url = chrome.runtime.getURL("index.html");
              var top = win.top + Math.round((win.height - height) / 2);
              var left = win.left + Math.round((win.width - width) / 2);
              chrome.windows.create({'url': url, 'type': 'popup', 'width': width, 'height': height, 'top': top, 'left': left}, function (w) {app.id.main = w.id});
            });
          }
        });        
      });
    }
  }
})();

chrome.windows.onRemoved.addListener(function (e) {if (e === app.id.main) {app.id.main = null}});
chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
  if(request.launch == true && request.senderName == "Documate")
  {
    app.id.main ? chrome.windows.update(app.id.main, {"focused": true}) : app.UI.create()
    sendResponse("Success");
  }
  else if(request.blobAsText)
  {  
      //console.log("new chunk received");

      var message = {
        blobAsText: request.blobAsText,
        mimeString: request.mimeString,
        chunks: request.chunks,
      };

      chrome.runtime.sendMessage(message);
      sendResponse();
      return;
  }
});