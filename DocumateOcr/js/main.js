const strNoResult = "The image cannot be recognized.\nPlease try to adjust the Contrast, Brightness, or Saturation of the image and then recognize it again.";

// if ('serviceWorker' in navigator) {
//   window.addEventListener('load', function() {
//     navigator.serviceWorker.register('/service-worker.js');
//   });
// }

const doOCR = async () => {
  
  showProgressBar();
  applyFilter();

  const image = document.getElementById('canvas');
  const result = document.getElementById('result');
  result.value = "";

  const { createWorker } = Tesseract;
  const worker = createWorker({
    workerPath: chrome.runtime.getURL('js/worker.min.js'),
    langPath: chrome.runtime.getURL('traineddata'),
    corePath: chrome.runtime.getURL('js/tesseract-core.wasm.js'),
    logger: m => {
      $("#divProgress").show();
      $("#labelP").text(m.status);
      $("#OcrProgress").val(m.progress*100);
    }
  });

  var selector = document.getElementById('language');
  var language = selector.options[selector.selectedIndex].value;

  await worker.load();
  await worker.loadLanguage(language);
  await worker.initialize(language);

  const { data: { text } } = await worker.recognize(image);

  await worker.terminate();

  if ( isNull(text) )
  {
    result.value = strNoResult;
  }
  else
  {
    result.value = text;
  }    

  hideProgressBar();  
  RecoverCanvas();
}

function showProgressBar()
{
  $("body").css("cursor", "progress");
  $("#ctrl").hide();
  $("#divProgress").show();
}

function hideProgressBar()
{
  $("body").css("cursor", "default");
  $("#ctrl").show();
  $("#divProgress").hide();
}

const btnOcr = document.getElementById('btnOcr');
btnOcr.onclick = doOCR;

const doBack2Dc = async () => {  
  chrome.management.getAll(function (m)
  {
    m.forEach(element => {
      if(element.name = "Documate")  
      {
        var message = {
          launch: true,
          senderName: "DocumateOcr"
        };
		
        chrome.runtime.sendMessage(element.id, message, function(){		
          if (chrome.runtime.lastError) {				
            console.log("could not send message to app");
          } 
        });

        window.close();
      }
    });
  });
}

const btnBack2Dc = document.getElementById('btnBack2Dc');
btnBack2Dc.onclick = doBack2Dc;

function isNull( str )
{
  if ( str == "" )
    return true;

  var regu = "^[ ]+$";
  var re = new RegExp(regu);

  var result = re.test(str);
  return re.test(str);
}

const doSaveResult = async () => {
  const textArea = document.getElementById('result');
  //var str = textArea.value
  if ( isNull(textArea.value) || textArea.value == strNoResult )
  {
    $("#dialog").dialog({
      autoOpen: false,
      width: 220,
      height: 120,
      show: {
        effect: "fade",
        duration: 500
      },
      hide: {
        effect: "fade",
        duration: 500
      },
      buttons: {         		 
        "OK": function() { $(this).dialog("close"); }		 
      }
    });

    $("#dialog").dialog( "open" );
  }
  else
  {
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours()+'-'+today.getMinutes()+'-'+today.getSeconds();
    var dt = date + '-'+ time;
    downloadToFile(textArea.value, 'OCR_'+ dt +'.txt', 'text/plain');
  }
}

const downloadToFile = (content, filename, contentType) => {
  const a = document.createElement('a');
  const file = new Blob([content], {type: contentType});
  
  a.href= URL.createObjectURL(file);
  a.download = filename;
  a.click();

	URL.revokeObjectURL(a.href);
};

const btnSaveResult = document.getElementById('btnSaveResult');
btnSaveResult.onclick = doSaveResult;


var _chunkIndex = 0;
var _blobs;

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if(_blobs === undefined)
  {
    _blobs = new Array(request.chunks);
  }

  //new chunk received  
  _chunkIndex++;                   

  var bytes = new Uint8Array(request.blobAsText.length);                     
  for (var i=0; i<bytes.length; i++)
  {
    bytes[i] = request.blobAsText.charCodeAt(i);            
  }         
  // store blob
  _blobs[_chunkIndex-1] = new Blob([bytes], {type: request.mimeString});           

  if (_chunkIndex == request.chunks)
  {
    // merge all blob chunks
    for (j=0; j<_blobs.length; j++)
    {
      var mergedBlob;
      if (j>0)
      {
        mergedBlob = new Blob([mergedBlob, _blobs[j]], {type: request.mimeString});
      }
      else
      {
        mergedBlob = new Blob([_blobs[j]], {type: request.mimeString});
      }
    }

    updateCanvas(mergedBlob);

    _chunkIndex = 0;
  }
  sendResponse();
});

function updateCanvas(blob)
{
  blobtoDataURL(blob, function (dataURL){

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    var img = new Image;

    img.onload = function(){
      ctx.clearRect(0, 0, $('#canvas').width(), $('#canvas').height());

      if(this.width/this.height > canvas.width/canvas.height)
      {
        // full width
        var height = canvas.width * this.height / this.width;
        var startY = (canvas.height - height) / 2;
        ctx.drawImage(img,0,0,this.width,this.height,0,startY,canvas.width,height);
      }
      else
      {
        // full height
        var width = canvas.height * this.width / this.height;
        var startX = (canvas.width - width) / 2;
        ctx.drawImage(img,0,0,this.width,this.height,startX,0,width,canvas.height);
        //ctx.drawImage(img,0,0,this.width,this.height,0,0,canvas.width,canvas.height);        
      }

      var buffer = document.getElementById('buffer');    
      var bufferContext = buffer.getContext("2d");
      bufferContext.drawImage(canvas, 0, 0);
    };
    img.src = dataURL;
  });     
}

function blobtoDataURL(blob, callback) {
  var fr = new FileReader();
  fr.onload = function(e) {
      callback(e.target.result);
  };
  fr.readAsDataURL(blob);
}

window.onload = function() {
  window.resizeTo(800, 600);
};

const resetBtn = document.getElementById('reset');
resetBtn.onclick = resetFilter;

function RecoverCanvas()
{
  var buffer = document.getElementById('buffer');
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  var filterVal = 'contrast(1) ';
  filterVal += 'brightness(1) ';
  filterVal += 'saturate(1) ';
  ctx.filter = filterVal;
  ctx.drawImage(buffer, 0, 0);

  UpdateFilter();
}

function applyFilter() {
  var buffer = document.getElementById('buffer');
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  var filterVal = 'contrast(' + $('#contrast').val() +') ';
  filterVal += 'brightness(' + $('#brightness').val() +') ';
  filterVal += 'saturate(' + $('#saturate').val() +') ';

  ctx.filter = filterVal;
  ctx.drawImage(buffer,0,0);

  filterVal = 'contrast(1) ';
  filterVal += 'brightness(1) ';
  filterVal += 'saturate(1) ';
  $('#canvas').css('webkitFilter', filterVal);
}

function resetFilter()
{
  $('#contrast').val(1);
  $('#brightness').val(1);
  $('#saturate').val(1);
  UpdateFilter();
}

function UpdateFilter()
{
  var filterVal = 'contrast(' + $('#contrast').val() +') ';
  filterVal += 'brightness(' + $('#brightness').val() +') ';
  filterVal += 'saturate(' + $('#saturate').val() +') ';
  $('#canvas').css('webkitFilter', filterVal);
}

$('#contrast').on('input', UpdateFilter);
$('#brightness').on('input', UpdateFilter);
$('#saturate').on('input', UpdateFilter);

