
'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// init 
///////////////////////////////////////////////////////////////////////////////////////////

window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
var appFs;

chrome.app.window.current().setAlwaysOnTop(false);

function onFsSuccess(fs) {
	appFs = fs;
	//console.log(fs);
}

function onFsError(fe) {
	console.log(fe);
}

window.requestFileSystem(
	PERSISTENT,
	512 * 1024 * 1024,
	onFsSuccess,
	onFsError);


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// create element  
///////////////////////////////////////////////////////////////////////////////////////////

function createDocumentElement(tagId, childId)
{
	var parent = document.createElement(tagId);
	parent.id = childId;
	document.body.appendChild(parent);
}

function createElement(tagId, parentId, childId)
{
    var parent = document.getElementById(parentId);
    var child = document.createElement(tagId);

	child.id = childId;
	
	parent.appendChild(child);
}

function createCanvasElement(parentId, childId)
{
	createElement("canvas", parentId, childId)
}

function createDivElement(parentId, childId)
{
	createElement("div", parentId, childId)
}

function createSpanElement(parentId, childId)
{
	createElement("span", parentId, childId)
}

function createSelectElement(parentId, childId)
{
	createElement("select", parentId, childId)
}

function createTextareaElement(parentId, childId)
{
	createElement("textarea", parentId, childId)
}

function createImgElement(parentId, childId)
{
	createElement("img", parentId, childId)
}

function createVideoElement(parentId, childId)
{
	createElement("video", parentId, childId)
}

function createInputElement(parentId, childId)
{
	createElement("input", parentId, childId)
}

function createLabelElement(parentId, childId)
{
	createElement("label", parentId, childId)
}

///////////////////////////////////////////////////////////////////////////////////////////
// operation   
///////////////////////////////////////////////////////////////////////////////////////////

function showElement(id, mode)
{
	var eleId;

	eleId = document.getElementById(id);
	eleId.hidden = !mode;
}


'use strict';


if ( typeof MouseStatus == "undefined" )
{
    var MouseStatus = {};
    MouseStatus.over = 0;
    MouseStatus.normal = 1;
    MouseStatus.click = 2;
}

if ( typeof MouseBtnId == "undefined" )
{
	var MouseBtnId = {};
	MouseBtnId.left = 1;
	MouseBtnId.right = 3;
}

if ( typeof DrawState == "undefined" )
{
    var DrawState = {};
    DrawState.start = 0;
    DrawState.move = 1;
    DrawState.end = 2;
    DrawState.undo = 3;
    DrawState.redo = 4;
    DrawState.mirror = 5;
    DrawState.flip = 6;
	DrawState.brightness = 7;
	DrawState.zoom = 8;
	DrawState.pan = 9;
	DrawState.whiteBalance = 10;
}

if ( typeof VrField == "undefined" )
{
    var VrField = {};
    VrField.w = 0;
    VrField.h = 1;
    VrField.data = 2;
}

if ( typeof FontField == "undefined" )
{
    var FontField = {};
    FontField.value = 0;
    FontField.status = 1;
}

if ( typeof BaseImgChg == "undefined" )
{
    var BaseImgChg = {};
    BaseImgChg.before = 0;
    BaseImgChg.after = 1;
}

if ( typeof UiType == "undefined" )
{
    var UiType = {};
    UiType.toolBar = 0;
    UiType.scrollBar = 1;
    UiType.colorBar = 2;
	UiType.sysToolBar = 3;
	UiType.displayCanvas = 4;
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  constant 
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof BtnData == "undefined" )
{
    var BtnData = {};
    BtnData.id = 0;
    BtnData.pic = 1;
    BtnData.state = 2;
	BtnData.groupId = 3;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  create
///////////////////////////////////////////////////////////////////////////////////////////

function createToolBarBtns(btnData, parentId)
{
	var strTotal = "";
	var strElement;
	
    var len = btnData.length;
	var i;

	// set style

	for ( i = 0; i < len; i++ )
	{
		strElement = '<span class="' + parentId + ' toolBar" id="' + btnData[i][BtnData.id] + '"></span>';
		strTotal += strElement;
	}

	$('#' + parentId).html(strTotal);
}

function createToolBar(btnData, cssCfg, parentId, id, position, funcClick)
{
	// create div element
	createDivElement(parentId, id);

	// save button data

	$('#' + id).data('btnData', btnData);

    // create buttons
    
    createToolBarBtns(btnData, id);

	// set style

	var eleString = '.' + id;

	$(eleString).css(cssCfg);

	// set pos

	var isFullH = false;
	var isRight = false;
	var isBottom = false;
	var startY = 0;
	var offsetX = cssCfg.width;
	var offsetY = cssCfg.height;

	if ( 'right' === position )
	{
		isRight = true;
		offsetX = 0;
		isFullH = true;
	
	}

	if ( 'left' === position )
	{
		offsetX = 0;
		isFullH = true;
	}

	if ( 'top-right' === position )
	{
		isRight = true;
		offsetY = 0;
		offsetX = -cssCfg.width;
	}

	if ( 'top-left' === position )
	{
		offsetY = 0;
	}

	if ( 'bottom' === position )
	{
		offsetX = 60;
		offsetY = 0;
		isBottom = true;
	}

	$(eleString).attr('style', function(index, previousValue)
		{
			var attrLeft;

			if ( isRight ) attrLeft = ' left: calc(100% - ' + (cssCfg.width - index * offsetX) + 'px);';
			else attrLeft = ' left: ' + index*offsetX + 'px;';

			var curY;
			var attrTop;

			curY = startY + index * offsetY;
			attrTop = ' top: ' + curY + 'px;';

			return previousValue + attrLeft + attrTop;
		});
	
	// set mouse event handler

	$(eleString).hover(toolBarOnMouseEnter, toolBarOnMouseLeave);

	if ( funcClick == undefined )
	{
		$(eleString).on('click', toolBarOnMouseClick);
	}
	else
	{
		$(eleString).on('click', funcClick);
	}

	// set container element with full height

	if ( isFullH )
	{
		(function()
		{
			var cssH =
			{
				top: titleH + 'px',
				height: 'calc(100% - ' + titleH + 'px)',
				width: cssCfg.width + 'px',
				background: appCfg.toolBarColorLeave,
				opacity: appCfg.toolBarOpacityLeave,
				position: 'absolute',
			};

			if ( isRight ) cssH.right = '0px';
			else cssH.left = '0px';
			
			$('#' + id).css(cssH);
		})();
	}

	// set container element with full width

	if ( isBottom )
	{
		(function()
		{
			var cssW =
			{				
				top: 'calc(100% - ' + appCfg.scrollIconH + 'px)',
				left: '340px',//appCfg.toolBarIconW + 'px',
				height: appCfg.scrollIconH + 'px',//appCfg.toolBarIconH + 'px',
				width: '60px',				
				background: "#00000000",//appCfg.toolBarColorLeave,
				opacity: appCfg.toolBarOpacityLeave,
				position: 'absolute',
			};

			$('#' + id).css(cssW);
		})();
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  update 
///////////////////////////////////////////////////////////////////////////////////////////

function getToolBarBtnIdx(btnData, btnId)
{
    var i;
    var len = btnData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( btnId === btnData[i][BtnData.id] )
	    {
	        return i;
	    }
	}

	return -1;
}

function setToolBarBtnPic(btnData, idx, state)
{
    var eleId;
	var bkg;
	var color;
	var opacity;

	eleId = "#" + btnData[idx][BtnData.id];
	
	bkg = fcGetDefIconPath(btnData[idx][BtnData.pic]);
	$(eleId).css('background', bkg);

	if (fullPhotoMode)
	{
		if ( state === MouseStatus.over )
		{
			color = appCfg.toolBarColorEnter;
			opacity = appCfg.toolBarOpacityEnter;
		}
		else if ( state === MouseStatus.normal )
		{
			color = appCfg.toolBarColorHide;
			opacity = appCfg.toolBarOpacityHide;
		}
		else
		{
			color = appCfg.toolBarColorClick;
			opacity = appCfg.toolBarOpacityClick;
		}
	}
	else
	{
		if ( state === MouseStatus.over )
		{
			color = appCfg.toolBarColorEnter;
			opacity = appCfg.toolBarOpacityEnter;
		}
		else if ( state === MouseStatus.normal )
		{
			color = appCfg.toolBarColorLeave;
			opacity = appCfg.toolBarOpacityLeave;
		}
		else
		{
			color = appCfg.toolBarColorClick;
			opacity = appCfg.toolBarOpacityClick;
		}
	}	

	$(eleId).css('background-color', color);
	$(eleId).css('opacity', opacity);
}

function updateToolBar(btnData)
{
    var i;
    var len = btnData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( 1 === btnData[i][BtnData.state] )
	    {
			setToolBarBtnPic(btnData, i, MouseStatus.click);
		}
		else if ( 2 === btnData[i][BtnData.state] )
		{
			setToolBarBtnPic(btnData, i, MouseStatus.over);
		}
		else
		{
 			setToolBarBtnPic(btnData, i, MouseStatus.normal);
		}
	}
}

function getToolBarSelectedBtnIdx(btnData, groupId)
{
    var i;
    var len = btnData.length;

	for ( i = 0; i < len; i++ )
	{
		if ( groupId === btnData[i][BtnData.groupId] )
		{
		    if ( btnData[i][BtnData.state] > 0 )
		    {
		        return i;
		    }
		}
	}

	return -1;
}

function getToolBarGroupIdBtnCnt(btnData, groupId)
{
	var cnt = 0;
    var i;
    var len = btnData.length;

	for ( i = 0; i < len; i++ )
	{
		if ( groupId === btnData[i][BtnData.groupId] )
		{
		    cnt += 1;
		}
	}

	return cnt;
}

function selectToolBarBtn(btnData, idx)
{
    // toggle selectd button state
    
    var state = 1;
	
	if ( btnData[idx][BtnData.state] ) state = 0;

	// get group id
	
	var groupId;

	groupId = btnData[idx][BtnData.groupId];

	if ( getToolBarGroupIdBtnCnt(btnData, groupId) > 1 )
	{
		state = 1;
	}

    // clear selected button state

	if ( groupId > 0 )
	{
		var preSelected = getToolBarSelectedBtnIdx(btnData, groupId);

		if ( preSelected >= 0 )
		{
			if ( groupId === btnData[preSelected][BtnData.groupId] )
			{
			    btnData[preSelected][BtnData.state] = 0;
				setToolBarBtnPic(btnData, preSelected, MouseStatus.normal);
			}
		}
	}
	
	// set and update selected button
	
	btnData[idx][BtnData.state] = state;
	if ( state )
	{
	    setToolBarBtnPic(btnData, idx, MouseStatus.click);

		if ( 0 === groupId )
		{
			setTimeout(function () {
				btnData[idx][BtnData.state] = 0;
				setToolBarBtnPic(btnData, idx, MouseStatus.normal);
			},
			100);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  mouse event 
///////////////////////////////////////////////////////////////////////////////////////////

function toolBarOnMouseHoverHandler(event, state)
{
	var eleId = '#' + event.target.id;
	var btnData = $(eleId).parent().data('btnData');

	var idx = getToolBarBtnIdx(btnData, event.target.id);

	if ( 0 === btnData[idx][BtnData.state] )
	{
		setToolBarBtnPic(btnData, idx, state);
	}
}

function toolBarOnMouseEnter(event)
{
	toolBarOnMouseHoverHandler(event, MouseStatus.over);
}

function toolBarOnMouseLeave(event)
{
	toolBarOnMouseHoverHandler(event, MouseStatus.normal);
}

function toolBarOnMouseClick(event)
{
	fcMouseEventNotify(UiType.toolBar, MouseStatus.click, event);

	var btnData = $('#' + event.target.id).parent().data('btnData');
    var idx = getToolBarBtnIdx(btnData, event.target.id);
    if ( idx >= 0 )
    {
        selectToolBarBtn(btnData, idx);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
//  control 
///////////////////////////////////////////////////////////////////////////////////////////

function toolBarSetElementState(id, member, state)
{
	var btnData = $('#' + id).data('btnData');
	var i;

	for ( i = 0; i < btnData.length; i++ )
	{
		if ( member == btnData[i][BtnData.id] )
		{
			btnData[i][BtnData.state] = state;
			updateToolBar(btnData);
			return;
		}
	}
}

function toolBarSetAllElementState(id, state)
{
	var btnData = $('#' + id).data('btnData');
	var i;

	for ( i = 0; i < btnData.length; i++ )
	{
		btnData[i][BtnData.state] = state;
	}
	updateToolBar(btnData);
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  constant 
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof ColorData == "undefined" )
{
    var ColorData = {};
    ColorData.color = 0;
    ColorData.state = 1;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  create
///////////////////////////////////////////////////////////////////////////////////////////

function createColorBarBtns(colorData, parentId)
{
	var strTotal = "";
	var strElement;
	
    var len = colorData.length;
	var i;
	var eleId;

	for ( i = 0; i < len; i++ )
	{
		//eleId = parentId + '-' + colorData[i][ColorData.color];
		eleId = colorData[i][ColorData.color];

		createDivElement(parentId, eleId);

		$('#' + eleId).addClass(parentId + ' colorBar');
	}
}

function createColorBar(colorData, cssCfg, parentId, id, funcNotify)
{
	// create div element

	createDivElement(parentId, id);

	// set css

	(function()
	{
		var css =
		{
			position: 'absolute',
			'margin-left': '0px',
			background: appCfg.drawSetBackground,
			opacity: appCfg.drawSetOpacity,
		};

		var len = colorData.length;

		var w = appCfg.colorIconW;
		var h = appCfg.colorIconH;

		css.width = (w*len) + 'px';
		css.height = h + 'px';
		css.left = 'calc(100% - ' + (w*len) + 'px)';
		css.top = '0px';
		
		$('#' + id).css(css);
	
	})();

	// save color data

	colorData.fnNotify = funcNotify;

	$('#' + id).data('colorData', colorData);

    // create buttons
    
    createColorBarBtns(colorData, id);

	// set style

	var eleString = '.' + id;

	$(eleString).css(cssCfg);

	// set pos

	$(eleString).attr('style', function(index, previousValue)
		{
			var attrLeft;
			attrLeft = ' left: calc(100% - ' + (cssCfg.width*(index+1)) + 'px);';
			
			var attrTop;
			attrTop = ' top: ' + 0 + 'px;';

			var color;
			color = ' background: #' + colorData[index][ColorData.color] + ';'; 

			var margin;
			margin = ' margin: ' + appCfg.colorIconMarginNormal + 'px;';

			var width;
			width = ' width: ' + (cssCfg.width - 2*appCfg.colorIconMarginNormal) + 'px;';

			var height;
			height = ' height: ' + (cssCfg.height - 2*appCfg.colorIconMarginNormal) + 'px;';

			return previousValue + attrLeft + attrTop + color + margin + width + height;
		});
	
	// set mouse event handler

	$(eleString).hover(colorBarOnMouseEnter, colorBarOnMouseLeave);

	$(eleString).on('click', colorBarOnMouseClick);

	// notify

	colorBarNotify($('#' + id));
}


///////////////////////////////////////////////////////////////////////////////////////////
//  mouse event 
///////////////////////////////////////////////////////////////////////////////////////////

function colorBarOnMouseClick(event)
{
	var colorData = $('#' + event.target.id).parent().data('colorData');
	var i;

	for ( i = 0; i < colorData.length; i++ )
	{
		if ( event.target.id === colorData[i][ColorData.color] ) colorData[i][ColorData.state] = 1;
		else colorData[i][ColorData.state] = 0;
	}
	
    updateColorBar(colorData);

	colorBarNotify($('#' + event.target.id).parent());

	fcMouseEventNotify(UiType.colorBar, MouseStatus.click, event);
}

function colorBarOnMouseHoverHandler(event, state)
{
	var eleId = '#' + event.target.id;
	var colorData = $(eleId).parent().data('colorData');
	var idx = getColorBarBtnIdx(colorData, event.target.id);

	if ( 0 === colorData[idx][ColorData.state] )
	{
		setColorBarBtn(colorData, idx, state);
	}
}

function colorBarOnMouseEnter(event)
{
	colorBarOnMouseHoverHandler(event, MouseStatus.over);
}

function colorBarOnMouseLeave(event)
{
	colorBarOnMouseHoverHandler(event, MouseStatus.normal);
}


///////////////////////////////////////////////////////////////////////////////////////////
//  update 
///////////////////////////////////////////////////////////////////////////////////////////

function getColorBarBtnIdx(colorData, btnId)
{
    var i;
    var len = colorData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( btnId === colorData[i][ColorData.color] )
	    {
	        return i;
	    }
	}

	return -1;
}

function setColorBarBtn(colorData, idx, state)
{
	var eleId = "#" + colorData[idx][ColorData.color];
	var margin;
	var size;

	if ( state === MouseStatus.over )
	{
		margin = appCfg.colorIconMarginOver;
		size = appCfg.colorIconW - appCfg.colorIconMarginNormal*2;
	}
	else if ( state === MouseStatus.normal )
	{
		margin = appCfg.colorIconMarginNormal;
		size = appCfg.colorIconW - appCfg.colorIconMarginNormal*2;
	}
	else
	{
		margin = appCfg.colorIconMarginClick;
		size = appCfg.colorIconW - appCfg.colorIconMarginClick*2;
	}

	$(eleId).css('margin', margin + 'px');
	$(eleId).css('width', size + 'px');
	$(eleId).css('height', size + 'px');
}

function updateColorBar(colorData)
{
    var i;
    var len = colorData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( 1 === colorData[i][ColorData.state] )
	    {
			setColorBarBtn(colorData, i, MouseStatus.click);
		}
		else
		{
 			setColorBarBtn(colorData, i, MouseStatus.normal);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  notify 
///////////////////////////////////////////////////////////////////////////////////////////

function hexToRgb(hex)
{
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function colorBarNotify(id$)
{
	var colorData = $(id$).data('colorData');
	var color;
	var i;
	var r, g, b;

	if ( colorData.fnNotify )
	{
		for ( i = 0; i < colorData.length; i++ )
		{
			if ( colorData[i][ColorData.state] )
			{
				color = colorData[i][ColorData.color];
			
				r = hexToRgb('#' + color).r;
				g = hexToRgb('#' + color).g;
				b = hexToRgb('#' + color).b;
			
				colorData.fnNotify(color, r, g, b);
			}
		}
	}
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  constant 
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof ColorData == "undefined" )
{
    var ColorData = {};
    ColorData.color = 0;
    ColorData.state = 1;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  create
///////////////////////////////////////////////////////////////////////////////////////////

function createColorBarBtnsType2(colorData, parentId)
{
    var len = colorData.length;
	var i;
	var spanId;
	var canvasId;
	var selector;
	var id;
	var ctx;
	var w, h;
	var r, g, b;
	var color;

	w = appCfg.cb2BlockW - 2*appCfg.cb2FrameGap;
	h = appCfg.cb2BlockH - 2*appCfg.cb2FrameGap;

	var cssCanvas =
	{
		position: 'absolute',
		left: appCfg.cb2FrameGap,
		top: appCfg.cb2FrameGap,
		width: w,
		height: h,
		opacity: 1,
	};

	var cssSpan =
	{
		position: 'absolute',
		left: 0,
		top: 0,
		width: appCfg.scrollIconW,
		height: appCfg.scrollIconH,
		opacity: 1,
	};

		
	for ( i = 0; i < len; i++ )
	{
		// get color
	
		color = colorData[i][ColorData.color];

		r = hexToRgbType2('#' + color).r;
		g = hexToRgbType2('#' + color).g;
		b = hexToRgbType2('#' + color).b;

		// create base span

		spanId = color;
		createSpanElement(parentId, spanId);

		// set span size

		selector = '#' + spanId;

		cssSpan.left = (len - 1 - i) * appCfg.scrollIconW;

		$(selector).css(cssSpan);

		// create canvas

		canvasId = spanId + '_' + 'canvas';
		createCanvasElement(spanId, canvasId);

		// set canvas mouse event handler
		
		selector = '#' + canvasId;

		$(selector).hover(colorBarOnMouseEnterType2, colorBarOnMouseLeaveType2);

		$(selector).on('click', colorBarOnMouseClickType2);

		// set canvas size

		$(selector).css(cssCanvas);

	    $(selector).attr('width', w);
	    $(selector).attr('height', h);

		// draw color block

		id = document.getElementById(canvasId);
		ctx = id.getContext('2d');

		fcClearCanvas(id);

		var fillStyle = 'rgba('+ r + ', ' + g + ', ' + b + ', ' + 1.0 + ')';

		ctx.fillStyle = fillStyle;
		ctx.fillRect(0, 0, w, h);
	}
}

function createColorBarType2(colorData, cssCfg, parentId, id, funcNotify)
{
	// create div element

	createSpanElement(parentId, id);

	// set css

	(function()
	{
		var css =
		{
			position: 'absolute',
			'margin-left': '0px',
			background: appCfg.drawSetBackground,
			opacity: appCfg.drawSetOpacity,
		};

		var len = colorData.length;

		var w = appCfg.cb2BlockW;
		var h = appCfg.cb2BlockH;

		css.width = (w*len) + 'px';
		css.height = h + 'px';
		//css.right = '0px';
		css.left = 'calc(100% - ' + (w*len) + 'px)';
		css.top = '0px';
		
		$('#' + id).css(css);
	
	})();

	// save color data

	colorData.fnNotify = funcNotify;

	$('#' + id).data('colorData', colorData);

    // create buttons
    
    createColorBarBtnsType2(colorData, id);

	// notify

	colorBarNotifyType2($('#' + id));
}


///////////////////////////////////////////////////////////////////////////////////////////
//  mouse event 
///////////////////////////////////////////////////////////////////////////////////////////

function colorBarOnMouseClickType2(event)
{
	var canvasId = event.target.id;
	var spanId = $('#' + canvasId).parent().attr('id');
	var baseId = $('#' + spanId).parent().attr('id');
	var colorData = $('#' + baseId).data('colorData');
	var i;

	for ( i = 0; i < colorData.length; i++ )
	{
		if ( spanId === colorData[i][ColorData.color] ) colorData[i][ColorData.state] = 1;
		else colorData[i][ColorData.state] = 0;
	}
	
    updateColorBarType2(colorData);

	colorBarNotifyType2($('#' + baseId));

	fcMouseEventNotify(UiType.colorBar, MouseStatus.click, event);
}

function colorBarOnMouseHoverHandlerType2(event, state)
{
	var canvasId = event.target.id;
	var spanId = $('#' + canvasId).parent().attr('id');
	var baseId = $('#' + spanId).parent().attr('id');
	var colorData = $('#' + baseId).data('colorData');
	var idx = getColorBarBtnIdxType2(colorData, spanId);

	if ( 0 === colorData[idx][ColorData.state] )
	{
		setColorBarBtnType2(colorData, idx, state);
	}
}

function colorBarOnMouseEnterType2(event)
{
	colorBarOnMouseHoverHandlerType2(event, MouseStatus.over);
}

function colorBarOnMouseLeaveType2(event)
{
	colorBarOnMouseHoverHandlerType2(event, MouseStatus.normal);
}


///////////////////////////////////////////////////////////////////////////////////////////
//  update 
///////////////////////////////////////////////////////////////////////////////////////////

function getColorBarBtnIdxType2(colorData, btnId)
{
    var i;
    var len = colorData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( btnId === colorData[i][ColorData.color] )
	    {
	        return i;
	    }
	}

	return -1;
}

function setColorBarBtnType2(colorData, idx, state)
{
	var eleId = "#" + colorData[idx][ColorData.color];
	var bkColor;

	if ( state === MouseStatus.over )
	{
		bkColor = appCfg.cb2MouseColorEnter;
	}
	else if ( state === MouseStatus.normal )
	{
		bkColor = appCfg.cb2MouseColorLeave;
	}
	else
	{
		bkColor = appCfg.cb2MouseColorClick;
	}

	$(eleId).css('background', bkColor);
}

function updateColorBarType2(colorData)
{
    var i;
    var len = colorData.length;

	for ( i = 0; i < len; i++ )
	{
	    if ( 1 === colorData[i][ColorData.state] )
	    {
			setColorBarBtnType2(colorData, i, MouseStatus.click);
		}
		else
		{
 			setColorBarBtnType2(colorData, i, MouseStatus.normal);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  notify 
///////////////////////////////////////////////////////////////////////////////////////////

function hexToRgbType2(hex)
{
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function colorBarNotifyType2(id$)
{
	var colorData = $(id$).data('colorData');
	var color;
	var i;
	var r, g, b;

	if ( colorData.fnNotify )
	{
		for ( i = 0; i < colorData.length; i++ )
		{
			if ( colorData[i][ColorData.state] )
			{
				color = colorData[i][ColorData.color];
			
				r = hexToRgbType2('#' + color).r;
				g = hexToRgbType2('#' + color).g;
				b = hexToRgbType2('#' + color).b;
			
				colorData.fnNotify(color, r, g, b);
			}
		}
	}
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  create
///////////////////////////////////////////////////////////////////////////////////////////

function createScrollBar(scrollData, cssCfg, parentId, id, funcNotify)
{
	var iconId;
	var canvasId;
	var eleId;
	var bkg;
	var curX;
	var w, h;
	var totalW;
	
	// create div element

	createSpanElement(parentId, id);

	// set div css

	totalW = scrollData.iconSize + appCfg.scrollIconGap + scrollData.length + appCfg.scrollTickH;

	if ( typeof scrollData.iconR != "undefined" ) totalW += appCfg.scrollIconGap + scrollData.iconSize;

	cssCfg.left = scrollData.startX + 'px';
	cssCfg.top = scrollData.startY + 'px';
	cssCfg.width = totalW + 'px';
	cssCfg.height = scrollData.iconSize + 'px';

	$('#' + id).css(cssCfg);
	
	// save scroll data

	scrollData.id = id;
	scrollData.mouseDown = false;
	scrollData.fnNotify = funcNotify;

	$('#' + id).data('scrollData', scrollData);

	// create child element

	iconId = id + '-iconL';
	createSpanElement(id, iconId);

	var canvasId = id + '-canvas';
	createCanvasElement(id, canvasId);

	// set left icon style

	curX = 0;

	eleId = '#' + iconId;

	bkg = fcGetDefIconPath(scrollData.iconL);
	$(eleId).css('background', bkg);

	$(eleId).css('top', 0 + 'px');
	$(eleId).css('left', curX + 'px');
	$(eleId).css('width', scrollData.iconSize + 'px');
	$(eleId).css('height', scrollData.iconSize + 'px');
	$(eleId).css('position', 'absolute');

	curX += scrollData.iconSize;

	// set canvas style

	curX += appCfg.scrollIconGap;

	eleId = '#' + canvasId;

	w = scrollData.length + appCfg.scrollTickH;
	h = scrollData.iconSize;
	
	$(eleId).css('top', 0 + 'px');
	$(eleId).css('left', curX + 'px');
	$(eleId).css('width', w + 'px');
	$(eleId).css('height', h + 'px');
	$(eleId).css('position', 'absolute');

    $(eleId).attr('width', w);
    $(eleId).attr('height', h);

	curX += w;

	// set right icon style

	if ( typeof scrollData.iconR != "undefined" )
	{
		iconId = id + '-iconR';
		createSpanElement(id, iconId);

		curX += appCfg.scrollIconGap;
	
		eleId = '#' + iconId;

		bkg = fcGetDefIconPath(scrollData.iconR);
		$(eleId).css('background', bkg);

		$(eleId).css('top', 0 + 'px');
		$(eleId).css('left', curX + 'px');
		$(eleId).css('width', scrollData.iconSize + 'px');
		$(eleId).css('height', scrollData.iconSize + 'px');
		$(eleId).css('position', 'absolute');
	}

	// notify host

	scrollBarNotify(id);

	// set canvas mouse event handler 

	eleId = '#' + canvasId;

	(function () {

		var sd = $('#' + id).data('scrollData');
		
		$(eleId)
			.on('mousedown', function(event) {
				if ( MouseBtnId.left === event.which )
				{
					sd.mouseDown = true;

					updateScrollPos(event);

					fcMouseEventNotify(UiType.scrollBar, MouseStatus.click, event);
				}
			})
			.on('mousemove', function(event) {
				if ( sd.mouseDown )
				{
					updateScrollPos(event);
				}
			})
	        .on('mouseup',function() {
				if ( MouseBtnId.left === event.which )
				{
					sd.mouseDown = false;
				}
	        })
	        .on('mouseout',function() {
				if ( MouseBtnId.left === event.which )
				{
					sd.mouseDown = false;
				}
			})
			.on('touchstart', function(event) {
				fcTouchHandler(event);
			})
			.on('touchmove', function(event) {
				fcTouchHandler(event);
			})
	        .on('touchend',function() {
				fcTouchHandler(event);
	        })
	        .on('touchcancel',function() {
				fcTouchHandler(event);
			});
	})();

	return totalW;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  update
///////////////////////////////////////////////////////////////////////////////////////////

function updateScrollPos(event)
{
	var eleId = '#' + event.target.id;
	var sd = $(eleId).parent().data('scrollData');

	var pos = event.offsetX;;
	var offset =  appCfg.scrollTickH / 2;

	if ( pos <= offset ) pos = 0;
	if ( pos > sd.length ) pos = sd.length;
	sd.curPos = pos;

	updateScrollBar(sd.id);

	scrollBarNotify(sd.id);
}

function updateScrollBar(id)
{
	var canvas;
	var eleId = '#' + id;
	var sd = $(eleId).data('scrollData');
	var context;
	var x, y, w, h, r;
	var offsetY;

	canvas = document.getElementById(id + '-canvas');
	context = canvas.getContext('2d');

	// clear background

	fcClearCanvas(canvas);

	// account offset Y

	offsetY = (sd.iconSize - appCfg.scrollTickH)/2;

	// draw axis

	x = appCfg.scrollTickH / 2;
	y = offsetY + (appCfg.scrollTickH - appCfg.scrollLineH)/2;
	w = sd.length;
	h = appCfg.scrollLineH;

	context.rect(x, y, w, h);
	context.fillStyle = sd.color;
	context.fill();

	// draw tick

	x = appCfg.scrollTickH/2 + sd.curPos;
	y = offsetY + appCfg.scrollTickH/2;
	r = appCfg.scrollTickH/2;

	context.arc(x, y, r, 0, 2 * Math.PI, false);
	context.fillStyle = sd.color;
	context.fill();
}


///////////////////////////////////////////////////////////////////////////////////////////
//  notify
///////////////////////////////////////////////////////////////////////////////////////////

function scrollBarNotify(id)
{
	var eleId = '#' + id;
	var sd = $(eleId).data('scrollData');

	if ( sd.fnNotify )
	{
		sd.fnNotify( sd.length, sd.curPos );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
//  control
///////////////////////////////////////////////////////////////////////////////////////////

function scrollBarSetPos(id, pos)
{
	var sd = $('#' + id).data('scrollData');

	if ( pos <= sd.length )
	{
		sd.curPos = pos;
		updateScrollBar(id);
	}
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  create
///////////////////////////////////////////////////////////////////////////////////////////

function showConfigDlg(mode)
{
	showElement('configDlg', mode);
}

function initConfigDlg()
{
	var eleId;
	var sliderId;
	var configDlg = "configDlg";

	var x = 24;
	var y = 36;
	var gapV = 48;

	var w = 420;
	var h = 200;

	createDivElement("uiArea", configDlg);
	eleId = "#" + configDlg;
	$(eleId).css('background', appBaseCfg.ColorLeave);
	$(eleId).css("top", "calc(50%)");
	$(eleId).css('left', appCfg.toolBarIconW + "px");
	$(eleId).css('width', w + 'px');
	$(eleId).css('height', h + 'px');
	$(eleId).css('position', 'absolute');
	$(eleId).hide()

	// close icon
	eleId = configDlg + '-close';
	createSpanElement(configDlg, eleId);
	
	eleId = '#' + eleId;
	bkg = fcGetDefIconPath("close.png");

	$(eleId).css('background', bkg);
	$(eleId).css('top', 0 + 'px');
	$(eleId).css('left', (w - appCfg.toolBarIconW) + 'px');
	$(eleId).css('width', appCfg.toolBarIconW + 'px');
	$(eleId).css('height', appCfg.toolBarIconH + 'px');
	$(eleId).css('position', 'absolute');
	$(eleId).on('click', function(){
		$("#"+configDlg).hide()
	});

	// brightness
	eleId = configDlg + '-brightness';
	sliderId =  createScrollBarII(configDlg, eleId, "brightness.png", x, y);
	eleId = '#' + sliderId;
	$(eleId).attr({
		"type" : "range",
		"max" : 90,
		"min" : -90,
		"step" : 2,
		"value" : 0
	});
	$(eleId).on('input', function(m){
		
		appFC.previewBrightnessAdj = Math.floor(m.currentTarget.value);

		if ( appFC.curMode )
		{
			fcCanvasCtrl(DrawState.brightness);
		}
	});

	// rotate
	eleId = configDlg + '-rotate';
	sliderId = createScrollBarII(configDlg, eleId, "rotate.png", x, y + gapV);
	eleId = '#' + sliderId;
	$(eleId).attr({
		"type" : "range",
		"max" : 180,
		"min" : -180,
		"step" : 5,
		"value" : 0
	});
	$(eleId).on('input', function(m){
		appFC.previewRotate = Math.floor(m.currentTarget.value);
	});

	// white balence
	eleId = configDlg + '-whiteBalence';
	sliderId = createScrollBarII(configDlg, eleId, "wb.png", x, y + gapV * 2);
	eleId = '#' + sliderId;
	$(eleId).attr({
		"type" : "range",
		"max" : 40,
		"min" : -40,
		"step" : 1,
		"value" : 0
	});
	$(eleId).on('input', function(m){
		appFC.previewWhiteBalanceAdj = Math.floor(m.currentTarget.value);
		
		if ( appFC.curMode )
		{
			fcCanvasCtrl(DrawState.whiteBalance);
		}
	});
}

function createScrollBarII(parentId, id, icon, x, y)
{
	var sliderW = 200;
	var gapH = 16;

	eleId = id + "-icon";
	createSpanElement(parentId, eleId);
	eleId = '#' + eleId;
	$(eleId).css({
		"background" : fcGetDefIconPath(icon),
		"top" : y,
		"left" : x,
		"width" : appCfg.toolBarIconW + 'px',
		"height" : appCfg.toolBarIconH + 'px',
		"position" : "absolute"
	});

	var sliderId = id + '-slider';
	createInputElement(parentId, sliderId);

	eleId = "#" + sliderId;
	$(eleId).attr("class", "slider");
	$(eleId).css({
		"top" : y,
		"left" : x + appCfg.toolBarIconW + gapH,
		"width" : sliderW + 'px',
		"height" : appCfg.toolBarIconH + 'px',
		"position" : "absolute"
	});	

	eleId = id + '-reset';
	createSpanElement(parentId, eleId);

	eleId = '#' + eleId;
	$(eleId).css('background', fcGetDefIconPath("undo.png"));
	$(eleId).css('top', y + 'px');
	$(eleId).css('left', ( x + appCfg.toolBarIconW + gapH + sliderW + gapH )  + 'px');
	$(eleId).css('width', appCfg.toolBarIconW + 'px');
	$(eleId).css('height', appCfg.toolBarIconH + 'px');
	$(eleId).css('position', 'absolute');
	$(eleId).on('click', function(){
		$("#"+sliderId).val(0);
		$("#"+sliderId).trigger("input");
	});

	return sliderId;
}

function createScrollBarDlg(scrollData, cssCfg, parentId, id, funcNotify, withReset = false)
{
	var eleId;
	var w, h;
	var bkg;
	var dlgCss =
	{
		position: 'absolute',
		'margin-left': '0px',
		background: appCfg.scrollDlgBkg,
		opacity: appCfg.scrollDlgOpacity,
	};
	
	// create div element

	createSpanElement(parentId, id);

	// set div css

	w = scrollData.iconSize + appCfg.scrollIconGap + scrollData.length + appCfg.scrollTickH;
	if ( typeof scrollData.iconR != "undefined" ) w += appCfg.scrollIconGap + scrollData.iconSize;

	h = scrollData.iconSize + appCfg.toolBarIconH;

	dlgCss.left = 'calc(50% - ' + (w/2) + 'px)';
	dlgCss.top = 'calc(50% - ' + ((h - appCfg.titleIconH)/2) + 'px)';
	dlgCss.width = w + 'px';
	dlgCss.height = h + 'px';

	$('#' + id).css(dlgCss);

	// create close icon element

	eleId = id + '-close';
	createSpanElement(id, eleId);
	
	eleId = '#' + eleId;
	bkg = fcGetDefIconPath("close.png");

	$(eleId).css('background', bkg);
	$(eleId).css('top', 0 + 'px');
	$(eleId).css('left', (w - appCfg.toolBarIconW) + 'px');
	$(eleId).css('width', appCfg.toolBarIconW + 'px');
	$(eleId).css('height', appCfg.toolBarIconH + 'px');
	$(eleId).css('position', 'absolute');
	$(eleId).on('click', ScrollBarDlgClose);

	// creat reset icon elment
	if ( withReset )
	{
		eleId = id + '-reset';
		createSpanElement(id, eleId);

		eleId = '#' + eleId;
		bkg = fcGetDefIconPath("undo.png");
		
		$(eleId).css('background', bkg);
		$(eleId).css('top', 0 + 'px');
		$(eleId).css('left', (w/2 - appCfg.toolBarIconW/2) + 'px');
		$(eleId).css('width', appCfg.toolBarIconW + 'px');
		$(eleId).css('height', appCfg.toolBarIconH + 'px');
		$(eleId).css('position', 'absolute');
		$(eleId).on('click', function(){
			scrollBarSetPos(id + '-scrollbar', scrollData.length/2);
			updateScrollBar(id + '-scrollbar');
			scrollBarNotify(id + '-scrollbar');
		});
	}
	

	// create scroll bar

	eleId = id + '-scrollbar';

	scrollData.startY = appCfg.toolBarIconH;

	createScrollBar(scrollData, cssCfg, id, eleId, funcNotify);
	updateScrollBar(eleId);
}

///////////////////////////////////////////////////////////////////////////////////////////
//  event handler
///////////////////////////////////////////////////////////////////////////////////////////

function ScrollBarDlgClose(event)
{
	fcCloseActiveDlg();
}

function ScrollBarDlgSetPos(id, pos)
{
	var eleId = id + '-scrollbar';

	scrollBarSetPos(eleId, pos);
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  constant 
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof PictureWinAction == "undefined" )
{
    var PictureWinAction = {};
	PictureWinAction.move = 0;
    PictureWinAction.trAnchor = 1;
    PictureWinAction.tlAnchor = 2;
	PictureWinAction.brAnchor = 3;
	PictureWinAction.blAnchor = 4;
	PictureWinAction.down = 5;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  function 
///////////////////////////////////////////////////////////////////////////////////////////

function initPictureWin()
{
	// set default data

	var picData =
	{
		dispX: 0,
		dispY: 0,
		dispW: 0,
		dispH: 0,
		imgSrc: 0,
		isActive: false,
		x: 0,
		y: 0,
		w: 0,
		h: 0,
		mouseDown: false,
		type: PictureWinAction.down,
		startX: 0,
		startY: 0,
	};

	var eleId = '#importPictureWin';

	$(eleId).data('picData', picData);

	// create image display area

	displayPictureCreateImgDsiplayArea();
	
	displayPictureMouseEvent('importPicture', PictureWinAction.move);

	// verify expand ratio

	var expandRatio;

	expandRatio = appCfg.ipwRadiusExpandRatio;
	expandRatio = Math.floor(expandRatio);
	if ( expandRatio < 1 ) expandRatio = 1;

	appFC.ipwRadiusExpandRatio = expandRatio;

	// create anchor

	displayPictureCreateAnchor('tlAnchor', PictureWinAction.tlAnchor);
	displayPictureCreateAnchor('trAnchor', PictureWinAction.trAnchor);
	displayPictureCreateAnchor('blAnchor', PictureWinAction.blAnchor);
	displayPictureCreateAnchor('brAnchor', PictureWinAction.brAnchor);

	// hide

	activePictureWin(false);
}

function displayPictureCreateImgDsiplayArea()
{
	var r = appCfg.ipwRadius;

	var css =
	{
		position: 'absolute',
		left: r + 'px',
		top: r + 'px',
		width: 'calc(100% - ' + 2*r + 'px)',
		height: 'calc(100% - ' + 2*r + 'px)',
		opacity: appCfg.ipwOpacity,
	};

	createCanvasElement('importPictureWin', 'importPicture');

	$('#importPicture').css(css);
}

function displayPictureCreateAnchor(id, type)
{
	var eleId = "#" + id;
	var x, y;
	var r = appCfg.ipwRadius;
	var expandRatio;
	var size;
	var extraSpace;

	// caculate extra space

	size = r*2;
	expandRatio = appFC.ipwRadiusExpandRatio;
	extraSpace = size*(expandRatio - 1)/2;
	appFC.ipwExtraSpace = extraSpace;

	// create

	createCanvasElement('importPictureWin', id);

	// set size

	switch ( type )
	{
		case PictureWinAction.trAnchor:
			x = 'calc(100% - ' + (size + extraSpace) + 'px)';
			y = -extraSpace + 'px';
			break;
			
		case PictureWinAction.tlAnchor:
			x = -extraSpace + 'px';
			y = -extraSpace + 'px';
			break;
			
		case PictureWinAction.brAnchor:
			x = 'calc(100% - ' + (size + extraSpace) + 'px)';
			y = 'calc(100% - ' + (size + extraSpace) + 'px)';
			break;
			
		case PictureWinAction.blAnchor:
			x = -extraSpace + 'px';
			y = 'calc(100% - ' + (size + extraSpace) + 'px)';
			break;
	}

	size = size * expandRatio;

	var css =
	{
		position: 'absolute',
		left: x,
		top: y,
		width: size + 'px',
		height: size + 'px',
		opacity: appCfg.ipwOpacity,
	};

	$(eleId).css(css);

	$(eleId).attr('width', size);
	$(eleId).attr('height', size);	

	// draw circle

	var angle = Math.PI * 2;

	var ctx = document.getElementById(id).getContext('2d');

	ctx.fillStyle = appCfg.ipwColor;
	ctx.strokeStyle = appCfg.ipwColor;
	ctx.lineWidth = appCfg.ipwLineWidth;

	ctx.beginPath();
	ctx.arc((r+extraSpace), (r+extraSpace), r, 0, angle, false);
	ctx.closePath();
	ctx.fill();

	// set mouse event handler

	displayPictureMouseEvent(id, type);
}

function displayPictureMouseEvent(id, type)
{
	var eleId = '#importPictureWin';
	var pd = $(eleId).data('picData');
	var mc = appFC.curMode;

	var r = appCfg.ipwRadius;
	var r2 = r*r;

	eleId = '#' + id;

	$(eleId)
		.on('mousedown', function(event) {
			if ( MouseBtnId.left === event.which )
			{
				if ( checkInside(event, type) )
				{
					pd.mouseDown = true;

					pd.startX = event.offsetX;
					pd.startY = event.offsetY;

					pd.type = type;
				}
			}
		})
		.on('mousemove', function(event) {
			if ( pd.mouseDown )
			{
				switch ( pd.type )
				{
					case PictureWinAction.move:
						move(event);
						break;

					case PictureWinAction.trAnchor:
					case PictureWinAction.tlAnchor:
					case PictureWinAction.brAnchor:
					case PictureWinAction.blAnchor:
						updateSize(pd.type, event);
						break;
				}
			}
		})
        .on('mouseup',function(event) {
			if ( MouseBtnId.left === event.which )
			{
				pd.mouseDown = false;
				updateImgSize();
			}
        })
        .on('mouseout',function(event) {
			if ( MouseBtnId.left === event.which )
			{
				pd.mouseDown = false;
				updateImgSize();
			}
		})
		.on('touchstart', function(event) {
			fcTouchHandler(event);
		})
		.on('touchmove', function(event) {
			fcTouchHandler(event);
		})
		.on('touchend',function(event) {
			fcTouchHandler(event);
		})
		.on('touchcancel',function(event) {
		 	fcTouchHandler(event);
		});

	function updateImgSize()
	{
		var mc = appFC.curMode;
		var ratio = mc.imgW / dispW;

		pd.x = pd.dispX * ratio;
		pd.y = pd.dispY * ratio;
		pd.w = pd.dispW * ratio;
		pd.h = pd.dispH * ratio;
	}

	function updateSize(type, event)
	{
		var dx = event.offsetX - pd.startX;
		var dy = event.offsetY - pd.startY;

		var x = pd.dispX;
		var y = pd.dispY;
		var w = pd.dispW;
		var h = pd.dispH;
	
		switch ( type )
		{
			case PictureWinAction.trAnchor:
				w += dx;
				y += dy;
				h -= dy;
				break;
				
			case PictureWinAction.tlAnchor:
				x += dx;
				w -= dx;
				y += dy;
				h -= dy;
				break;
				
			case PictureWinAction.brAnchor:
				w += dx;
				h += dy;
				break;
				
			case PictureWinAction.blAnchor:
				x += dx;
				w -= dx;
				h += dy;
				break;
		}

		pd.dispX = x;
		pd.dispY = y;
		pd.dispW = w;
		pd.dispH = h;

		var css =
		{
			left: dispX + x,
			top: dispY + y,
			width: w,
			height: h,
		};

		$('#' + event.target.id).parent().css(css);
	}

	function move(event)
	{
		var dx = event.offsetX - pd.startX;
		var dy = event.offsetY - pd.startY;

		pd.dispX += dx;
		pd.dispY += dy;

		var css =
		{
			left: dispX + pd.dispX,
			top: dispY + pd.dispY,
		};

		$('#' + event.target.id).parent().css(css);
	}

	function checkInside(event, type)
	{
		var size = appCfg.ipwRadius*2;
		var extraSpace = appFC.ipwExtraSpace;
		var x, y;

		if ( PictureWinAction.move == type ) return true;

		x = event.offsetX;
		y = event.offsetY;

		if ( (x >= extraSpace) && (x < (extraSpace + size) ) && (y >= extraSpace) && (y < (extraSpace + size) ) ) return true;

		return false;
	}
}

function displayPictureWin(image)
{
	var imgDrawW, imgDrawH;
	var mc = appFC.curMode;
	var id = 'importPictureWin';
	var selector = '#' + id;
	var r = appCfg.ipwRadius;

	// ajust display size

	imgDrawW = image.width;
	imgDrawH = image.height;

	// limit min size

	var minSize = r*2;

	if ( imgDrawW < minSize )
	{
		imgDrawH = imgDrawH * minSize / imgDrawW;
		imgDrawW = minSize;
	}

	if ( imgDrawH < minSize )
	{
		imgDrawW = imgDrawW * minSize / imgDrawH;
		imgDrawH = minSize;
	}

	// limit max size

	var maxW = mc.imgW - r*2;
	var maxH = mc.imgH - r*2;
	
	if ( imgDrawW > maxW )
	{
		imgDrawH = imgDrawH * maxW / imgDrawW;
		imgDrawW = maxW;
	}

	if ( imgDrawH > maxH )
	{
		imgDrawW = imgDrawW * maxH / imgDrawH;
		imgDrawH = maxH;
	}

	// location in base image

	var canvasImgW, canvasImgH;
	var x, y;
	var w, h;
	var ratio;

	canvasImgW = image.width + r*2;
	canvasImgH = image.height + r*2;

	w = imgDrawW + r*2;
	h = imgDrawH + r*2;

	x = (mc.imgW - w) / 2;
	y = (mc.imgH - h) / 2;

	// update store data

	var pd = $(selector).data('picData');

	pd.imgSrc = image;
	pd.dispW = dispW;
	pd.dispH = dispH;
	pd.x = x;
	pd.y = y;
	pd.w = w;
	pd.h = h;

	// location in display area

	pd.isActive = true;

	resizePictureWin();
	
	// ���J�v��

	selector = '#importPicture'
    $(selector).attr('width', image.width);
    $(selector).attr('height', image.height);	

	var ctx = document.getElementById('importPicture').getContext('2d');

	ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, image.width, image.height);

	// ��� 

	activePictureWin(true);
}

function activePictureWin(mode)
{
	var pd = $('#' + 'importPictureWin').data('picData');

	showElement('importPictureWin', mode);

	pd.isActive = mode;
}

function resizePictureWin()
{
	var pd = $('#' + 'importPictureWin').data('picData');
	var mc = appFC.curMode;
	var x, y;
	var w, h;
	var ratio;

	if ( typeof pd == "undefined"  ) return;

	if ( false == pd.isActive ) return;

	ratio = dispW / mc.imgW;

	x = pd.x * ratio;
	y = pd.y * ratio;
	w = pd.w * ratio;
	h = pd.h * ratio;

	var css =
	{
		position: 'absolute',
		left: dispX + x,
		top: dispY + y,
		width: w,
		height: h,
		opacity: appCfg.ipwOpacity,
	};

	$('#' + 'importPictureWin').css(css);

	pd.dispX = x;
	pd.dispY = y;
	pd.dispW = w;
	pd.dispH = h;
}

function displayPictureWinClose()
{
	var eleId = '#importPictureWin';
	var pd = $(eleId).data('picData');
	var mc = appFC.curMode;
	var ctx = appFC.drawContext;
	var r = appCfg.ipwRadius * mc.imgW / dispW;

	if ( false == pd.isActive ) return;

	fcClearDrawingCanvas();

	console.log(pd);
	console.log(r);
	ctx.drawImage(pd.imgSrc, 0, 0, pd.imgSrc.width, pd.imgSrc.height, pd.x + r, pd.y + r, pd.w - 2*r, pd.h - 2*r);

	fcCanvasCtrl(DrawState.end);
	if ( mc.imageProcess ) mc.imageProcess();

	activePictureWin(false);
}


