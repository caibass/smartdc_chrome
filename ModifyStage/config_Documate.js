
'use strict';

const appName = "Documate";


var appBaseCfg = 
{
	// windows resize delay

	resizeDelayMs: 0,

	// mode / title / system / toolbar icon size

	iconW: 48,
	iconH: 48,

	// drawing cfg area size 
	
	drawSetH: 30,

	// mouse event
	
	ColorEnter: '#2874A6',
	ColorLeave: '#154360',
	ColorClick: '#1F618D',
	ColorHide:	'#000',

	OpacityEnter: '1.0',
	OpacityLeave: '1.0',
	OpacityClick: '1.0',
	OpacityHide:  '0'
};

var appCfg =
{
	// version

	cfgVersion: '1.5.0',
	
	// background color

	bgColor: '#FAFAFA',

	// used icon size

	titleIconH: appBaseCfg.iconH,

	toolBarIconW: appBaseCfg.iconW,
	toolBarIconH: appBaseCfg.iconH,

	modeIconW: 120,
	modeIconH: appBaseCfg.iconH,

	// common mouse event config

	mouseColorEnter: appBaseCfg.ColorEnter,
	mouseColorLeave: appBaseCfg.ColorLeave,
	mouseColorClick: appBaseCfg.ColorClick,

	mouseOpacityEnter: appBaseCfg.OpacityEnter,
	mouseOpacityLeave: appBaseCfg.OpacityLeave,
	mouseOpacityClick: appBaseCfg.OpacityClick,

	// drawing config

	drawSetH: appBaseCfg.drawSetH,

	drawSetBackground: appBaseCfg.ColorLeave,
	drawSetOpacity: appBaseCfg.OpacityLeave,

	// drawing config color select bar type 1

	colorIconW: 30,
	colorIconH: appBaseCfg.drawSetH,
	colorIconMarginClick: 3,
	colorIconMarginNormal: 6,
	colorIconMarginOver: 4,
	colorIconOpacity: '1.0',

	// drawing config color select bar type 2

	cb2Apply: 1,

	cb2BlockW: 30,
	cb2BlockH: appBaseCfg.drawSetH,
	cb2FrameGap: 6,
	cb2MouseColorEnter: appBaseCfg.ColorEnter,
	cb2MouseColorLeave: appBaseCfg.ColorLeave,
	cb2MouseColorClick: appBaseCfg.ColorClick,

	// scroll bar common set

	scrollIconW: 30,
	scrollIconH: appBaseCfg.drawSetH,
	scrollIconGap: 0,
	scrollTickH: 12,
	scrollLineH: 2,
	scrollColor: "#F8F9F9",
	scrollGroupGap: 8,

	// drawing config opacity scroll bar

	scrollOpacityW: 90,
	scrollOpacityDefPos: 80,

	// drawing config line width scroll bar
	
	scrollLineWidthW: 90,
	scrollLineWidthDefPos: 30,

	lineWidthMin: 4,
	lineWidthMax: 40,

	// scroll bar dialog

	scrollDlgBkg: appBaseCfg.ColorLeave,
	scrollDlgOpacity: appBaseCfg.OpacityLeave,

	// brightness scroll bar

	scrollBrighnessW: 180,
	scrollBrighnessDefPos: 90,

	brightnewwMin: -128,
	brightnewwMax: 128,

	whiteBalanceMin: -25,
	whiteBalanceMax: 25,

	// zoom scroll bar

	scrollZoomW: 180,
	scrollZoomDefPos: 0,

	zoomMaxRatio: 8,

	// pan window

	panWinGapX: 4,
	panWinGapY: 4,
	panWinColor: appBaseCfg.ColorLeave,
	panCropColor: '#555',
	panOpacity: appBaseCfg.OpacityLeave, 

	// setting dialog

	sdBkg: appBaseCfg.ColorLeave,
	sdOpacity: appBaseCfg.OpacityLeave,
	
	sdIconW: appBaseCfg.iconW,
	sdIconH: appBaseCfg.iconH,

	sdExtraW: 14,

	sdIconScrollGap: 4,
	sdGroupGap: 0,
	sdScrollGap: 8,
	sdFrameGapX: 4,
	sdFrameGapY: 4,
	sdScrollH: 30,

	sdVideoRsoW: 120,
	sdFontFamilyW: 120,
	sdFontStyleW: 120,
	sdFontSizeW: 60,

	// text input dialog

	tidBkg: appBaseCfg.ColorLeave,
	tidOpacity: appBaseCfg.OpacityLeave,
	
	tidIconW: appBaseCfg.iconW,
	tidIconH: appBaseCfg.iconH,
	tidTextFrmW: 300,
	tidTextFrmH: 200,
	tidFrameGap: 8,
	tidIconFrmGap: 4,

	tidTextSize: 20,
	
	// drawing tool

	eraserOpacity: 0.8,

	minArrowSize: 10,

	// tool bar 

	toolBarColorEnter: appBaseCfg.ColorEnter,
	toolBarColorLeave: appBaseCfg.ColorLeave,
	toolBarColorClick: appBaseCfg.ColorClick,
	toolBarColorHide:  appBaseCfg.ColorHide,

	toolBarOpacityEnter: appBaseCfg.OpacityEnter,
	toolBarOpacityLeave: appBaseCfg.OpacityLeave,
	toolBarOpacityClick: appBaseCfg.OpacityClick,
	toolBarOpacityHide:	 appBaseCfg.OpacityHide,

	// import picture window

	ipwColor: '#AAA',
	ipwOpacity: 1,
	ipwLineWidth: 6,
	ipwRadius: 16,

	ipwRadiusExpandRatio: 1,

	// video stream

	defPreviewRsoX: 1280,
	defPreviewRsoY: 720,

	// drawing board 

	defDrawingBoardW: 1920,
	defDrawingBoardH: 1080,

	drawingBoardBgColor: '#FFF',

	// playback

	defPictureW: 1920,
	defPictureH: 1080,

	thumbW: 160,
	thumbH: 120,
	thumbPadding: 10,
	thumbFrmSize: 4,
	thumbFrmColorNormal: '#000',
	thumbFrmColorChecked: '#FF0',

	pbInfoW: 140,
	pbInfoH: 24,
	pbInfoColor: '#FF0',
	pbInfoFontSize: '200%',
	pbInfoFontWWeight: 'normal',

	// about

	aboutWinW: 400,
	aboutWinH: 340,
	aboutBkg: '#154360',
	aboutOpacity: 1,
	aboutIconW: appBaseCfg.iconW,
	aboutIconH: appBaseCfg.iconH,
	aboutGapX: 30,

	// periodic capture dialog

	pcdBkg: appBaseCfg.ColorLeave,
	pcdOpacity: appBaseCfg.OpacityLeave,
	
	pcdIconOverlayY: 20,
	pcdIconW: appBaseCfg.iconW,
	pcdIconH: appBaseCfg.iconH,
	pcdTextW: 80,
	pcdTextH: 20,
	pcdUnitW: 60,
	pcdUnitH: 16,
	pcdUnitOffsetY: 4,
	pcdExtraW: 12,

	pcdCapturePeriodSec: 1,
	pcdCaptureIntervalHour: 1,

	// periodic capture info bar
	
	pcibBkg: appBaseCfg.ColorLeave,
	pcibOpacity: appBaseCfg.OpacityLeave,

	pcibIconW: appBaseCfg.iconW,
	pcibIconH: appBaseCfg.iconH,

	pcibTextH: 24,
	pcibTextW: 120,
	pcibTextColor: '#2F2',
	pcibTextFontSize: '150%',

	pcidGapY: 4,

	// confirm dialog

	cdIconW: appBaseCfg.iconW,
	cdIconH: appBaseCfg.iconH,

	cdFrameGap: 26,
	cdIconGap: 10,

	cdBkg: appBaseCfg.ColorLeave,
	cdOpacity: appBaseCfg.OpacityLeave,

	cdMouseColorEnter: appBaseCfg.ColorEnter,
	cdMouseColorLeave: appBaseCfg.ColorLeave,
	cdMouseColorClick: appBaseCfg.ColorClick,

	// sys info window
	
	sysInfoWinW: 88,
	sysInfoWinH: 82,
	sysInfoH: 16,
	sysInfoGap: 4,

	// drawing canvas overlay

	disableDrawingCanvasOverlay: 0,

	// disable UI component
	
	disableDrawingToolBar: 0,
	disableColorBar: 0,
	disableDrawingOpacityScrollBar: 0,
	disableDrawingLineWidthScrollBar: 0,

	disableDrawTextSetting: 0,
};

var drawingToolBarData =
[
    // element id          pic filename             selected	group id
	[ "btn_freehand",      "freeHand.png",			0,			1 ],
	[ "btn_arrow",         "arrow.png",				0,			1 ],
	[ "btn_eraser",        "ereser.png",			0,			1 ],
	[ "btn_line",          "line.png",				0,			1 ],
	[ "btn_rectangleLine", "rectangleLine.png",		0,			1 ],
	[ "btn_rectangle",     "rectangle.png",			0,			1 ],
	[ "btn_circleLine",    "circleLine.png",		0,			1 ],
	[ "btn_circle",        "circle.png",			0,			1 ],
	[ "btn_erase_all",     "clearAll.png",			0,			0 ],
	[ "btn_text",          "text.png",				0,			1 ],
];

var drawingToolBarDataEmpty =
[
];

var bottomToolBarData =
[
    // element id          pic filename             selected	group id
	[ "btn_record",       "record.png",	        	0,			0 ],
	//[ "btn_pause",		  "pause.png",				0,			0 ]
]

var previewToolBarData =
[
    // element id          pic filename             selected	group id
    [ "btn_setting",       "setting.png",	        0,			0 ],
    [ "btn_capture",       "capture.png",	        0,			0 ],
    [ "btn_freeze",        "Freeze.png",	        0,			3 ],
    [ "btn_timesave",      "timesave.png",	        0,			0 ],
    [ "btn_mirrow",        "mirrow.png",	        0,			5 ],
	[ "btn_flip",          "flip.png",	            0,			6 ],
	[ "btn_config",  	   "config.png",		    0,			0 ],
	//[ "btn_brightness",    "brightness.png",	    0,			0 ],
	//[ "btn_whitebalance",  "wb.png",        	    0,			0 ],
    [ "btn_zoom",          "zoom.png",	            0,			0 ],
	[ "btn_importPic",     "inportPic.png",		    0,			0 ],
    [ "btn_redo",          "redo.png",	            0,			0 ],
	[ "btn_undo",          "undo.png",	            0,			0 ],
];

var modeToolBarData =
[
    // element id          pic filename             selected	group id
	[ "mode_about",        "about.png",				0,			0 ],
	[ "mode_preview",      "preview.png",			1,			1 ],
	[ "mode_playback",     "playback.png",			0,			1 ],
	[ "mode_drawingBoard", "drawingboard.png",		0,			1 ],
	//[ "btn_record",        "record.png",	            0,			0 ],
];

var sysToolBarData =
[
    // element id          pic filename             selected	group id
	[ "sys_close",         "close.png",			    0,			1 ],
	[ "sys_fullScreen",    "fullScreen.png",		0,			1 ],
	[ "sys_fullPhoto",     "fullPhoto.png",		    0,			1 ],
	[ "sys_smallScreen",   "screenSmall.png",		0,			1 ],
];

var thumbnailToolBarData =
[
    // element id          pic filename             selected	group id
	[ "btn_saveDisk",      "saveDisk.png",			0,			0 ],
	//[ "btn_uploadCloud",   "uploadCloud.png",		0,			0 ],
	[ "btn_delete",        "delete.png",		    0,			0 ],
	[ "btn_toggleAll",     "select_all_check.png",  0,			0 ],
];

var picPlaybackToolBarData =
[
    // element id                   pic filename                selected	group id
	[ "ppbtn_savePlayback",         "savePlayback.png",		    0,			0 ],
	[ "ppbtn_closeAndBackPlayback", "closeAndBackPlayback.png", 0,			0 ],
    [ "ppbtn_zoom",                 "zoom.png",	                0,			0 ],
	[ "ppbtn_importPic",            "inportPic.png",		    0,			0 ],
	[ "ppbtn_redo",                 "redo.png",		            0,			0 ],
	[ "ppbtn_undo",                 "undo.png",		            0,			0 ],
	[ "ppbtn_transform",            "perspectiveImage.png",     0,			0 ],
	[ "ppbtn_ocr",            		"ocr.png",	        		0,			0 ],
];

var drawingBoardToolBarData =
[
    // element id                   pic filename                selected	group id
	[ "dbbtn_savePlayback",         "savePlayback.png",		    0,			0 ],
	[ "dbbtn_redo",                 "redo.png",		            0,			0 ],
	[ "dbbtn_undo",                 "undo.png",		            0,			0 ],
	[ "dbbtn_importPic",            "inportPic.png",		    0,			0 ],
];

var colorBarData = 
[
	// color    	selected
	[ '782D91',		0 ],
	[ '1B1464',		0 ],
	[ '0000FF',		1 ],
	[ '00FF00',		0 ],
	[ 'FFFF00',		0 ],
	[ 'F15A24',		0 ],
	[ 'FF0000',		0 ],
	[ 'FFFFFF',		0 ],
	[ '999999',		0 ],
	[ '000000',		0 ],
];

var colorBarDataEmpty = 
[
];

var fontFamilyData =
[
	[ "Arial",			1 ],
	[ "Serif",			0 ],
	[ "Verdana",		0 ],
	[ "Courier New",	0 ],
];

var fontStyleData =
[
	[ "Normal",			1 ],
	[ "Bold",			0 ],
	[ "Italic",			0 ],
	[ "Bold Italic",	0 ],
];

var fontSizeData =
[
	[ 8,	0 ],
	[ 12,	0 ],
	[ 16,	0 ],
	[ 20,	0 ],
	[ 24,	0 ],
	[ 28,	1 ],
	[ 36,	0 ],
	[ 48,	0 ],
	[ 72,	0 ],
];

