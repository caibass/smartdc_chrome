
'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// variable
///////////////////////////////////////////////////////////////////////////////////////////

var imageDisplayCanvas;
var imageDisplayContext;

var dispW;
var dispH;

var dispX;
var dispY;


///////////////////////////////////////////////////////////////////////////////////////////
// create element
///////////////////////////////////////////////////////////////////////////////////////////

function updateCanvasSize(id, w, h)
{
	var css =
	{
		position: 'absolute',
		left: 0,
		top: 0,
		width: w,
		height: h,
		display: false,
	};

	var selector = '#' + id;

	$(selector).css(css);

    $(selector).attr('width', w);
    $(selector).attr('height', h);
}

function createImageCanvas(parentId, childId, w, h)
{
	createCanvasElement(parentId, childId);

	updateCanvasSize(childId, w, h)
}

function hideCanvasGroup(parentId)
{
	var canvasTbl = [
		'baseImageCanvas',
		'imageProcessCanvas',
		'drawingCanvas',
		'tempDrawingCanvas',
		'combineCanvas',
	];

	var i;
	var seperator = '-';
	var childId;
	var eleId;

	for ( i = 0; i < canvasTbl.length; i++ )
	{
		childId = parentId + seperator + canvasTbl[i];

		eleId = document.getElementById(childId);
		eleId.hidden = true;
	}
}

function createCanvasGroup(parentId, w, h)
{
	var seperator = '-';
	var childId;

	var canvasTbl = [
		'baseImageCanvas',
		'imageProcessCanvas',
		'drawingCanvas',
		'tempDrawingCanvas',
		'combineCanvas',
	];

	var i;

	for ( i = 0; i < canvasTbl.length; i++ )
	{
		childId = parentId + seperator + canvasTbl[i];
		createImageCanvas(parentId, childId, w, h);
	}
}

function resetCanvasGroupSize(modeCfg, w, h)
{
	var seperator = '-';
	var childId;
	var eleId;
	var i;

	var canvasTbl = [
		'baseImageCanvas',
		'imageProcessCanvas',
		'drawingCanvas',
		'tempDrawingCanvas',
		'combineCanvas',
	];

	for ( i = 0; i < canvasTbl.length; i++ )
	{
		childId = modeCfg.baseId + seperator + canvasTbl[i];
		updateCanvasSize(childId, w, h);
	}
}

function getCanvasGroupContext(modeCfg)
{
	var seperator = '-';
	var childId;
	var eleId;

	var canvasTbl = [
		'baseImageCanvas',
		'imageProcessCanvas',
		'drawingCanvas',
		'tempDrawingCanvas',
		'combineCanvas',
	];

	var contextTbl = [
		'baseImageContext',
		'imageProcessContext',
		'drawingContext',
		'tempDrawingContext',
		'combineContext',
	];

	var i;

	for ( i = 0; i < canvasTbl.length; i++ )
	{
		childId = modeCfg.baseId + seperator + canvasTbl[i];
		modeCfg[canvasTbl[i]] = document.getElementById(childId);
		modeCfg[contextTbl[i]] = modeCfg[canvasTbl[i]].getContext('2d');
	}
}

(function()
{
	// create base area 
	
	createDocumentElement('div', 'baseArea');

	// create title bar 
	
	createDivElement('baseArea', 'appTitle');

	// create preview area

	createDivElement('baseArea', 'previewArea');

	createElement('video', 'previewArea', 'videoSrc');

	createCanvasGroup('previewArea', appCfg.defPreviewRsoX, appCfg.defPreviewRsoY);

	// create playback area

	createDivElement('baseArea', 'playbackArea');

	createCanvasGroup('playbackArea', appCfg.defPictureW, appCfg.defPictureH);

	// create drawing area

	createDivElement('baseArea', 'drawingArea');

	createCanvasGroup('drawingArea', appCfg.defDrawingBoardW, appCfg.defDrawingBoardH);

	// final image display area

	createDivElement('baseArea', 'imageDisplayArea');

	createCanvasElement('imageDisplayArea', 'displayCanvas');

	createCanvasElement('imageDisplayArea', 'saveFileCanvas');

	// UI display area

	createDivElement('baseArea', 'uiArea');

	createSpanElement('uiArea', 'importPictureWin');

	createDivElement('uiArea', 'drawingCfgArea');
	setDrawingCfgArea();

	createSpanElement('uiArea', 'panWin');
	createCanvasElement('panWin', 'cropWin');

	createSpanElement('uiArea', 'settingDlg');

	createSpanElement('uiArea', 'textInputDlg');

	// asign common use variable

	imageDisplayCanvas = document.getElementById('displayCanvas');
	imageDisplayContext = imageDisplayCanvas.getContext('2d');

	// hide canvas group

	hideCanvasGroup('previewArea');
	hideCanvasGroup('playbackArea');
	hideCanvasGroup('drawingArea');

	// hide save file canvas

	showElement('saveFileCanvas', false);
	
})();


///////////////////////////////////////////////////////////////////////////////////////////
// body
///////////////////////////////////////////////////////////////////////////////////////////

function cfgBody()
{
	$('body').css('background-color', appCfg.bgColor);
}


///////////////////////////////////////////////////////////////////////////////////////////
// title bar
///////////////////////////////////////////////////////////////////////////////////////////

function updateTitleBarSize()
{
	titleW = winW;
	titleH = appCfg.titleIconH;
}

function cfgTitleBar()
{
	var cssCfg =
	{
		position: 'absolute',
	    background: appCfg.mouseColorLeave,
    	padding: '0 0 0 0px',
	    top: '0px',
	    height: appCfg.titleIconH + 'px',
	    opacity: appCfg.mouseOpacityLeave,
	}

	var x;
	var w;

	var modeBarW;
	var sysBarW;

	modeBarW = appCfg.modeIconW * modeToolBarData.length;
	sysBarW = appCfg.toolBarIconW * sysToolBarData.length;

	cssCfg.left = modeBarW + 'px';
	cssCfg.width = 'calc(100% - ' + (modeBarW + sysBarW) + 'px)';

	$('#appTitle').css(cssCfg);

	$('#appTitle').css('-webkit-app-region', 'drag');
}


///////////////////////////////////////////////////////////////////////////////////////////
// adjust size
///////////////////////////////////////////////////////////////////////////////////////////

var cssCommonSizeObj =
{
	position: 'absolute',
	left: 0,
	top: 0,
	width: 800,
	height: 260,
	style: 'border:1px dashed black',
	background: appCfg.bgColor,
}

function adjImageDisplayArea()
{
	var id$;
	var totalW, totalH;
	var x, y, w, h;

	if (fullPhotoMode)
	{
		totalW = winW;
		totalH = winH;
	}
	else
	{

		totalW = winW - appBaseCfg.iconW*2;
		totalH = winH - titleH - appBaseCfg.drawSetH;
	}
	

	if ( totalW * appFC.imgH < totalH * appFC.imgW )	
	{
		// use full display width

		w = totalW;
		x = (winW - totalW) / 2;
		//x = 0;

		h = Math.floor(w * appFC.imgH / appFC.imgW);
		y = Math.floor((totalH - h) / 2);
	}
	else
	{
		// use full display height

		h = totalH;
		y = 0;

		w = Math.floor(h * appFC.imgW / appFC.imgH);
		x = Math.floor((totalW - w) / 2) + appBaseCfg.iconW;
	}

	if (!fullPhotoMode)
		y += titleH;

	cssCommonSizeObj.left = x;
	cssCommonSizeObj.top = y;

	cssCommonSizeObj.width = w;
	cssCommonSizeObj.height = h;

	id$ = $('#displayCanvas');

	id$.css(cssCommonSizeObj);
	
    id$.attr('width', w);
    id$.attr('height', h);

	dispX = x;
	dispY = y;

	dispW = w;
	dispH = h;
}


///////////////////////////////////////////////////////////////////////////////////////////
// tool bar
///////////////////////////////////////////////////////////////////////////////////////////

var nullToolBarData = [];

function initToolBar()
{
	var cssToolBar =
	{
		position: 'absolute',
		width: appCfg.toolBarIconW,
		height: appCfg.toolBarIconH,
		'margin-left': '0px',
	};

	cssToolBar.width = appCfg.toolBarIconW;
	cssToolBar.height = appCfg.toolBarIconH;

	createToolBar(nullToolBarData, cssToolBar, 'uiArea', 'playbackRToolBar', 'right');

	if ( 1 === appCfg.disableDrawingToolBar )
	{
		createToolBar(drawingToolBarDataEmpty, cssToolBar, 'uiArea', 'drawingToolBar', 'right', drawingToolBarOnClick);
		updateToolBar(drawingToolBarDataEmpty);
	}
	else
	{
		createToolBar(drawingToolBarData, cssToolBar, 'uiArea', 'drawingToolBar', 'right', drawingToolBarOnClick);
		updateToolBar(drawingToolBarData);
	}

	createToolBar(thumbnailToolBarData, cssToolBar, 'uiArea', 'thumbnailToolBar', 'left', thumbnailToolBarOnClick);
	updateToolBar(thumbnailToolBarData);

	createToolBar(picPlaybackToolBarData, cssToolBar, 'uiArea', 'picPlaybackToolBar', 'left', picturePlaybackToolBarOnClick);
	updateToolBar(picPlaybackToolBarData),

	createToolBar(drawingBoardToolBarData, cssToolBar, 'uiArea', 'drawingBoardToolBar', 'left', drawingBoardToolBarOnClick);
	updateToolBar(drawingBoardToolBarData);

	createToolBar(previewToolBarData, cssToolBar, 'uiArea', 'previewToolBar', 'left', previewToolBarOnClick);
	updateToolBar(previewToolBarData);

	createToolBar(sysToolBarData, cssToolBar, 'uiArea', 'sysToolBar', 'top-right', sysToolBarOnClick);
	updateToolBar(sysToolBarData);

	cssToolBar.width = appCfg.modeIconW/2;

	createToolBar(bottomToolBarData, cssToolBar, 'uiArea', 'bottomToolBarData', 'bottom', previewToolBarOnClick);
	updateToolBar(bottomToolBarData);
	$("#btn_pause").hide();

	cssToolBar.width = appCfg.modeIconW;
	cssToolBar.height = appCfg.modeIconH;

	createToolBar(modeToolBarData, cssToolBar, 'uiArea', 'modeToolBar', 'top-left', modeToolBarOnClick);
	updateToolBar(modeToolBarData);
}

function hideAllToolBars()
{
	var idTbl = [
			'playbackRToolBar',
			'previewToolBar',
			'drawingToolBar',
			'thumbnailToolBar',
			'picPlaybackToolBar',
			'drawingBoardToolBar',
		];

	var i;

	for ( i = 0; i < idTbl.length; i++ )
	{
		showToolBar(idTbl[i], false);
	}
}

function showToolBar(id, mode)
{
	showElement(id, mode);
}


///////////////////////////////////////////////////////////////////////////////////////////
// drawing config area
///////////////////////////////////////////////////////////////////////////////////////////

function setDrawingCfgArea()
{
	var css =
	{
		position: 'absolute',
		'margin-left': '0px',
		background: appCfg.drawSetBackground,
		opacity: appCfg.drawSetOpacity,
	};

	var w = appCfg.toolBarIconW;
	var h = appCfg.drawSetH;

	css.width = 'calc(100% - ' + (w * 2) + 'px)';
	css.height = h + 'px';
	css.left = w + 'px';
	css.top = 'calc(100% - ' + h + 'px)';
	
	$('#drawingCfgArea').css(css);
}


///////////////////////////////////////////////////////////////////////////////////////////
// color bar
///////////////////////////////////////////////////////////////////////////////////////////

function initColorBar()
{
	var cssColorBar =
	{
		position: 'absolute',
		width: appCfg.colorIconW,
		height: appCfg.colorIconH,
		'margin-left': '0px',
		opacity: appCfg.colorIconOpacity,
	};

	if ( 1 === appCfg.disableColorBar )
	{
		createColorBar(colorBarDataEmpty, cssColorBar, 'drawingCfgArea', 'drawingCfgColorBar', fcColorBarNotify);
		updateColorBar(colorBarDataEmpty);
	}
	else
	{
		if ( appCfg.cb2Apply )
		{
			createColorBarType2(colorBarData, cssColorBar, 'drawingCfgArea', 'drawingCfgColorBar', fcColorBarNotify);
			updateColorBarType2(colorBarData);
		}
		else
		{
			createColorBar(colorBarData, cssColorBar, 'drawingCfgArea', 'drawingCfgColorBar', fcColorBarNotify);
			updateColorBar(colorBarData);
		}
	}
}

function fcColorBarNotify(color, r, g, b)
{
	appFC.colorHex = color;
	appFC.colorR = r;
	appFC.colorG = g;
	appFC.colorB = b;

	fcUpdateDrawStyle();
}


///////////////////////////////////////////////////////////////////////////////////////////
// scroll bar
///////////////////////////////////////////////////////////////////////////////////////////

function initScrollBar()
{

	var w = 0;

	var cssScrollBar =
	{
		position: 'absolute',
		'margin-left': '0px',
	};

	// for opacity

	var scrollBarOpacity =
	{
		iconL: '30x_30alpha.png',
		iconSize: appCfg.scrollIconH,
		length: appCfg.scrollOpacityW,
		curPos: appCfg.scrollOpacityDefPos,
		startX: 0,
		startY: 0,
		color: appCfg.scrollColor,
	};

	if ( 0 === appCfg.disableDrawingOpacityScrollBar )
	{
		w = createScrollBar(scrollBarOpacity, cssScrollBar, 'drawingCfgArea', 'drawingOpacity', opacityScrollBarNotify);
		updateScrollBar('drawingOpacity');
	}

	// for line width

	var scrollBarLineWidth =
	{
		iconL: '30x30lineWidth.png',
		iconSize: appCfg.scrollIconH,
		length: appCfg.scrollLineWidthW,
		curPos: appCfg.scrollLineWidthDefPos,
		startX: 0,
		startY: 0,
		color: appCfg.scrollColor,
	};

	scrollBarLineWidth.startX = w + appCfg.scrollGroupGap;

	if ( 0 === appCfg.disableDrawingLineWidthScrollBar )
	{
		createScrollBar(scrollBarLineWidth, cssScrollBar, 'drawingCfgArea', 'drawingLineWidth', lineWidthScrollBarNotify);
		updateScrollBar('drawingLineWidth');
	}
}

function opacityScrollBarNotify(length, pos)
{
	appFC.opacity = 0.1 + (pos/length)*0.9;

	appFC.opacity = appFC.opacity.toFixed(1);

	fcUpdateDrawStyle();
}

function lineWidthScrollBarNotify(length, pos)
{
	appFC.lineWidth = (appCfg.lineWidthMin + (appCfg.lineWidthMax - appCfg.lineWidthMin)*(pos/length));

	appFC.lineWidth = Math.floor(appFC.lineWidth);

	//console.log("line width: " + appFC.lineWidth);
}

///////////////////////////////////////////////////////////////////////////////////////////
// scroll bar dialog
///////////////////////////////////////////////////////////////////////////////////////////

function initScrollBarDlg()
{

	var w;

	var cssScrollBar =
	{
		position: 'absolute',
		'margin-left': '0px',
	};

	// for brightness

	var scrollBarBrightness =
	{
		iconL: 'brightness_down.png',
		iconR: 'brightness_up.png',
		iconSize: appCfg.toolBarIconW,
		length: appCfg.scrollBrighnessW,
		curPos: appCfg.scrollBrighnessDefPos,
		startX: 0,
		startY: 0,
		color: appCfg.scrollColor,
	};

	createScrollBarDlg(scrollBarBrightness, cssScrollBar, 'uiArea', 'brightnessDlg', brightnessScrollBarNotify, true);
	showBrightnessDlg(false);

	// for white balance

	var scrollWhiteBalance =
	{
		iconL: 'wb_b.png',
		iconR: 'wb_r.png',
		iconAuto: 'undo.png',
		iconSize: appCfg.toolBarIconW,
		length: appCfg.scrollBrighnessW,
		curPos: appCfg.scrollBrighnessDefPos,
		startX: 0,
		startY: 0,
		color: appCfg.scrollColor,
	};

	createScrollBarDlg(scrollWhiteBalance, cssScrollBar, 'uiArea', 'whiteBalanceDlg', whiteBalanceScrollBarNotify, true);
	showWhiteBalanceDlg(false);

	// for zoom
	
	var scrollBarZoom =
	{
		iconL: 'zoom_in.png',
		iconR: 'zoom_out.png',
		iconSize: appCfg.toolBarIconW,
		length: appCfg.scrollZoomW,
		curPos: appCfg.scrollZoomDefPos,
		startX: 0,
		startY: 0,
		color: appCfg.scrollColor,
	};

	createScrollBarDlg(scrollBarZoom, cssScrollBar, 'uiArea', 'zoomDlg', zoomScrollBarNotify);
	showZoomDlg(false);
}

function showBrightnessDlg(mode)
{
	showElement('brightnessDlg', mode);
}

function showWhiteBalanceDlg(mode)
{
	showElement('whiteBalanceDlg', mode);
}

function showZoomDlg(mode)
{
	showElement('zoomDlg', mode);
}

function brightnessScrollBarNotify(length, pos)
{
	var brightness;

	brightness = appCfg.brightnewwMin + (appCfg.brightnewwMax - appCfg.brightnewwMin)*pos/length;

	brightness = Math.floor(brightness);

	appFC.previewBrightnessAdj = brightness;

	if ( appFC.curMode )
	{
		fcCanvasCtrl(DrawState.brightness);
	}
}

function whiteBalanceScrollBarNotify(length, pos)
{
	var wb;
	wb = appCfg.whiteBalanceMin + (appCfg.whiteBalanceMax - appCfg.whiteBalanceMin)*pos/length;
	wb = Math.floor(wb);
	
	appFC.previewWhiteBalanceAdj = wb;

	if ( appFC.curMode )
	{
		fcCanvasCtrl(DrawState.whiteBalance);
	}
}

function zoomScrollBarNotify(length, pos)
{
	var zoom;

	zoom = 1 + (appCfg.zoomMaxRatio - 1)*pos/length;

	zoom = zoom.toFixed(1);

	if ( appFC.curMode )
	{
		fcUpdateZoomRatio(zoom, pos);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// setting dialog
///////////////////////////////////////////////////////////////////////////////////////////

function initSettingDlg()
{
	var dlgCss =
	{
		position: 'absolute',
		'margin-left': '0px',
		background: appCfg.sdBkg,
		opacity: appCfg.sdOpacity,
	};

	var totalW, totalH;
	var curX, curY;

	var noSettingLine = 2;

	if ( 1 === appCfg.disableDrawTextSetting )
	{
		noSettingLine = 1;
	}

	// set window size

	totalW = appCfg.sdFrameGapX*2 + appCfg.sdIconW + appCfg.sdIconScrollGap + appCfg.sdFontFamilyW + appCfg.sdFontStyleW + appCfg.sdFontSizeW + appCfg.sdScrollGap*2;
	totalW += appCfg.sdExtraW;
	totalH = appCfg.sdFrameGapY*noSettingLine + appCfg.sdIconH*(noSettingLine+1) + appCfg.sdGroupGap;

	dlgCss.left = 'calc(50% - ' + (totalW/2) + 'px)';
	dlgCss.top = 'calc(50% - ' + ((totalH - appCfg.titleIconH)/2) + 'px)';
	dlgCss.width = totalW + 'px';
	dlgCss.height = totalH + 'px';

	$('#settingDlg').css(dlgCss);

	// set close icon

	curX = totalW - appCfg.sdFrameGapX - appCfg.sdIconW;
	curY = appCfg.sdFrameGapY;

	createSettingDlgIcon('settingDlg', 'sdClsoeIcon', 'close.png', curX, curY);

	$('#sdClsoeIcon').on('click', previewSetttingDlgCloseClick);

	curY =+ appCfg.sdIconH;

	// video resolution icon

	curX = appCfg.sdFrameGapX;

	createSettingDlgIcon('settingDlg', 'sdVideoRsoIcon', 'setting_reslution.png', curX, curY);

	curX += (appCfg.sdIconW + appCfg.sdIconScrollGap);

	// video resolution scroll bar

	createVideoResolutionScrollBar('settingDlg', 'sdVideoRsoScrollbar', appFC.curVideoResolutionList, curX, curY, appCfg.sdVideoRsoW);

	curY += (appCfg.sdIconH + appCfg.sdGroupGap);

	// test settings

	if ( 0 === appCfg.disableDrawTextSetting )
	{
		// text icon

		curX = appCfg.sdFrameGapX;

		createSettingDlgIcon('settingDlg', 'sdFontIcon', 'setting_textEdit.png', curX, curY);

		curX += (appCfg.sdIconW + appCfg.sdIconScrollGap);

		// text family scroll bar

		createFontScrollBar('settingDlg', 'sdfontFamily', fontFamilyData, curX, curY, appCfg.sdFontFamilyW);

		curX += (appCfg.sdFontFamilyW + appCfg.sdScrollGap);

		createFontScrollBar('settingDlg', 'fontStyle', fontStyleData, curX, curY, appCfg.sdFontStyleW);
		
		curX += (appCfg.sdFontStyleW + appCfg.sdScrollGap);

		createFontScrollBar('settingDlg', 'fontSize', fontSizeData, curX, curY, appCfg.sdFontSizeW);

		curY += (appCfg.sdIconH + appCfg.sdGroupGap);
	}
	
	// hide dialog

	showSettingDlg(false);

	// update font config

	fcUpdateFontCfg();
}

function createSettingDlgIcon(parentId, id, pic, startX, startY)
{
	var eleId;
	var bkg;

	createSpanElement(parentId, id);

	eleId = '#' + id;

	bkg = fcGetDefIconPath(pic);

	$(eleId).css('background', bkg);
	$(eleId).css('top', startY + 'px');
	$(eleId).css('left', startX + 'px');
	$(eleId).css('width', appCfg.sdIconW + 'px');
	$(eleId).css('height', appCfg.sdIconH + 'px');
	$(eleId).css('position', 'absolute');
}

function updateVideoResolutionElement(data)
{
	cleanVideoResolutionElement();

	createVideoResolutionElement(data);
}

function cleanVideoResolutionElement()
{
	var eleId = "#" + appFC.idVideoResolutionList;

	$(eleId + " > option").remove();
}

function createVideoResolutionElement(data)
{
	var strTotal = "";
	var i;
    var eleId;
	var dispStr;
	var selStr;
	var strElement;
	var id;

	id = appFC.idVideoResolutionList;

	strTotal = '';

	for ( i = 0; i < data.length; i++ )
	{
        eleId = id + '-' + data[i][VrField.w] + 'x' + data[i][VrField.h];
		dispStr = data[i][VrField.w] + ' x ' + data[i][VrField.h];;

		if ( data[i][VrField.data] === 1 )
		{
			selStr = ' selected="true"';

			appFC.reqImgW = data[i][VrField.w];
			appFC.reqImgH = data[i][VrField.h];
		}
		else
		{
			selStr = ''
		}
	
		strElement = '<option' + selStr + ' id="' + eleId + '">' + dispStr +'</option>';
		strTotal += strElement;
	}

	$('#' + id).html(strTotal);
}

function createVideoResolutionScrollBar(parendId, id, data, startX, startY, width)
{
    var eleId;

	appFC.idVideoResolutionList = id;

	createSelectElement(parendId, id);

	createVideoResolutionElement(data);

	eleId = '#' + id;

	$(eleId).css('top', (startY + (appCfg.sdIconH - appCfg.sdScrollH)/2) + 'px');
	$(eleId).css('left', startX + 'px');
	$(eleId).css('width', width + 'px');
	$(eleId).css('height', appCfg.sdScrollH + 'px');
	$(eleId).css('position', 'absolute');
}

function createFontScrollBar(parendId, id, data, startX, startY, width)
{
	var strTotal = "";
	var strElement;
	var i;
    var eleId;
	var dispStr;
	var selStr;
	
	createSelectElement(parendId, id);

	strTotal = '';

	for ( i = 0; i < data.length; i++ )
	{
        eleId = id + '-' + data[i][FontField.value];
		dispStr = data[i][FontField.value];

		if ( data[i][FontField.status] === 1 ) selStr = ' selected="true"';
		else selStr = ''
	
		strElement = '<option' + selStr + ' id="' + eleId + '">' + dispStr +'</option>';
		strTotal += strElement;
	}

	$('#' + id).html(strTotal);

	eleId = '#' + id;

	$(eleId).css('top', (startY + (appCfg.sdIconH - appCfg.sdScrollH)/2) + 'px');
	$(eleId).css('left', startX + 'px');
	$(eleId).css('width', width + 'px');
	$(eleId).css('height', appCfg.sdScrollH + 'px');
	$(eleId).css('position', 'absolute');
}

function showSettingDlg(mode)
{
	showElement('settingDlg', mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// text input dialog
///////////////////////////////////////////////////////////////////////////////////////////

function initTextInputDlg()
{
	var totalW;
	var totalH;
	var eleId;
	var x, y;
	var bkg;
	
	totalW = appCfg.tidFrameGap + appCfg.tidTextFrmW + appCfg.tidFrameGap;
	totalH = appCfg.tidFrameGap + appCfg.tidIconH + appCfg.tidIconFrmGap + appCfg.tidTextFrmH + appCfg.tidFrameGap;

	// set dialog size

	eleId = '#textInputDlg';

	$(eleId).css('top', 'calc(50% - ' + ((totalH - appCfg.titleIconH)/2) + 'px)');
	$(eleId).css('left', 'calc(50% - ' + (totalW/2) + 'px)');
	$(eleId).css('width', totalW + 'px');
	$(eleId).css('height', totalH + 'px');
	
	$(eleId).css('position', 'absolute');
	$(eleId).css('background', appCfg.tidBkg);
	$(eleId).css('opacity', appCfg.tidOpacity);

	// close icon

	createSpanElement('textInputDlg', 'textInputClose');

	eleId = '#textInputClose';

	bkg = fcGetDefIconPath('yes.png');

	x = totalW - appCfg.tidFrameGap - appCfg.tidIconW;
	y = appCfg.tidFrameGap;

	$(eleId).css('background', bkg);
	$(eleId).css('top', y + 'px');
	$(eleId).css('left', x + 'px');
	$(eleId).css('width', appCfg.tidIconW + 'px');
	$(eleId).css('height', appCfg.tidIconH + 'px');
	$(eleId).css('position', 'absolute');

	$(eleId).on('click', textInputDlgCloseClick);

	// text input area

	createTextareaElement('textInputDlg', 'textInputArea');

	eleId = '#textInputArea';

	x = appCfg.tidFrameGap;
	y = appCfg.tidFrameGap + appCfg.tidIconH + appCfg.tidIconFrmGap;

	$(eleId).css('position', 'absolute');
	$(eleId).css('resize', 'none');
	
	$(eleId).css('top', y + 'px');
	$(eleId).css('left', x + 'px');
	$(eleId).css('width', appCfg.tidTextFrmW + 'px');
	$(eleId).css('height', appCfg.tidTextFrmH + 'px');

	$(eleId).css('padding', 0 + 'px');
	
	$(eleId).attr('cols', appCfg.tidTextFrmW);
	$(eleId).attr('rows', appCfg.tidTextFrmH);

	// hide dialog

	showTextInputDlg(false);
}

function showTextInputDlg(mode)
{
	showElement('textInputDlg', mode);

	if ( mode )
	{
		setTimeout(function() {
			$('#textInputArea').focus();
		}, 300);
	}
}

function updateTextInputCfg()
{
	var eleId;
	var color;

	color = 'rgb(' + appFC.colorR + ',' + appFC.colorG + ',' + appFC.colorB + ')';

	eleId = '#textInputArea';

	$(eleId).css('font', appCfg.tidTextSize + 'px' + ' ' + appFC.fontFamily);
	$(eleId).css('color', color);

	if ( -1 != appFC.fontStyle.indexOf("Bold") )
	{
		$(eleId).css('font-weight', 'bold');

		appFC.fontBold = true;
	}
	else
	{
		$(eleId).css('font-weight', 'normal');

		appFC.fontBold = false;
	}
	
	if ( -1 != appFC.fontStyle.indexOf("Italic") )
	{
		$(eleId).css('font-style', 'italic');

		appFC.fontItalic = true;
	}
	else
	{
		$(eleId).css('font-style', 'normal');

		appFC.fontItalic = false;
	}
}

function resetTextInputContent()
{
	var eleId;

	eleId = document.getElementById('textInputArea');
	
	eleId.value = '';
}

function getTextInputContent()
{
	var eleId;

	eleId = document.getElementById('textInputArea');
	
	return eleId.value;
}

function getTextInputLength()
{
	var eleId;

	eleId = document.getElementById('textInputArea');
	
	return eleId.value.length;
}

///////////////////////////////////////////////////////////////////////////////////////////
// about dialog   
///////////////////////////////////////////////////////////////////////////////////////////

function initAboutDlg()
{
	var baseId = 'aboutDlg';
	var closeId = baseId + '-' + 'close';
	var contentId = baseId + '-' + 'content';
	var eleId;
	var bkg;

	// create element
	
	createDivElement('uiArea', baseId);
	createDivElement(baseId, closeId);
	createDivElement(baseId, contentId);

	// set window

	eleId = '#' + baseId;

	$(eleId).css('top', 'calc(50% - ' + ((appCfg.aboutWinH - appCfg.titleIconH)/2) + 'px)');
	$(eleId).css('left', 'calc(50% - ' + (appCfg.aboutWinW/2) + 'px)');
	$(eleId).css('width', appCfg.aboutWinW + 'px');
	$(eleId).css('height', appCfg.aboutWinH + 'px');
	
	$(eleId).css('position', 'absolute');
	$(eleId).css('background', appCfg.aboutBkg);
	$(eleId).css('opacity', appCfg.aboutOpacity);

	// set close icon

	eleId = '#' + closeId;

	bkg = fcGetDefIconPath('close.png');

	$(eleId).css('background', bkg);
	$(eleId).css('top', '0' + 'px');
	$(eleId).css('left', 'calc(100% - ' + appCfg.aboutIconW + 'px)');
	$(eleId).css('width', appCfg.aboutIconW + 'px');
	$(eleId).css('height', appCfg.aboutIconH + 'px');
	$(eleId).css('position', 'absolute');

	$(eleId).click( function () {
		fcCloseActiveDlg();
	});

	// set content area

	eleId = '#' + contentId;

	$(eleId).css('top', appCfg.aboutIconH + 'px');
	$(eleId).css('left', appCfg.aboutGapX + 'px');
	$(eleId).css('width', (appCfg.aboutWinW - 2*appCfg.aboutGapX) + 'px');
	$(eleId).css('height', (appCfg.aboutWinH - appCfg.aboutIconH) + 'px');
	$(eleId).css('position', 'absolute');

	$(eleId).html(aboutContent);

	// set company link

	eleId = '#companyLink';

	$(eleId).click( function () {
		window.open(aboutCompanyLink);
	});

	// set manual link

	eleId = '#manualLink';

	$(eleId).click( function () {
		window.open(aboutManualLink);
	});

	// hide dialog

	showElement(baseId, false);
}

function aboutDlgOpen()
{
	showElement('aboutDlg', true);
}

function aboutDlgClose()
{
	showElement('aboutDlg', false);
}

///////////////////////////////////////////////////////////////////////////////////////////
// periodic capture dialog
///////////////////////////////////////////////////////////////////////////////////////////

function setPeriodicCaptureDlgElement(id, pic, startX, startY, width, height, hasPic)
{
	var eleId;
	var bkg;

	eleId = '#' + id;

	if ( hasPic )
	{
		bkg = fcGetDefIconPath(pic);
		$(eleId).css('background', bkg);
	}
	
	$(eleId).css('top', startY + 'px');
	$(eleId).css('left', startX + 'px');
	$(eleId).css('width', width + 'px');
	$(eleId).css('height', height + 'px');
	$(eleId).css('position', 'absolute');
}

function createPeriodicCaptureDlgIcon(parentId, id, pic, startX, startY)
{
	createSpanElement(parentId, id);

	setPeriodicCaptureDlgElement(id, pic, startX, startY, appCfg.pcdIconW, appCfg.pcdIconH, true);
}

function createPeriodicCaptureDlgUnit(parentId, id, startX, startY, text)
{
	createLabelElement(parentId, id);

	setPeriodicCaptureDlgElement(id, false, startX, startY + appCfg.pcdUnitOffsetY, appCfg.pcdUnitW, appCfg.pcdUnitH, false);

	$('#' + id).css('text-align', 'right');

	periodicCaptureDlgUpdateText(id, text);
}

function createPeriodicCaptureDlgText(parentId, id, startX, startY, text)
{
	var eleId = '#' + id;

	createInputElement(parentId, id);

	setPeriodicCaptureDlgElement(id, false, startX, startY, appCfg.pcdTextW, appCfg.pcdTextH, false);

	$(eleId).keydown(function (e) {
		// Allow: backspace, delete
		if ($.inArray(e.keyCode, [46, 8]) !== -1 ||
		// Allow: home, end, left, right, down, up
		(e.keyCode >= 35 && e.keyCode <= 40)) {
		// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$(eleId).attr('value', text);

	$(eleId).css('text-align', 'center');
}

function periodicCaptureDlgPeriod()
{
	var baseId = 'periodicCaptureDlg';
	var periodText = baseId + '_' + 'periodText';

	return document.getElementById(periodText).value;
}

function periodicCaptureDlgInterval()
{
	var baseId = 'periodicCaptureDlg';
	var intervalText = baseId + '_' + 'intervalText';
	
	return document.getElementById(intervalText).value;
}

function periodicCaptureDlgUpdateText(id, text)
{
	var id;

	id = document.getElementById(id);

	fcUpdateElementText(id, text);
}

function initPeriodicCaptureDlg()
{
	var baseId = 'periodicCaptureDlg';
	
	var periodIcon = baseId + '_' + 'periodIcon';
	var periodText = baseId + '_' + 'periodText';
	var periodUnit = baseId + '_' + 'periodUnit';
	
	var intervalIcon = baseId + '_' + 'intervalIcon';
	var intervalText = baseId + '_' + 'intervalText';
	var intervalUnit = baseId + '_' + 'intervalUnit';
	
	var closeIcon = baseId + '_' + 'close';
	var startIcon = baseId + '_' + 'startIcon';
	
	var eleId;
	var x, y;
	var textOffsetY = (appCfg.pcdIconH - appCfg.pcdTextH)/2;
	var unitOffsetY = (appCfg.pcdIconH - appCfg.pcdUnitH)/2;

	// set dialog size

	createDivElement('uiArea', baseId);

	var totalW;
	var totalH;

	totalW = appCfg.pcdIconW + appCfg.pcdTextW + appCfg.pcdUnitW + appCfg.pcdExtraW;
	totalH = appCfg.pcdIconH*4 - appCfg.pcdIconOverlayY*3;

	eleId = '#' + baseId;

	$(eleId).css('position', 'absolute');
	$(eleId).css('background', appCfg.pcdBkg);
	$(eleId).css('opacity', appCfg.pcdOpacity);

	$(eleId).css('left', 'calc(50% - ' + (totalW/2) + 'px)');
	$(eleId).css('top', 'calc(50% - ' + ((totalH - appCfg.titleIconH)/2) + 'px)');
	$(eleId).css('width', totalW + 'px');
	$(eleId).css('height', totalH + 'px');

	// set close icon

	x = totalW - appCfg.pcdIconW;
	y = 0;

	createPeriodicCaptureDlgIcon(baseId, closeIcon, 'close.png', x, y);

	$('#' + closeIcon).on('click', previewPeriodicCaptureDlgClickClose);

	y += appCfg.pcdIconH - appCfg.pcdIconOverlayY;

	// set periodic element

	x = 0;
	createPeriodicCaptureDlgIcon(baseId, periodIcon, 'timesave_time.png', x, y);

	x += appCfg.pcdIconW;
	createPeriodicCaptureDlgText(baseId, periodText, x, y + textOffsetY, appFC.capturePeriodSec);

	x += appCfg.pcdTextW;
	createPeriodicCaptureDlgUnit(baseId, periodUnit, x, y + unitOffsetY, 'Seconds');

	y += appCfg.pcdIconH - appCfg.pcdIconOverlayY;

	// set interval element

	x = 0;
	createPeriodicCaptureDlgIcon(baseId, intervalIcon, 'timesave_keep.png', x, y);

	x += appCfg.pcdIconW;
	createPeriodicCaptureDlgText(baseId, intervalText, x, y + textOffsetY, appFC.captureIntervalHour);

	x += appCfg.pcdTextW;
	createPeriodicCaptureDlgUnit(baseId, intervalUnit, x, y + unitOffsetY, 'Minutes');

	y += appCfg.pcdIconH - appCfg.pcdIconOverlayY;

	// set start icon

	x = (totalW - appCfg.pcdIconW) / 2;
	
	createPeriodicCaptureDlgIcon(baseId, startIcon, 'timesave_star.png', x, y);

	$('#' + startIcon).on('click', previewPeriodicCaptureRun);

	// hide dialog

	showPeriodicCaptureDlg(false);
}

function showPeriodicCaptureDlg(mode)
{
	showElement('periodicCaptureDlg', mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// periodic capture info bar
///////////////////////////////////////////////////////////////////////////////////////////

function resizePeriodicCaptureInfoBar()
{
	var baseId = 'periodicCaptureInfoBar';
	var totalW, totalH;
	var eleId;
	var y;
	
	y = Math.min((dispY + dispH), (winH - appCfg.drawSetH));
	
	totalW = appCfg.pcibTextW + appCfg.pcibIconW;
	totalH = appCfg.pcibIconH;

	eleId = '#' + baseId;

	$(eleId).css('position', 'absolute');
	$(eleId).css('background', appCfg.pcibBkg);
	$(eleId).css('opacity', appCfg.pcibOpacity);

	$(eleId).css('left', 'calc(50% - ' + (totalW/2) + 'px)');
	$(eleId).css('top', (y - totalH - appCfg.pcidGapY) + 'px');
	$(eleId).css('width', totalW + 'px');
	$(eleId).css('height', totalH + 'px');
}

function initPeriodicCaptureInfoBar()
{
	var baseId = 'periodicCaptureInfoBar';
	var textId = baseId + '_' + 'text';
	var iconId = baseId + '_' + 'icon';

	var eleId;

	// set base element

	createSpanElement('uiArea', baseId);

	resizePeriodicCaptureInfoBar();

	// set text element

	createLabelElement(baseId, textId);

	eleId = '#' + textId;

	$(eleId).css('position', 'absolute');
	$(eleId).css('left', 0);
	$(eleId).css('top', ((appCfg.pcibIconH - appCfg.pcibTextH)/2) + 'px');
	$(eleId).css('width', appCfg.pcibTextW + 'px');
	$(eleId).css('height', appCfg.pcibTextH + 'px');

	$(eleId).css('text-align', 'center');
	$(eleId).css('line-height', appCfg.pcibTextH + 'px');
	$(eleId).css('font-size', appCfg.pcibTextFontSize);
	$(eleId).css('color', appCfg.pcibTextColor);
	
	// set stop icon

	createSpanElement(baseId, iconId);

	eleId = '#' + iconId;

	$(eleId).css('position', 'absolute');
	$(eleId).css('left', appCfg.pcibTextW + 'px');
	$(eleId).css('top', 0);
	$(eleId).css('width', appCfg.pcibIconW + 'px');
	$(eleId).css('height', appCfg.pcibIconH + 'px');

	$(eleId).css('background', fcGetDefIconPath('timesave_star.png'));

	$(eleId).on('click', stopPeriodicCapture);

	// hide info bar

	showPeriodicCaptureInfoBar(false);
}

function stopPeriodicCapture()
{
	appFC.capturePeriodRun = false;

	showPeriodicCaptureInfoBar(false);
}

function showPeriodicCaptureInfoBar(mode)
{
	showElement('periodicCaptureInfoBar', mode);
}

function updatePeriodicCaptureInfoBar()
{
	var baseId = 'periodicCaptureInfoBar';
	var textId = baseId + '_' + 'text';

	var id;
	var text;

	text = appFC.capturePeriodCurCnt + ' / ' + appFC.capturePeriodTotalCnt;

	id = document.getElementById(textId);
	
	fcUpdateElementText(id, text);
}

///////////////////////////////////////////////////////////////////////////////////////////
// confirm dialog
///////////////////////////////////////////////////////////////////////////////////////////

function resizeConfirmDlg()
{
	var baseId = 'confirmDlg';
	var totalW, totalH;
	var eleId;

	totalW = appCfg.cdFrameGap*2 + appCfg.cdIconW*2 + appCfg.cdIconGap;
	totalH = appCfg.cdFrameGap*2 + appCfg.cdIconH;

	eleId = '#' + baseId;

	$(eleId).css('position', 'absolute');
	$(eleId).css('background', appCfg.cdBkg);
	$(eleId).css('opacity', appCfg.cdOpacity);

	$(eleId).css('left', 'calc(50% - ' + (totalW/2) + 'px)');
	$(eleId).css('top', 'calc(50% - ' + (totalH/2) + 'px)');
	$(eleId).css('width', totalW + 'px');
	$(eleId).css('height', totalH + 'px');
}

function initConfirmDlg()
{
	var baseId = 'confirmDlg';
	var yesId = baseId + '_' + 'yes';
	var noId = baseId + '_' + 'no';
	var startX, startY;

	// create base element

	createSpanElement('uiArea', baseId);

	resizeConfirmDlg();

	// create icon

	startX = appCfg.cdFrameGap;
	startY = appCfg.cdFrameGap;

	createConfirmDlgIcon(noId, 'no.png', startX, startY);

	startX += appCfg.cdIconW + appCfg.cdIconGap;

	createConfirmDlgIcon(yesId, 'yes.png', startX, startY);

	function createConfirmDlgIcon(id, picture, x, y)
	{
		var eleId;
		var bkg;
		
		createSpanElement(baseId, id);

		eleId = '#' + id;

		bkg = fcGetDefIconPath(picture);
		$(eleId).css('background', bkg);
	
		$(eleId).css('left', x + 'px');
		$(eleId).css('top', y + 'px');
		$(eleId).css('width', appCfg.cdIconW + 'px');
		$(eleId).css('height', appCfg.cdIconH + 'px');
		$(eleId).css('position', 'absolute');

		$(eleId)
			.on('mousedown', function(event) {
				if ( MouseBtnId.left === event.which )
				{
					$(eleId).css('background-color', appCfg.cdMouseColorClick);

					handleCinfirmDlgClick(event.target.id);
				}
			})
			.on('mousemove', function(event) {
				$(eleId).css('background-color', appCfg.cdMouseColorEnter);
			})
	        .on('mouseout',function() {
				$(eleId).css('background-color', appCfg.cdMouseColorLeave);
	        });
	}

	// handle click

	function handleCinfirmDlgClick(id)
	{
		//console.log(id + " clicked");
		
		var baseId = 'confirmDlg';
		var yesId = baseId + '_' + 'yes';
		var noId = baseId + '_' + 'no';
		
		var eleId;
		var result;

		eleId = document.getElementById(baseId);

		if ( eleId.clickHandler )
		{
			eleId.clickHandler( id == yesId );
		}
	}

	// hide dialog

	showConfirmDlg(false);
}

function showConfirmDlg(mode)
{
	var baseId = 'confirmDlg';
	
	showElement(baseId, mode);
}

function setConfirmDlgClickHandler(fn)
{
	var baseId = 'confirmDlg';
	var eleId;

	eleId = document.getElementById(baseId);

	eleId.clickHandler = fn;
}

function setConfirmDlgTip(tipYes, tipNo)
{
	var baseId = 'confirmDlg';
	var yesId = baseId + '_' + 'yes';
	var noId = baseId + '_' + 'no';

	fcUpdateElementTitle(yesId, tipYes);
	fcUpdateElementTitle(noId, tipNo);
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  get local storage image file 
///////////////////////////////////////////////////////////////////////////////////////////

var inputFiles;


function lsInitSelectFilesDlg()
{
	createInputElement('uiArea', 'imgInput');
	
	inputFiles = document.getElementById('imgInput');
	inputFiles.hidden = true;
	inputFiles.type = 'file';
	inputFiles.onchange = lsGetSelectFilesList;
}

function lsGetSelectFilesList()
{
	var files = inputFiles.files;

	if ( files.length )
	{
		if ( -1 != files[0].type.indexOf("image") )
		{
			fcLoadImage(files[0]);
		}
	}
}

function lsSelectFilesDlg()
{
	$('#imgInput').val('');
	inputFiles.click();
}

///////////////////////////////////////////////////////////////////////////////////////////
//  image file 
///////////////////////////////////////////////////////////////////////////////////////////

function fcLoadImage(file)
{
	var image = new Image();

	image.addEventListener("load",
		function ()
		{
			var imageInfo = file.name + ' ' + 
			image.width  + 'x' + 
			image.height + ' ' +
			file.type    + ' ' +
			Math.round(file.size/1024) +'KB';
			//console.log(imageInfo);

			displayPictureWin(image);
		}
	);

	image.src = window.URL.createObjectURL(file);
}

///////////////////////////////////////////////////////////////////////////////////////////
//  init 
///////////////////////////////////////////////////////////////////////////////////////////

function galleryInit()
{
	lsInitSelectFilesDlg();

	galleryFillThumbnailDiv();
}

function refleshGalleryFillThumbnailDiv()
{
	appFC.thumbSelectedFileCnt = 0;
	appFC.thumbnailCnt = 0;
	appFC.thumbFileCnt = 0;
	$('#thumbDiv').empty();
	galleryFillThumbnailDiv();
}

function galleryFillThumbnailDiv()
{
	if ( !appFs )
	{
		setTimeout(function () {
				galleryFillThumbnailDiv();
			},
			50);
			
		return;
	}

	var dirReader = appFs.root.createReader();

	dirReader.readEntries(
		enumFiles,
		errorHandler
	);

	function enumFiles(entries)
	{
		//console.log(entries);
		for ( var i = 0; i < entries.length; i++ )
		{
			if ( entries[i].isFile )
			{
				if ( -1 != entries[i].name.indexOf(".jpg") )
				{
					appFC.thumbnailCnt += 1;

					galleryCreateThumbnailDiv(entries[i], appFC.thumbnailCnt);
				}

				if ( -1 != entries[i].name.indexOf(".webm") )
				{
					appFC.thumbnailCnt += 1;

					galleryCreateVideoDiv(entries[i], appFC.thumbnailCnt);
				}
			}
		}
	}

	function errorHandler(fe)
	{
		console.log(fe.code);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// thumbnail   
///////////////////////////////////////////////////////////////////////////////////////////

function galleryGetFitSize(dstW, dstH, srcW, srcH)
{
	var size = {
			x: 0,
			y: 0,
			w: 0,
			h: 0
		};

	if ( dstW * srcH < srcW * dstH )
	{
		// use full display width

		size.w = dstW;
		size.x = 0;

		size.h = Math.floor(size.w * srcH / srcW);
		size.y = Math.floor((dstH - size.h) / 2);
	}
	else
	{
		// use full display height

		size.h = dstH;
		size.y = 0;

		size.w = Math.floor(size.h * srcW / srcH);
		size.x = Math.floor((dstW - size.w) / 2);
	}

	return size;
}

var requestFullScreenEle;
$(document).on("webkitfullscreenchange",function(){
	if (document.fullscreenElement) 
	{
		//console.log(`Element: ${document.fullscreenElement.id} entered full-screen mode.`);
		//console.log(document.fullscreenElement);
		requestFullScreenEle = document.fullscreenElement;
		requestFullScreenEle.style.border = 'none';
	} 
	else 
	{
		//console.log('Leaving full-screen mode.');
		var borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + appCfg.thumbFrmColorChecked;
		requestFullScreenEle.style.border = borderCfg;
	}	
});

function galleryCreateVideoDiv(entry, no)
{
	var divId = 'thumb' + '_' + no;
	var imgId = 'img' + '_' + no;
	var iconId ="vIcon_" + no;
	var eleId;
	var thumbW = appCfg.thumbW;
	var thumbH = appCfg.thumbH;
	var paddingS = appCfg.thumbPadding;
	var w, h;

	// div

	createDivElement('thumbDiv', divId);

	w = (thumbW - (paddingS*2));
	h = (thumbH - (paddingS*2));

	eleId = '#' + divId;

	$(eleId).css('width', w + 'px');
	$(eleId).css('height', h + 'px');
	$(eleId).css('padding', paddingS + 'px');
	$(eleId).css('display', 'inline-block');
	$(eleId).css('position', 'relative');

	// Video

	createVideoElement(divId, imgId);
	createDivElement(divId, iconId);

	entry.file(
		function(file)
		{
			var reader = new FileReader();

			reader.onloadstart = function(e)
            {
				var size = galleryGetFitSize(w, h, 1280, 720);
				//console.log(size);
				//var size = galleryGetFitSize(w, h, image.width, image.height);
				var border = appCfg.thumbFrmSize + 'px' + ' solid ' + appCfg.thumbFrmColorNormal;

				eleId = '#' + imgId;

				var css =
				{
					left: size.x + paddingS,
					top: size.y + paddingS,
					width: size.w,
					height: size.h,
					position: 'absolute',
					border: border,
				};

				$(eleId).css(css);
				$(eleId).addClass('thumbImg');

				eleId = document.getElementById(imgId);
				eleId.src = entry.toURL();

				eleId.autoplay = false;
                eleId.controls = false;
                eleId.loop = false;
					
				eleId.fileName = e.name;
				eleId.fileEntry = entry;
				eleId.checked = false;

				css =
				{
					right:6,
					bottom:17,
					width:36,
					height:12,
					position:"absolute",
					backgroundColor:"#000",
					color:"#EEE",
					textAlign:"center",
					fontSize:12,
					opacity:.75
				};

				$('#'+iconId).css(css);

				eleId.addEventListener("loadedmetadata", function(){
					var a = Math.round(eleId.duration);
					document.getElementById(iconId).innerText=Math.floor(a/60)+":"+padLeft(a%60,2)
				});

				$(eleId).on('click', function (event) {
					var ele = document.getElementById(event.target.id);
					var bkColor;
					var borderCfg;

					if (chrome.app.window.current().isFullscreen() )
					{
						return;
					}

					//console.log(ele.checked);

					if ( ele.checked )
					{
						ele.checked = false;
						bkColor = appCfg.thumbFrmColorNormal;
						galleryReductSelectedFile();
					}
					else
					{
						ele.checked = true;
						bkColor = appCfg.thumbFrmColorChecked;
						galleryAddSelectedFile();
					}
					borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
					$(this).css('border', borderCfg);							
					$(this).toggleClass('selected');
				});

				$(eleId).dblclick( function () {
					var eleId = document.getElementById(this.id);

					eleId.checked = true;
					bkColor = appCfg.thumbFrmColorChecked;
					galleryAddSelectedFile();
					borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
					$(this).css('border', borderCfg);							
					$(this).toggleClass('selected');

					//console.log(eleId);
					eleId.muted = false;
					eleId.currentTime = 0;
					//eleId.style.border = 'none';
					eleId.webkitRequestFullScreen();
				});

				eleId.addEventListener('mouseover', function(e){
					var playPromise = eleId.play();
					if (playPromise !== undefined) {
						playPromise.then(_ => {

							eleId.muted = true;
							eleId.play();
						})
						.catch(error => {
						// Auto-play was prevented
						// Show paused UI.
						});
					}					
				});
				eleId.addEventListener('mouseleave', function(e){
					var playPromise = eleId.play();
					if (playPromise !== undefined) {
						playPromise.then(_ => {

							//eleId.stop();
							eleId.pause();
							eleId.currentTime = 0;
						})
						.catch(error => {
						// Auto-play was prevented
						// Show paused UI.
						});
					}					
				});
				galleryAddFileFinish();
			}
			reader.readAsText(file);			
		},
		function (fe) {
			console.log(fe.code);
		}
	);
}

// Picture
function galleryCreateThumbnailDiv(entry, no)
{
	var divId = 'thumb' + '_' + no;
	var imgId = 'img' + '_' + no;
	var eleId;
	var thumbW = appCfg.thumbW;
	var thumbH = appCfg.thumbH;
	var paddingS = appCfg.thumbPadding;
	var w, h;

	// div

	createDivElement('thumbDiv', divId);

	w = (thumbW - (paddingS*2));
	h = (thumbH - (paddingS*2));

	eleId = '#' + divId;

	$(eleId).css('width', w + 'px');
	$(eleId).css('height', h + 'px');
	$(eleId).css('padding', paddingS + 'px');
	$(eleId).css('display', 'inline-block');
	$(eleId).css('position', 'relative');

	// img

	createImgElement(divId, imgId);

	entry.file(
		function(file)
		{
			var image = new Image();

			image.addEventListener("load",
				function ()
				{					
					var size = galleryGetFitSize(w, h, image.width, image.height);
					var border = appCfg.thumbFrmSize + 'px' + ' solid ' + appCfg.thumbFrmColorNormal;

					eleId = '#' + imgId;

					var css =
					{
						left: size.x + paddingS,
						top: size.y + paddingS,
						width: size.w,
						height: size.h,
						position: 'absolute',
						border: border,
					};

					$(eleId).css(css);

					$(eleId).addClass('thumbImg');

					eleId = document.getElementById(imgId);
					eleId.src = image.src;
					
					eleId.fileName = file.name;
					eleId.imgSrc = image;
					eleId.fileEntry = entry;
					eleId.checked = false;
					
					$(eleId)
						.on('click', function (event) {
							var ele = document.getElementById(event.target.id);
							var bkColor;
							var borderCfg;

							if ( ele.checked )
							{
								ele.checked = false;
								bkColor = appCfg.thumbFrmColorNormal;
								galleryReductSelectedFile();
							}
							else
							{
								ele.checked = true;
								bkColor = appCfg.thumbFrmColorChecked;
								galleryAddSelectedFile();
							}

							borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
							$(this).css('border', borderCfg);							
							$(this).toggleClass('selected');
						});

					$(eleId).dblclick( function () {
						var eleId = document.getElementById(this.id);

						eleId.checked = true;
						bkColor = appCfg.thumbFrmColorChecked;
						galleryAddSelectedFile();
						borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
						$(this).css('border', borderCfg);
						$(this).toggleClass('selected');

						//console.log(this.id + " double clicked, szie = " + eleId.imgSrc.width + 'x' + eleId.imgSrc.height);
						playbackDisplayImgMode(eleId.imgSrc);
					});

					galleryAddFileFinish();
				}
			);

			image.src = window.URL.createObjectURL(file);
		},
		function (fe) {
			console.log(fe.code);
		}
	);
}

function gallerySelectAll(mode)
{
	var ids = $('.thumbImg');
	var className = 'selected';
	var eleId;
	var $id;
	var i;
	var checked;
	var bkColor;
	var borderCfg;

	if ( mode )
	{
		checked = true;
		bkColor = appCfg.thumbFrmColorChecked;
	}
	else
	{
		checked = false;
		bkColor = appCfg.thumbFrmColorNormal;
	}

	borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;

	for ( i = 0; i < ids.length; i++ )
	{
		eleId = document.getElementById(ids[i].id);
		eleId.checked = checked;

		$id = $('#' + ids[i].id);

		$id.css('border', borderCfg);

		if ( mode ) $id.addClass(className);
		else $id.removeClass(className);
	}
}

function galleryDeleteSelectedFileFinish()
{
	appFC.thumbFileCnt -= 1,
	appFC.thumbSelectedFileCnt -= 1,

	galleryUpdateFileInfo();
}

function galleryAddFileFinish()
{
	appFC.thumbFileCnt += 1,

	galleryUpdateFileInfo();
}

function galleryAddSelectedFile()
{
	galleryUpdateSelectedFileDelay();
	//appFC.thumbSelectedFileCnt += 1,
	//galleryUpdateFileInfo();
}

function galleryReductSelectedFile()
{
	galleryUpdateSelectedFileDelay();
	//appFC.thumbSelectedFileCnt -= 1,
	//galleryUpdateFileInfo();
}

var selectedFileDelay = -1;
function galleryUpdateSelectedFileDelay()
{
	if ( selectedFileDelay == -1 )
	{
		selectedFileDelay = 2;
		setTimeout(galleryUpdateSelectedFileDelay, 100);
	}
	else if (selectedFileDelay > 0)
	{
		selectedFileDelay -= 1;
		setTimeout(galleryUpdateSelectedFileDelay, 100);
	}
	else // selectedFileDalay == 0
	{
		selectedFileDelay = -1;
		galleryUpdateSelectedFile();
	}
}

function galleryUpdateSelectedFile()
{
	var ids = $('.thumbImg');
	var className = 'selected';

	appFC.thumbSelectedFileCnt = 0;

	for (var i = 0; i < ids.length; i++ )
	{
		var eleId = document.getElementById(ids[i].id);
		var $id = $('#' + ids[i].id);		

		if ( eleId.checked )
		{
			appFC.thumbSelectedFileCnt += 1;
			$id.addClass(className);
		}
		else
		{
			$id.removeClass(className);
		}
		// if ( eleId.checked )
		// {
		// 	var bkColor = appCfg.thumbFrmColorChecked;
		// 	var borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
		// 	$id.css('border', borderCfg);
		// 	$id.addClass(className);
		// 	appFC.thumbSelectedFileCnt += 1;
		// }			
		// else
		// {
		// 	var bkColor = appCfg.thumbFrmColorNormal;
		// 	var borderCfg = appCfg.thumbFrmSize + 'px' + ' solid ' + bkColor;
		// 	$id.css('border', borderCfg);
		// 	$id.removeClass(className);
		// }
	}

	galleryUpdateFileInfo();
}

function galleryUpdateFileInfo()
{
	var text;

	text = appFC.thumbSelectedFileCnt + ' / ' + appFC.thumbFileCnt;

	playbackUpdateFileInfoElement(text);

	galleryUpdateSelectAllToolTip();
}

function galleryUpdateSelectAllToolTip()
{
	var strId;

	if ( appFC.thumbFileCnt && (appFC.thumbFileCnt === appFC.thumbSelectedFileCnt ) ) strId = "btnUnselectAll";
	else strId = "btnSelectAll";

	fcUpdateElementTitle("btn_toggleAll", strId);
}

///////////////////////////////////////////////////////////////////////////////////////////
// delete file 
///////////////////////////////////////////////////////////////////////////////////////////

function galleryDeleteFiles(eleId)
{
	var i;
	
	for ( i = 0; i < eleId.length; i++ )
	{
		eleId[i].fileEntry.remove(function () {
			},
			function () {
				console.log(fe.code);
				}
			);

		($('#' + eleId[i].id).parent()).remove();

		galleryDeleteSelectedFileFinish();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// save canvas to local storage 
///////////////////////////////////////////////////////////////////////////////////////////

function lsSaveCanvasById(id, filename)
{
    var canvasElement = document.getElementById(id);

	lsSaveCanvas(canvasElement, filename);
}

function lsSaveCanvas(canvasElement, filename)
{
    var MIME_TYPE = "image/jpeg";
	var imgURL = canvasElement.toDataURL(MIME_TYPE);

	lsSaveUrl(imgURL, filename);
}

function lsSaveImage(image, filename)
{
	var canvas = document.getElementById('saveFileCanvas');
	var ctx = canvas.getContext('2d');

    $('#saveFileCanvas').attr('width', image.width);
    $('#saveFileCanvas').attr('height', image.height);

	ctx.drawImage(image, 0, 0, image.width, image.height);
	
	lsSaveCanvas(canvas, filename);
}

function lsSaveUrl(url, filename)
{
    var MIME_TYPE = "image/jpeg";
    var dlLink = document.createElement('a');
	
    dlLink.download = filename;
    dlLink.href = url;
    dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');

    document.body.appendChild(dlLink);
    dlLink.click();
    document.body.removeChild(dlLink);
}

///////////////////////////////////////////////////////////////////////////////////////////
// save files to local storage default folder (file by file)
///////////////////////////////////////////////////////////////////////////////////////////

function lsSaveFilesToDefaultFoloer(eleId)
{
	var i;
	
	for ( i = 0; i < eleId.length; i++ )
	{
		lsSaveImage(eleId[i].imgSrc, eleId[i].fileName);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// save files to local storage selected folder 
///////////////////////////////////////////////////////////////////////////////////////////

function lsSaveFilesToSelectedFolder(eleId)
{
	var i;

	if ( 0 == eleId.length ) return;	

	chrome.fileSystem.chooseEntry(
		{ type: 'openDirectory' }, 
		function(dirEntry) {
			if(chrome.runtime.lastError) return;
			if ( !dirEntry || !dirEntry.isDirectory ) return;

			for ( i = 0; i < eleId.length; i++ )
			{
				if ( eleId[i].tagName == "IMG" )
				{
					var canvas = imageToCanvas(eleId[i].imgSrc);
					// only jpeg is supported by jsPDF
					// var imgData = canvas.toDataURL("image/jpeg", 1.0);
					// var pdf = new jsPDF();
				
					// pdf.addImage(imgData, 'JPEG', 0, 0, canvas.width, canvas.height);
					// pdf.save("download.pdf");

					writeCanvasToFile(dirEntry, canvas, eleId[i].fileName);
				}

				if ( eleId[i].tagName == "VIDEO" )
				{
					appFs.root.getFile(eleId[i].fileEntry.name, {}, function(fileEntry)
					{
						fileEntry.copyTo(dirEntry);
					});
				}
 			}
		}
	);

	function imageToCanvas(image)
	{
		var canvas = document.getElementById('saveFileCanvas');
		var ctx = canvas.getContext('2d');

	    $('#saveFileCanvas').attr('width', image.width);
	    $('#saveFileCanvas').attr('height', image.height);

		ctx.drawImage(image, 0, 0, image.width, image.height);

		return canvas;
	}

	function writeCanvasToFile(dirEntry, canvas, filename)
	{
		canvas.toBlob(function(blob) {
				dirEntry.getFile(filename, {create:true}, function(entry) {
					entry.createWriter(function(writer) {
						writer.write(blob);
					});
				});
			},
			"image/png", 1.0
		);
	}
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// constant 
///////////////////////////////////////////////////////////////////////////////////////////

var choseVideoSource = '';

var videoW = 1280;	//appCfg.defPreviewRsoX;
var videoH = 720;	//appCfg.defPreviewRsoY;

var videoElement = document.getElementById('videoSrc');

var videoLastUpdateTime;
 

///////////////////////////////////////////////////////////////////////////////////////////
// check device 
///////////////////////////////////////////////////////////////////////////////////////////

function isSupportedDevice( vid, pid )
{
    var len = DevList.length;
    var i;

	for ( i = 0; i < len; i++ )
	{
		if ( vid === DevList[i][0] )
		{
		    if ( pid === DevList[i][1] )
		    {
				appFC.reqVideoResolutionList = DevList[i][2];

		        return true;
		    }
		}
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////////////////
// connect stream 
///////////////////////////////////////////////////////////////////////////////////////////
var IsDeviceConnected = false;

var successCallback = function(stream)
{
	window.stream = stream; // make stream available to console

	if ( !videoElement )
	{
		videoElement = document.getElementById('videoSrc');
		//console.log("successCallback get videoElement again");
	}

	//videoElement.src = window.URL.createObjectURL(stream);
	stream.getAudioTracks()[0].enabled = false;

	videoElement.srcObject = stream;
	videoElement.muted = true;
    videoElement.play();
	videoElement.hidden = true;

	IsDeviceConnected = true;
};

var errorCallback = function(error)
{
    console.log("navigator.getUserMedia error: ", error);
};


var deviceChangeTime = 0;
navigator.mediaDevices.ondevicechange = function()
{
	//console.log("ON DEVICE CHANGE");

	if ( IsRecording )
	{
		StopRecord();
	}

	// if (fullPhotoMode)
	// {
	// 	switchFullPhotoMode(false);	
	// }

	if (deviceChangeTime == 0 && IsDeviceConnected)
	{
		IsDeviceConnected = false;
		disconnectVideoSrc();
		deviceChangeTime = 1;

        setTimeout(function(){
            deviceChangeTime = 0;
        },2000);
	}

	if(deviceChangeTime == 0 && !IsDeviceConnected)
	{
        connectVideoSrc();
        deviceChangeTime = 1;

        setTimeout(function(){
            deviceChangeTime = 0;
        },2000);
	}
}

var start = function()
{
    var videoSource = choseVideoSource;
	
    var constraints = {
		audio: true,
        video: {
            deviceId: videoSource ? {exact: videoSource} : undefined,
            width: {min: videoW, max: videoW},
			height: {min: videoH, max: videoH}
        }
	};
	//navigator.mediaDevices.getUserMedia({ video: true }).then(successCallback).catch(errorCallback);
	navigator.mediaDevices.getUserMedia(constraints).then(successCallback).catch(errorCallback);
};

var setVideoSelectEvent = function()
{
    start();
}

var getSources = function(sourceInfos)
{
	var getDevice = false;

    for(var i=0 ; i < sourceInfos.length; i++)	
    {
		var sourceInfo = sourceInfos[i];

		if(sourceInfo.kind === 'videoinput')
		{
	        if (sourceInfo.label.length > 0)
	        {
	            var getVendorID = parseInt("0x"+sourceInfo.label.split(":")[0].split("(")[1], 16);
				var getProductID = parseInt("0x"+sourceInfo.label.split(")")[0].split(":")[1], 16);
	            //console.log("VID_PID =", getVendorID.toString(16), " _ ", getProductID.toString(16));

                if ( true === isSupportedDevice( getVendorID, getProductID ) )
                {
				    //console.log("use", sourceInfo.label);

					fcUpdateVideoResolution();

					getDevice = true;

                    choseVideoSource = sourceInfo.deviceId;

                    //setVideoSelectEvent();
					
				    //break;
                }
	        }
		}
    }

	if ( getDevice )
	{
		setVideoSelectEvent();
	}
}

function disconnectVideoSrc()
{
    if ( !!window.stream )
    {
        var appVersion = navigator.appVersion;
        var objOffsetVersion = appVersion.indexOf("Chrome");
        var chromeVersion = appVersion.substr(objOffsetVersion+7, 2);

        if ( chromeVersion <= 46 )
	    {
            window.stream.stop();
        }
        else
        {
            window.stream.getVideoTracks()[0].stop();
        }
    }
}

function connectVideoSrc()
{
    disconnectVideoSrc();
		
    navigator.mediaDevices.enumerateDevices().then(getSources);
}


///////////////////////////////////////////////////////////////////////////////////////////
// update stream frame 
///////////////////////////////////////////////////////////////////////////////////////////

var lastTime = -1;

function updateVideoStreamFrame()
{
	if (chrome.runtime.lastError) {				
		console.log("crequestAnimationFrame");
	} 

	if ( !videoElement )
	{
		videoElement = document.getElementById('videoSrc');
		//console.log("updateVideoStreamFrame() get videoElement again");
	}

    var time = videoElement.currentTime;
	
    if ( time && (time !== lastTime) )
	{
        //console.log('time: ' + time);
        lastTime = time;

		videoLastUpdateTime = new Date();

		previewVideoStreamUpdate(videoElement);
    }

	requestAnimationFrame(updateVideoStreamFrame);
}

///////////////////////////////////////////////////////////////////////////////////////////
// active check 
///////////////////////////////////////////////////////////////////////////////////////////

function intVideoStreamActiveCheck()
{
	videoLastUpdateTime = new Date();

	setInterval(function ()
		{
			var curTime = new Date();
			var diff = curTime - videoLastUpdateTime;

			//console.log(window.stream);

			if ( typeof window.stream == 'undefined' || 
				 (diff > 1000 && 
					window.stream.active == false &&
					false === chrome.app.window.current().isMinimized()) )
			{
				connectVideoSrc();
			}
		},
		3000 );
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// mouse event
///////////////////////////////////////////////////////////////////////////////////////////
var fullPhotoMode = false;
var fullScreenMode = false;
var oldScreenWidth = 0;
var oldScreenHeight = 0;

function switchFullPhotoMode(enable)
{
	fullPhotoMode = enable;
	window.onresize();
	fcToogleToolBar();
	sysUpdateToolBarTip();
	toolBarSetAllElementState('previewToolBar', 0);

	setHideIconCountdown();
}

function sysToolBarOnClick(event)
{
	switch (event.target.id)
	{
		case "sys_close":
			
			if ( true == chrome.app.window.current().isFullscreen() )
			{
				document.webkitCancelFullScreen();
			}
			
			window.close();
			
			break;
			
		case "sys_fullScreen":
			if ( fullScreenMode )
			{
				window.resizeTo(oldScreenWidth, oldScreenHeight);
			}
			else
			{
				oldScreenWidth = document.body.clientWidth;
				oldScreenHeight = document.body.clientHeight
				window.resizeTo(window.screen.width, window.screen.height);
			}
			fullScreenMode = !fullScreenMode;
			return;

			appFC.resizeDelayMs = 0;

			if ( true == chrome.app.window.current().isFullscreen() )
			{
				document.webkitCancelFullScreen();
			}
			else
			{
				document.documentElement.webkitRequestFullScreen();
			}
			
			break;
			
		case "sys_fullPhoto":			
			switchFullPhotoMode(!fullPhotoMode);			
			// fullPhotoMode = !fullPhotoMode;
			// window.onresize();
			// fcToogleToolBar();
			// sysUpdateToolBarTip();			
			break;
			
		case "sys_smallScreen":

			chrome.app.window.current().minimize();
			
			break;
	}

	//fcMouseEventNotify(UiType.sysToolBar, MouseStatus.click, event);
}

///////////////////////////////////////////////////////////////////////////////////////////
// tool tip
///////////////////////////////////////////////////////////////////////////////////////////

function sysUpdateToolBarTip()
{
	var strId;

	if ( appFC.showToolBar ) strId = "btnHideFunctionIcon";
	else strId = "btnShowFunctionIcon";

	fcUpdateElementTitle("sys_fullPhoto", strId);
}


'use strict';

///////////////////////////////////////////////////////////////////////////////////////////
// record
///////////////////////////////////////////////////////////////////////////////////////////

var mediaRecorder;
var recordedChunks = [];
var IsRecording = false;
var IsPause = false;
var recordingStartTime;
var recordingTotal;
var recordingPauseTime;
var OldImgW = 1280;
var OldImgH = 720;
var RECORD_LIMIT_SEC = 600;

function showRecordBtn(visible)
{
	showElement('bottomToolBarData', visible);
	if (visible)
		$("#btn_pause").hide();
}

function showAllToolBars(visible)
{
	var idTbl = [
		"btn_setting",      
		"btn_capture",      
		"btn_freeze",       
		"btn_timesave",     
		"btn_mirrow",       
		"btn_flip",
		//'btn_brightness',
		//'btn_whitebalance',
		"btn_config",
		"btn_zoom",         
		"btn_importPic",    
		"btn_redo",         
		"btn_undo",

		// "mode_about",       
		// "mode_preview",     
		// "mode_playback",    
		// "mode_drawingBoard",

		'btn_freehand',
		'btn_arrow',
		'btn_eraser',
		'btn_line',
		'btn_rectangleLine',
		'btn_rectangle',
		'btn_circleLine',
		'btn_circle',
		'btn_erase_all',
		'btn_text',
		// 'previewToolBar',
		// 'drawingToolBar',
		// 'modeToolBar',
		 'drawingCfgColorBar',
		 'drawingOpacity',
		 'drawingLineWidth'
	];

	var i;

	for ( i = 0; i < idTbl.length; i++ )
	{
		showToolBar(idTbl[i], visible);
	}

	if (visible)
	{
		//document.getElementById("sysToolBar").style.opacity = appCfg.toolBarOpacityLeave;
		document.getElementById("sysToolBar").style.background = appCfg.toolBarColorLeave;
	}
	else
	{
		//document.getElementById("sysToolBar").style.opacity = '0';
		document.getElementById("sysToolBar").style.backgroundColor = '#000';
	}

	//opacity: appCfg.toolBarOpacityLeave,
}

var recordStream;

function StartRecord()
{
	IsRecording = true;
	IsPause = false;
	showAllToolBars(false);
	fcCloseActiveDlg();
	$("#btn_pause").show();

	if (appFC.imgW != 1280 || appFC.imgH != 720)
	{
		OldImgW = appFC.imgW;
		OldImgH = appFC.imgH;
		fcChangeBaseImageSize(1280, 720);
		setTimeout(StartRecord, 2500);
		return;
	}

	// -- recording time stamp
	document.getElementById("recordtime").style.fontSize = "18px";
	document.getElementById("recordtime").style.fontWeight = "bold";
	document.getElementById("recordtime").style.color = "red";
	document.getElementById("recordtime").style.position = "absolute";
	document.getElementById("recordtime").style.bottom = "35px";
	document.getElementById("recordtime").style.right = (document.documentElement.clientWidth/2 - 30)+"px";
	document.getElementById("recordtime").style.visibility = "visible";
	//recordingsec = 0;

	var getd = new Date();
	recordingStartTime = getd.getTime();
	recordingPauseTime = 0;
	RecordingTimer();	

	if (1)
	{
		// record all canvas
		var mc = previewModeCfg;
		var canvas = mc['combineCanvas'];
		//var canvas = document.querySelector("canvas");
		
		recordStream = canvas.captureStream(25);
		//var stream = canvas.captureStream(25);

		if( window.stream != undefined)
		{
			window.stream.getAudioTracks()[0].enabled = true;
			recordStream.addTrack(window.stream.getAudioTracks()[0]);
		}
	}
	else
	{
		window.stream.getAudioTracks()[0].enabled = true;
		recordStream = window.stream		
	}	

	recordedChunks = [];

	var options = {  audioBitsPerSecond : 96000,
		videoBitsPerSecond : 256 * 8 * 1000,
		mimeType: 'video/webm;codecs=vp8; '};

	mediaRecorder = new MediaRecorder(recordStream, options);	
	mediaRecorder.ondataavailable = handleDataAvailable;
	mediaRecorder.start();

	function handleDataAvailable(event) {
		if (event.data && event.data.size > 0) {
			recordedChunks.push(event.data);
			//DownloadRecord();
			SaveRecord();
		} else {
			console.log("no data");
		}
	}	
};

function StopRecord()
{
	IsRecording = false;
	document.getElementById("recordtime").style.visibility = "hidden";
	showAllToolBars(true);
	$("#btn_pause").hide();
	mediaRecorder.stop();

	recordStream.getTracks().forEach(function(track){
		track.stop(); 
		recordStream.removeTrack(track);
	});

	if ( window.stream != undefined )
		window.stream.getAudioTracks()[0].enabled = false;

	var getd = new Date();
	var recordingNow = getd.getTime();
	if( IsPause )
	{
		IsPause = false;
		recordingTotal = recordingPauseTime;
	}
	else
	{
		recordingTotal = recordingPauseTime + recordingNow - recordingStartTime;
	}
};

function PauseRecord()
{
	IsPause = true;
	//showAllToolBars(true);
	mediaRecorder.pause();

	var getd = new Date();
	var recordingNow = getd.getTime();
	recordingPauseTime += recordingNow - recordingStartTime;
}

function ResumeRecord()
{
	IsPause = false;
	//showAllToolBars(false);
	mediaRecorder.resume();

	// set recording time
	var getd = new Date();
	recordingStartTime = getd.getTime();
	RecordingTimer();
}

function SaveRecord()
{
	var filename = fcGetFilenameByDateTime('webm');
	//console.log("filename: " + filename);
	appFs.root.getFile(filename, {create: true}, function(fileEntry)
    {
        fileEntry.createWriter(function(fileWriter)
        {

            fileWriter.onwriteend = function (e) {
				refleshGalleryFillThumbnailDiv();
				//appFC.thumbnailCnt += 1;
				//galleryCreateVideoDiv(fileEntry, appFC.thumbnailCnt);
            };

            fileWriter.onerror = function(e) {
				console.log(e);
            };

            let blob = new Blob(recordedChunks, {type: 'video/webm'});
            setTimeout(function(){
				
                ysFixWebmDuration(blob, recordingTotal, function(fixedBlob) {
                    //console.log(fixedBlob);
					fileWriter.write(fixedBlob);
					
					recordedChunks = [];
					fixedBlob - null;
					blob = null;

					if (OldImgW != 1280 || OldImgH != 720)
					{
						fcChangeBaseImageSize(OldImgW, OldImgH);
					}
                });
            },100);
            
            // fileWriter.write(blob);
        });
    });
}

function DownloadRecord()
{
	//console.log("Download Record");

	let blob = new Blob(recordedChunks, {
		type: 'video/webm'
	  });

	//console.log(recordingTotal);

	// var url = URL.createObjectURL(blob);
	// var a = document.createElement('a');
	// document.body.appendChild(a);

	// var filename = new Date().toISOString().slice(0,19);
	// a.style = 'display: none';
	// a.href = url;
	// a.download = filename + '.webm';
	// a.click();
	// window.URL.revokeObjectURL(url);
	// recordedChunks = [];
	// if (OldImgW != 1280 || OldImgH != 720)
	// {
	// 	fcChangeBaseImageSize(OldImgW, OldImgH);
	// }

	setTimeout(function(){
		ysFixWebmDuration(blob, recordingTotal, function(fixedBlob) {
			var url = URL.createObjectURL(fixedBlob);
			var a = document.createElement('a');
			document.body.appendChild(a);

			var filename = new Date().toISOString().slice(0,19);

			a.style = 'display: none';
			a.href = url;
			a.download = filename + '.webm';
			a.click();
			window.URL.revokeObjectURL(url);

			recordedChunks = [];

			if (OldImgW != 1280 || OldImgH != 720)
			{
				fcChangeBaseImageSize(OldImgW, OldImgH);
			}
		});
	},100);	
};

function padLeft(str, len) {
    str = '' + str;
    if (str.length >= len) {
        return str;
    } else {
        return padLeft("0" + str, len);
    }
};

function RecordingTimer()
{
	if (IsRecording && !IsPause)
	{
		var getd = new Date();
		var recordingNow = getd.getTime();
		recordingTotal = recordingPauseTime + recordingNow - recordingStartTime;
		var recordingsec = Math.floor((recordingTotal) / 1000);

		if (recordingsec >= RECORD_LIMIT_SEC)
		{
			StopRecord();
			return;
		}

		document.getElementById("recordtime").innerText = 
			padLeft(Math.floor(recordingsec/60),2) + " : " + padLeft(recordingsec%60, 2);

		setTimeout(() => {
			RecordingTimer()
		}, 1000);
	}
};

///////////////////////////////////////////////////////////////////////////////////////////
// mouse event
///////////////////////////////////////////////////////////////////////////////////////////


function modeToolBarOnClick(event)
{
	switch (event.target.id)
	{
		case "mode_preview":
			showRecordBtn(true);
			fcSwitchMode(previewModeCfg);
			
			break;
			
		case "mode_playback":
			if ( IsRecording )
			{
				StopRecord();
				setTimeout( function() {
					fcSwitchMode(playbackModeCfg);
					showRecordBtn(false);
				}, 1000);
			}
			else
			{
				fcSwitchMode(playbackModeCfg);
				showRecordBtn(false);
			}
			break;
			
		case "mode_drawingBoard":
			if ( IsRecording )
			{
				StopRecord();
				setTimeout( function() {
					fcSwitchMode(drawingBoardModeCfg);
					showRecordBtn(false);
				}, 1000);
			}
			else
			{
				fcSwitchMode(drawingBoardModeCfg);
				showRecordBtn(false);
			}
			break;

		case "mode_about":
			
			fcToogleDlg("mode_about", aboutDlgOpen, aboutDlgClose);

			break;
	}

	toolBarOnMouseClick(event);
}

'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
//  constant 
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof DrawData == "undefined" )
{
    var DrawData = {};
    DrawData.id = 0;
    DrawData.fn = 1;
}


///////////////////////////////////////////////////////////////////////////////////////////
//  apply draw type
///////////////////////////////////////////////////////////////////////////////////////////

function drawingToolBarOnClick(event)
{	
	fcCloseActiveDlg();

	var fnTbl =
	[
		[ 'btn_freehand',		drawFreeHand		],
		[ 'btn_arrow',			drawArrow			],
		[ 'btn_eraser',			drawEraser			],
		[ 'btn_line',			drawLine			],
		[ 'btn_rectangleLine',	drawRectangleLine	],
		[ 'btn_rectangle',		drawRectangle		],
		[ 'btn_circleLine',		drawCircleLine		],
		[ 'btn_circle',		 	drawCircle			],
		[ 'btn_text',			drawText			],
	];

	var i;
	var mc = appFC.curMode;

	if ( event.target.id == "btn_erase_all" )
	{
		fcClearCanvas(mc.drawingCanvas);
		fcPushDrawingCanvas(mc);
		if ( mc.imageProcess ) mc.imageProcess();
	}
	else	
	{
		for ( i = 0; i < fnTbl.length; i++ )
		{
			if ( event.target.id == fnTbl[i][DrawData.id] )
			{
				appFC.fnDraw = fnTbl[i][DrawData.fn];
			}
		}
	}

	toolBarOnMouseClick(event);
}


///////////////////////////////////////////////////////////////////////////////////////////
//  common tool
///////////////////////////////////////////////////////////////////////////////////////////

function baseDrawGetSize(startX, startY, endX, endY)
{
	var size =
	{
		x: 0,
		y: 0,
		w: 0,
		h: 0,

		centerX: 0,
		centerY: 0,
		endX: 0,
		endY: 0,

		regularX: 0,
		regularY: 0,
		regularSide: 0,
	};

	// caculate normal size

	size.x = Math.min(startX, endX);
	size.y = Math.min(startY, endY);

	size.w = Math.abs(endX - startX);
	size.h = Math.abs(endY - startY);

	size.centerX = size.x + size.w/2;
	size.centerY = size.y + size.h/2;

	size.endX = size.x + size.w;
	size.endY = size.y + size.h;

	// caculate regular size

	size.regularX = size.x;
	size.regularY = size.y;

	if ( size.w > size.h )
	{
		size.regularSide = size.h;

		if ( size.regularX !== startX )
		{
			size.regularX = startX - size.regularSide;
		}
	}
	else
	{
		size.regularSide = size.w;

		if ( size.regularY !== startY )
		{
			size.regularY = startY - size.regularSide;
		}
	}	

	return size;
}

function baseDrawEllipse(context, startX, startY, endX, endY)
{
	var size;
	var startX, startY;
	var centerX, centerY;
	var endX, endY;

	size = baseDrawGetSize(startX, startY, endX, endY);

	startX = size.x;
	startY = size.y;
	centerX = size.centerX;
	centerY = size.centerY;
	endX = size.endX;
	endY = size.endY;

	context.moveTo(centerX, startY);

	context.bezierCurveTo(endX, startY, endX, endY, centerX, endY);
	context.bezierCurveTo(startX, endY, startX, startY, centerX, startY);
}


///////////////////////////////////////////////////////////////////////////////////////////
//  freehand
///////////////////////////////////////////////////////////////////////////////////////////
var lastTimeMill = 0;
var ptlist = new Array();
function drawFreeHand(state)
{
	var ctx = appFC.drawContext;	

	switch ( state )
	{
		case DrawState.start:

			fcClearDrawingCanvas();

			ctx.strokeStyle = appFC.drawStyle;
			ctx.lineWidth = appFC.lineWidth;
			
			ptlist.push({x:appFC.drawStartX,y:appFC.drawStartY});
			ptlist.push({x:appFC.drawStartX,y:appFC.drawStartY});
			ptlist.push({x:appFC.drawStartX,y:appFC.drawStartY});
			break;

		case DrawState.move:
			var d = new Date();

			if ( d.getTime() - lastTimeMill < 10 )
				break;

			lastTimeMill = d.getTime();

			ptlist.push({x:appFC.drawCurX, y:appFC.drawCurY});			
			ctx.clearRect(0, 0, appFC.drawCanvas.width, appFC.drawCanvas.height);
			ctx.moveTo(ptlist[0].x, ptlist[0].y);
			ctx.beginPath();

			for (var i = 1; i < ptlist.length - 2; i++) {
                var c = (ptlist[i].x + ptlist[i + 1].x) / 2;
                var d = (ptlist[i].y + ptlist[i + 1].y) / 2;
                ctx.quadraticCurveTo(ptlist[i].x, ptlist[i].y, c, d);
			}
			
			ctx.quadraticCurveTo(
                ptlist[i].x,
                ptlist[i].y,
                ptlist[i + 1].x,
                ptlist[i + 1].y
            );

			//ctx.lineTo(appFC.drawCurX, appFC.drawCurY);
			ctx.stroke();
			
			break;

		case DrawState.end:
			ptlist = [];
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  arrow
///////////////////////////////////////////////////////////////////////////////////////////

function getArrowEnds(startPos, endPos, size)
{
	var ends =
		{
			end1:
				{
					x: 0,
					y: 0,
				},
			end2:
				{
					x: 0,
					y: 0,
				}
		};

	var dx = endPos.x - startPos.x;
	var dy = endPos.y - startPos.y;
	var length = Math.sqrt(dx * dx + dy * dy);
	var unitDx = dx / length;
	var unitDy = dy / length;

	ends.end1.x = endPos.x - unitDx*size - unitDy*size;
	ends.end1.y = endPos.y - unitDy*size + unitDx*size;

	ends.end2.x = endPos.x - unitDx*size + unitDy*size;
	ends.end2.y = endPos.y - unitDy*size - unitDx*size;

	return ends;
}

function drawArrow(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			fcClearDrawingCanvas();

			var startPos = { x: appFC.drawStartX, y: appFC.drawStartY };
			var endPos = { x: appFC.drawCurX, y: appFC.drawCurY };

			var size = Math.max(appFC.lineWidth*2, appCfg.minArrowSize);

			var ends = getArrowEnds(startPos, endPos, size);
			
			var ctx = appFC.drawContext;

			
			ctx.fillStyle = appFC.drawStyle;
			ctx.strokeStyle = appFC.drawStyle;
			ctx.lineWidth = appFC.lineWidth;

			ctx.beginPath();
			ctx.moveTo(startPos.x, startPos.y);
			ctx.lineTo((ends.end1.x + ends.end2.x)/2, (ends.end1.y + ends.end2.y)/2);
			ctx.closePath();
			ctx.stroke();

			ctx.moveTo(endPos.x, endPos.y);
			ctx.lineTo(ends.end1.x, ends.end1.y);
			ctx.lineTo(ends.end2.x, ends.end2.y);
			ctx.closePath();
			ctx.fill();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  eraser
///////////////////////////////////////////////////////////////////////////////////////////

function drawEraserRect(context, centerX, centerY, size, style)
{
	var x, y;
	var offset = 1;

	x = centerX - size/2;
	y = centerY - size/2;

	if ( typeof style == "undefined" )
	{
		x -= offset;
		y -= offset;
		size += offset*2;
	
		context.clearRect(x, y, size, size);
	}
	else
	{
		context.beginPath();

		context.strokeStyle = style;
		context.fillStyle = style;
		context.lineWidth = 0;

		context.rect(x, y, size, size);
		context.fill();

		context.stroke();
	}
}

function drawEraser(state)
{
	var ctx = appFC.baseContext;
	var size = appFC.lineWidth * 2;

	if ( size < 10 ) size = 10;

	switch ( state )
	{
		case DrawState.start:

			fcClearDrawingCanvas();

			drawEraserRect(ctx, appFC.drawCurX, appFC.drawCurY, size, appFC.eraserStyle);
			
			break;

		case DrawState.move:

			drawEraserRect(ctx, appFC.drawLastX, appFC.drawLastY, size);

			drawEraserRect(ctx, appFC.drawCurX, appFC.drawCurY, size, appFC.eraserStyle);

			break;

		case DrawState.end:

			drawEraserRect(ctx, appFC.drawCurX, appFC.drawCurY, size);
			
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  line
///////////////////////////////////////////////////////////////////////////////////////////

function drawLine(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			fcClearDrawingCanvas();

			var ctx = appFC.drawContext;

			ctx.strokeStyle = appFC.drawStyle;
			ctx.lineWidth = appFC.lineWidth;

			ctx.beginPath();
			ctx.moveTo(appFC.drawStartX, appFC.drawStartY);
			ctx.lineTo(appFC.drawCurX, appFC.drawCurY);
			ctx.stroke();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  rectangle line
///////////////////////////////////////////////////////////////////////////////////////////

function drawRectangleLine(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			fcClearDrawingCanvas();

			var ctx = appFC.drawContext;
			var size;

			ctx.strokeStyle = appFC.drawStyle;
			ctx.lineWidth = appFC.lineWidth;

			size = baseDrawGetSize(appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);

			ctx.beginPath();

			//ctx.rect(size.x, size.y, size.w, size.h);
			if ( appFC.drawRegularShape ) {
				ctx.rect(size.regularX, size.regularY, size.regularSide, size.regularSide);
			}
			else {
				ctx.rect(size.x, size.y, size.w, size.h);
			}

			ctx.stroke();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  rectangle
///////////////////////////////////////////////////////////////////////////////////////////

function drawRectangle(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			fcClearDrawingCanvas();

			var ctx = appFC.drawContext;
			var size;

			ctx.fillStyle = appFC.drawStyle;
			ctx.strokeStyle = appFC.clearStyle;
			ctx.lineWidth = 0;

			size = baseDrawGetSize(appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);

			if ( appFC.drawRegularShape ) {
				ctx.rect(size.regularX, size.regularY, size.regularSide, size.regularSide);
			}
			else {
				ctx.rect(size.x, size.y, size.w, size.h);
			}
			
			ctx.fill();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  circle line
///////////////////////////////////////////////////////////////////////////////////////////

function drawCircleLine(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			var ctx = appFC.drawContext;
			var size;

			fcClearDrawingCanvas();

			ctx.strokeStyle = appFC.drawStyle;
			ctx.lineWidth = appFC.lineWidth;

			ctx.beginPath();

			if ( appFC.drawRegularShape ) {
				size = baseDrawGetSize(appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);
				baseDrawEllipse(ctx, size.regularX, size.regularY, size.regularX + size.regularSide/3*4, size.regularY + size.regularSide);
			}
			else {
				baseDrawEllipse(ctx, appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);
			}

			ctx.closePath();

			ctx.stroke();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  circle
///////////////////////////////////////////////////////////////////////////////////////////

function drawCircle(state)
{
	switch ( state )
	{
		case DrawState.start:
			break;

		case DrawState.move:

			var ctx = appFC.drawContext;
			var size;

			fcClearDrawingCanvas();

			ctx.fillStyle = appFC.drawStyle;

			if ( appFC.drawRegularShape ) {
				size = baseDrawGetSize(appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);
				baseDrawEllipse(ctx, size.regularX, size.regularY, size.regularX + size.regularSide/3*4, size.regularY + size.regularSide);
			}
			else {
				baseDrawEllipse(ctx, appFC.drawStartX, appFC.drawStartY, appFC.drawCurX, appFC.drawCurY);
			}
			
			ctx.fill();
			
			break;

		case DrawState.end:
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//  text
///////////////////////////////////////////////////////////////////////////////////////////

function drawText(state)
{
	switch ( state )
	{
		case DrawState.start:
			if ( false == appFC.drawText )
			{
				fcToogleDlg("textInputDlg", textInputDlgOpen, textInputDlgClose);
			}
			break;

		case DrawState.move:
			break;

		case DrawState.end:
			if ( false == appFC.drawText )
			{
				if ( getTextInputLength() )
				{
					fcCanvasCtrl(DrawState.end);
				}
			}
			break;
	}
}

function textInputDlgOpen()
{
	updateTextInputCfg();
	resetTextInputContent();

	appFC.drawFontStyle = appFC.drawStyle;

	appFC.drawing = false;

	appFC.drawText = true;

	showTextInputDlg(true);
}

function textInputDlgClose()
{
	showTextInputDlg(false);

	fcClearDrawingCanvas();

	baseDrawText();

	appFC.drawText = false;

	appFC.fnDraw(DrawState.end);
}

function textInputDlgCloseClick()
{
	fcCloseActiveDlg();
}

function baseDrawText()
{
	var startX, startY;
	var curY;
	var textH;
	var ctx;
	var content;
	var curPos;
	var curStr;
	var pos;
	var fontStyle;

	if ( 0 == getTextInputLength() ) return;

	startX = appFC.drawStartX;
	startY = appFC.drawStartY;

	textH = appFC.fontSize;

	ctx = appFC.drawContext;
	content = getTextInputContent();

	fontStyle = '';
	if ( appFC.fontBold ) fontStyle += 'Bold ';
	if ( appFC.fontItalic ) fontStyle += 'Italic ';

	ctx.fillStyle = appFC.drawFontStyle;
	ctx.strokeStyle = appFC.drawStyle;
	ctx.font = fontStyle + textH + 'pt' + ' ' + appFC.fontFamily;

	curPos = content;

	curY = startY;

	while ( -1 != curPos.indexOf('\n') )
	{
		curY += textH;

		pos = curPos.indexOf('\n');

		curStr = curPos.substring(0, pos);

		ctx.fillText(curStr, startX, curY);

		curPos = curPos.substring(pos + 1);
	}

	ctx.fillText(curPos, startX, curY + textH);
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// mode config
///////////////////////////////////////////////////////////////////////////////////////////

var previewModeCfg =
{
	baseId: 'previewArea',
		
	lToolBar: 'previewToolBar',
	rToolBar: 'drawingToolBar',
	bToolBar: 'drawingCfgArea',
	
	taskCtrl: previewModeTaskCtrl,
	start: previewModeStart,
	init: previewModeInit,
	onResize: previewModeOnResize,
	canvasCtrl: previewCanvasControl,
	imageProcess: previewImageProcess,
	chgBaseImgCtrl: previewChangeBaseImageCtrl,
	
	imgW: appCfg.defPreviewRsoX,
	imgH: appCfg.defPreviewRsoY,

	freeze: false,

	hasTempDraw: false,
	mergeTempDraw: false,
	
	reqUndo: false,
	reqRedo: false,

	flip: false,
	mirror: false,

	zoomRatio: 1,
	zoomX: 0,
	zoomY: 0,
	zoomW: appCfg.defPreviewRsoX,
	zoomH: appCfg.defPreviewRsoY,

	panWinW: 0,
	panWinH: 0,

	zoomScrollBarPos: 0,
};


///////////////////////////////////////////////////////////////////////////////////////////
// flow control
///////////////////////////////////////////////////////////////////////////////////////////

function previewModeTaskCtrl(isEnter)
{
	var mc = previewModeCfg;

	if ( isEnter )
	{
		mc.panMove = false;

		if ( appFC.capturePeriodRun ) showPeriodicCaptureInfoBar(true);

		ScrollBarDlgSetPos('zoomDlg', mc.zoomScrollBarPos);
		
		if ( mc.zoomRatio > 1 ) fcShowPanWin(true);
	}
	else
	{
		displayPictureWinClose();
		showPeriodicCaptureInfoBar(false);
		fcShowPanWin(false);
	}
}

function previewModeStart()
{
	previewCheckReqVideoResolution();
	
	previewModeOnResize();
}

function previewModeInit()
{
	var mc = previewModeCfg;

	videoW = mc.imgW = appFC.reqImgW;
	videoH = mc.imgH = appFC.reqImgH;
	resetCanvasGroupSize(mc, videoW, videoH);

	getCanvasGroupContext(mc);

	fcInitCanvasArray(mc);

	fcInitCropWinEvent();

	fcShowPanWin(false);

	fcShowMode(mc, false);
}

function previewModeOnResize()
{
	if ( previewModeCfg.freeze ) fcUpdateImage();

	fcUpdatePanWin();
	fcUpdateCropWin();

	resizePeriodicCaptureInfoBar();
}


///////////////////////////////////////////////////////////////////////////////////////////
// tool bar
///////////////////////////////////////////////////////////////////////////////////////////

var recordStatus = 0;

function previewToolBarOnClick(event)
{
	var mc = previewModeCfg;

	switch ( event.target.id )
	{
		case 'btn_freeze':
			mc.freeze = !mc.freeze;
			break;

		case "btn_undo":
			fcCanvasCtrl(DrawState.undo);
			break;

		case "btn_redo":
			fcCanvasCtrl(DrawState.redo);
			break;

		case "btn_mirrow":
			mc.mirror = !mc.mirror;
			mc.zoomX = mc.imgW - mc.zoomX - mc.zoomW;
			fcUpdateCropWin();
			fcCanvasCtrl(DrawState.mirror);
			break;
			
		case "btn_flip":
			mc.flip = !mc.flip;
			mc.zoomY = mc.imgH - mc.zoomY - mc.zoomH;
			fcUpdateCropWin();
			fcCanvasCtrl(DrawState.flip);
			break;

		case "btn_config":
			fcCloseActiveDlg();			
			//fcToogleDlg("btn_config", configDlgOpen, configDlgClose);
			$("#configDlg").toggle(500);			
			break;

		case "btn_brightness":		
			fcToogleDlg("btn_brightness", previewBrightnessDlgOpen, previewBrightnessDlgClose);
			break;

		case "btn_whitebalance":
			fcToogleDlg("btn_whitebalance", previewWhiteBalanceDlgOpen, previewWhiteBalanceDlgClose);
			break;
			
		case "btn_zoom":
			$("#configDlg").hide();
			fcToogleDlg("btn_zoom", fcZoomDlgOpen, fcZoomDlgClose);
			break;

		case "btn_setting":
			fcToogleDlg("btn_setting", previewSettingDlgOpen, previewSettingDlgClose);
			break;

		case "btn_capture":			
			if (typeof window.stream=="undefined" || 
				!window.stream.active || 
				!IsDeviceConnected)
				 break;
			fcSaveCanvasToFile(mc['combineCanvas']);
			break;

		case "btn_timesave":
			if (typeof window.stream=="undefined" || 
				!window.stream.active || 
				!IsDeviceConnected)
				 break;
			if ( false == appFC.capturePeriodRun )
			{
				fcToogleDlg("btn_timesave", previewPeriodicCaptureDlgOpen, previewPeriodicCaptureDlgClose);
			}
			break;

		case "btn_importPic":
			lsSelectFilesDlg();
			break;

		case "btn_record":
			if (typeof window.stream=="undefined" || 
				!window.stream.active || 
				!IsDeviceConnected)
				 break;

			if (appFC.capturePeriodRun)
			{
				stopPeriodicCapture()
			}

			if (mc.flip || mc.mirror)
			{
				mc.flip = false;
				mc.mirror = false;

				fcUpdateCropWin();
				fcCanvasCtrl(DrawState.flip);
				fcCanvasCtrl(DrawState.mirror);

				toolBarSetElementState('previewToolBar', "btn_flip", 0);
				toolBarSetElementState('previewToolBar', "btn_mirrow", 0);
			}
				 
			if ( IsRecording )
			{
				StopRecord();
			}
			else
			{
				StartRecord();
			}
			break;

		case "btn_pause":
			if ( IsRecording )
			{
				if ( IsPause )
				{
					recordingTimeBlinkStop();
					ResumeRecord();
				}
				else
				{
					recordingTimeBlinkStart();
					PauseRecord();
				}
			}				
			break;
	}

	toolBarOnMouseClick(event);
}

var blinkId;
function blink_text() {
    $('#recordtime').fadeOut(500);
    $('#recordtime').fadeIn(500);
}

function recordingTimeBlinkStart()
{
	blinkId = setInterval(blink_text, 2000);
}

function recordingTimeBlinkStop()
{
	clearInterval(blinkId);
}

///////////////////////////////////////////////////////////////////////////////////////////
// canvas
///////////////////////////////////////////////////////////////////////////////////////////

function previewOverlayCanvas(dstContext, srcCanvas)
{
	var mc = previewModeCfg;

	dstContext.drawImage(srcCanvas, 0, 0, mc.imgW, mc.imgH);
}

function previewUpdateFrameRate()
{
	var i;
	var sum;
	
	if ( false == appFC.frameRateInit )
	{
		appFC.frameRateInit = true;

		appFC.frameLastTime = new Date();

		appFC.frameSlotIdx = appFC.frameCnt;

		for ( i = 0; i < appFC.frameCnt; i++ )
		{
			appFC.frameInterval[i] = 0;
		}

		return;
	}

	var curTime = new Date();
	var msDiff = curTime - appFC.frameLastTime;
	var idx = appFC.frameSlotIdx % appFC.frameCnt;

	appFC.frameLastTime = curTime;
	appFC.frameInterval[idx] = msDiff;

	sum = 0;
	for ( i = 0; i < appFC.frameCnt; i++ )
	{
		sum = sum + appFC.frameInterval[i];
	}

	appFC.frameRate = appFC.frameCnt * 1000 / sum;

	appFC.frameRate = appFC.frameRate.toFixed(2);

	if ( 0 == idx ) fcUpdateSysInfoData();

	appFC.frameSlotIdx = idx + 1;
}

var colorMatrix = 
		['1','0','0','0','0'
		,'0','1','0','0','0'
		,'0','0','1','0','0'
		,'0','0','0','1','0'];


function previewVideoStreamUpdate(videoCanvas)
{
	var mc = previewModeCfg;

	previewUpdateFrameRate();

	if ( appFC.checkVideoResolution )
	{
		appFC.checkVideoResolution = false;

		previewCheckReqVideoResolution();

		return;
	}

	if ( mc.freeze ) return;

	// r = [0], g = [6], b = [12], a = [18]
	colorMatrix[0] = 1 + appFC.previewBrightnessAdj/100 + appFC.previewWhiteBalanceAdj/100;
	colorMatrix[6] = 1 + appFC.previewBrightnessAdj/100;
	colorMatrix[12] = 1 + appFC.previewBrightnessAdj/100 - appFC.previewWhiteBalanceAdj/100;
	
	var filterStr = '';
	var filter = document.querySelector('#filter feColorMatrix');
	if (filter != null)
	{
		colorMatrix.forEach(item => filterStr += item +' ');
		//var redToBlue = '0 0 1 0 0  0 0.55 0 0 0  1 0 0 0 0  0 0 0 1 0 ';		
		filter.setAttribute('values', filterStr);
		mc.baseImageContext.filter = 'url(#filter)';		
	}		
	
	previewOverlayCanvas(mc.baseImageContext, videoCanvas);

	previewImageProcess();
}

var lastPreviewRotate = 0;

function previewImageProcess()
{
	var mc = previewModeCfg;
	var ctx = mc.imageProcessContext;

	// brightness caibass
	// if ( appFC.previewBrightnessAdj != 0 || appFC.previewWhiteBalanceAdj != 0 )
	// {
	// 	var vv = document.getElementById('videoSrc');
	// 	vv.style.setProperty('filter', "brightness(" + appFC.previewBrightnessAdj + "%)");
	// 	vv.style.setProperty('filter', "hue-rotate(" + appFC.previewWhiteBalanceAdj + "deg)");
	// }

	// if ( appFC.previewBrightnessAdj != 0 || appFC.previewWhiteBalanceAdj != 0 )
	// {
	// 	var pixels = mc.baseImageContext.getImageData(0, 0, mc.imgW, mc.imgH);
	// 	var d = pixels.data;
	// 	var adj = appFC.previewBrightnessAdj;
	// 	var adjWb = appFC.previewWhiteBalanceAdj;
		
	// 	for ( var i=0; i<d.length; i+=4 )
	// 	{
	// 		d[i] += adj + adjWb;
	// 		d[i+1] += adj;
	// 		d[i+2] += adj - adjWb;
	// 	}

	// 	mc.baseImageContext.putImageData(pixels, 0, 0);
	// }

	// flip & mirror
	var scaleX = 1;
	var scaleY = 1;
	var offsetX = 0;
	var offsetY = 0;

	if ( mc.mirror )
	{
		scaleX = -1;
		offsetX = mc.imgW;
	}

	if ( mc.flip )
	{
		scaleY = -1;
		offsetY = mc.imgH;
	}	

	// rotate
	if ( appFC.previewRotate == 0)
	{
		ctx.save();
		ctx.translate(offsetX, offsetY);
		ctx.scale(scaleX, scaleY);
		previewOverlayCanvas(mc.imageProcessContext, mc.baseImageCanvas);
		ctx.restore();
		lastPreviewRotate = 0;
	}
	else
	{		
		if ( appFC.previewRotate != lastPreviewRotate )
		{
			ctx.fillStyle = 'black';
			ctx.fillRect(0, 0, mc.imgW, mc.imgH);
			//ctx.clearRect(0, 0, mc.imgW, mc.imgH);
		}
		
		ctx.save();
		ctx.translate(mc.imgW/2, mc.imgH/2);
		ctx.rotate((Math.PI / 180) * appFC.previewRotate);
		ctx.scale(scaleX, scaleY)
		ctx.drawImage(mc.baseImageCanvas, -mc.imgW/2, -mc.imgH/2, mc.imgW, mc.imgH);
		ctx.restore();

		lastPreviewRotate = appFC.previewRotate;
	}	

	// next step
	previewRedoUndo();
}

function previewRedoUndo()
{
	var mc = previewModeCfg;

	if ( mc.reqUndo )
	{
		mc.reqUndo = false;

		fcDrawingUndo(mc);
	}
	else if ( mc.reqRedo )
	{
		mc.reqRedo = false;

		fcDrawingRedo(mc);
	}
	else
	{
		previewMergeDrawing();
	}
}

function previewMergeDrawing()
{
	var mc = previewModeCfg;

	if ( mc.zoomRatio == 1 )
	{
		previewOverlayCanvas(mc.combineContext, mc.imageProcessCanvas);
	}
	else
	{
		mc.combineContext.drawImage(mc.imageProcessCanvas, mc.zoomX, mc.zoomY, mc.zoomW, mc.zoomH, 0, 0, mc.imgW, mc.imgH);
	}

	if ( 1 === appCfg.disableDrawingCanvasOverlay )
	{
		previewDisplay();

		return;
	}

	if ( mc.mergeTempDraw )
	{
		mc.mergeTempDraw = false;
		
		previewOverlayCanvas(mc.drawingContext, mc.tempDrawingCanvas);

		fcPushDrawingCanvas(mc);
	}

	previewOverlayCanvas(mc.combineContext, mc.drawingCanvas);

	previewMergeTempDrawing();
}

function previewMergeTempDrawing()
{
	var mc = previewModeCfg;

	if ( mc.hasTempDraw )
	{
		previewOverlayCanvas(mc.combineContext, mc.tempDrawingCanvas);
	}
	
	previewDisplay();
}

function previewDisplay()
{
	var mc = previewModeCfg;

	if ( fcIsModeActive(mc) )
	{
		fcUpdateImage();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// canvas control
///////////////////////////////////////////////////////////////////////////////////////////

function previewCanvasControl(state)
{
	var mc = previewModeCfg;
	
	switch ( state )
	{
		case DrawState.start:
			return false;
			break;
			
		case DrawState.move:
			mc.hasTempDraw = true;
			break;
			
		case DrawState.end:
			if ( false == appFC.drawText )
			{
				mc.hasTempDraw = false;
				mc.mergeTempDraw = true;
			}
			break;

		case DrawState.undo:
			if ( mc.canvasStep > 0 )
			{
				mc.reqUndo = true;
			}
			break;

		case DrawState.redo:
			if ( mc.canvasStep < (mc.canvasArray.length - 1) )
			{
				mc.reqRedo = true;
			}
			break;
	}

	if ( mc.freeze ) return false;

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
// configDlg
///////////////////////////////////////////////////////////////////////////////////////////

function configDlgOpen()
{
	showConfigDlg(true);
}

function configDlgClose()
{
	showConfigDlg(false);
}

///////////////////////////////////////////////////////////////////////////////////////////
// brightness
///////////////////////////////////////////////////////////////////////////////////////////

function previewBrightnessDlgOpen()
{
	showBrightnessDlg(true);
}

function previewBrightnessDlgClose()
{
	showBrightnessDlg(false);
}

///////////////////////////////////////////////////////////////////////////////////////////
// white balance
///////////////////////////////////////////////////////////////////////////////////////////

function previewWhiteBalanceDlgOpen()
{
	showWhiteBalanceDlg(true);
}

function previewWhiteBalanceDlgClose()
{
	showWhiteBalanceDlg(false);
}

///////////////////////////////////////////////////////////////////////////////////////////
// setting dialog
///////////////////////////////////////////////////////////////////////////////////////////

function previewSettingDlgOpen()
{
	showSettingDlg(true);
}

function previewSettingDlgClose()
{
	showSettingDlg(false);

	fcUpdateFontCfg();

	previewCheckReqVideoResolution();
}

function previewSetttingDlgCloseClick()
{
	fcCloseActiveDlg();
}

function previewCheckReqVideoResolution()
{
	var eleId;
	var idx;

	eleId = document.getElementById('sdVideoRsoScrollbar');
	idx = eleId.selectedIndex;

	appFC.reqImgW = appFC.curVideoResolutionList[idx][VrField.w];
	appFC.reqImgH = appFC.curVideoResolutionList[idx][VrField.h];

	if ( (appFC.reqImgW != appFC.imgW) || (appFC.reqImgH != appFC.imgH) )
	{
		fcChangeBaseImageSize(appFC.reqImgW, appFC.reqImgH);

		fcUpdateSelVideoResolutionIdx(idx);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// video resolution change control
///////////////////////////////////////////////////////////////////////////////////////////

function previewChangeBaseImageCtrl(state, w, h)
{
	var mc = previewModeCfg;
	
	switch ( state )
	{
		case BaseImgChg.before:
			
			mc.zoomRatio = 1;
			fcShowPanWin(false);
			ScrollBarDlgSetPos('zoomDlg', 0);

			mc.freeze = false;
			toolBarSetElementState('previewToolBar', "btn_freeze", 0);

			disconnectVideoSrc();
			
			videoW = mc.imgW = w;
			videoH = mc.imgH = h;
			
			break;

		case BaseImgChg.after:

			connectVideoSrc();
			
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// periodic capture
///////////////////////////////////////////////////////////////////////////////////////////

function previewPeriodicCaptureDlgClickClose()
{
	fcCloseActiveDlg();
}

function previewPeriodicCaptureDlgClose()
{
	showPeriodicCaptureDlg(false);
}

function previewPeriodicCaptureDlgOpen()
{
	showPeriodicCaptureDlg(true);
}

function previewPeriodicCaptureRun()
{
	var totalFrame;

	fcCloseActiveDlg();

	appFC.capturePeriodSec = periodicCaptureDlgPeriod();
	appFC.captureIntervalHour = periodicCaptureDlgInterval();

	if ( appFC.capturePeriodSec < 1 ) return;

	totalFrame = appFC.captureIntervalHour*60/appFC.capturePeriodSec;
	totalFrame = Math.floor(totalFrame);
	appFC.capturePeriodTotalCnt = totalFrame;

	if ( appFC.capturePeriodTotalCnt )
	{
		appFC.capturePeriodRun = true;

		appFC.capturePeriodCurCnt = 0;

		appFC.capturePeriodMs = appFC.capturePeriodSec*1000;

		previewPeriodicCaptureSnap();

		showPeriodicCaptureInfoBar(true);
	}
}

function previewPeriodicCaptureSnap()
{
	var mc = previewModeCfg;
	
	if ( appFC.capturePeriodCurCnt >= appFC.capturePeriodTotalCnt )
	{
		appFC.capturePeriodRun = false;

		showPeriodicCaptureInfoBar(false);
		
		return;
	}

	if ( false == appFC.capturePeriodRun ) return; 

	if ( fcSaveCanvasToFile(mc['combineCanvas']) )
	{
		appFC.capturePeriodCurCnt += 1;

		updatePeriodicCaptureInfoBar();

		if ( appFC.capturePeriodCurCnt >= appFC.capturePeriodTotalCnt )
		{
			appFC.capturePeriodMs = 1000;
		}
	}

	setTimeout(function () {
		previewPeriodicCaptureSnap();
		},
		appFC.capturePeriodMs);
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// constant
///////////////////////////////////////////////////////////////////////////////////////////

if ( typeof PlbToolBarType == "undefined" )
{
    var PlbToolBarType = {};
    PlbToolBarType.thumbnail = 0;
    PlbToolBarType.image = 1;
}


///////////////////////////////////////////////////////////////////////////////////////////
// mode config
///////////////////////////////////////////////////////////////////////////////////////////

var playbackModeCfg =
{
	baseId: 'playbackArea',
		
	lToolBar: 'thumbnailToolBar',
	rToolBar: 'playbackRToolBar',
	bToolBar: 'playbackInfoBar',

	taskCtrl: playbackModeTaskCtrl,
	start: playbackModeStart,
	init: playbackModeInit,
	onResize: playbackModeOnResize,
	canvasCtrl: playbackCanvasControl,
	imageProcess: playbackImageProcess,
	chgBaseImgCtrl: playbackChangeBaseImageCtrl,
	
	imgW: appCfg.defPictureW,
	imgH: appCfg.defPictureH,

	curImage: 0,

	curMode: PlbToolBarType.thumbnail,

	zoomRatio: 1,
	zoomX: 0,
	zoomY: 0,
	zoomW: appCfg.defPreviewRsoX,
	zoomH: appCfg.defPreviewRsoY,

	panWinW: 0,
	panWinH: 0,

	zoomScrollBarPos: 0,
};


///////////////////////////////////////////////////////////////////////////////////////////
// flow control
///////////////////////////////////////////////////////////////////////////////////////////

function playbackModeTaskCtrl(isEnter)
{
	if ( isEnter )
	{
		fcShowDisplayCanvas(false);
	}
	else
	{
		displayPictureWinClose();
		
		fcShowDisplayCanvas(true);

		fcShowPanWin(false);
	}
}

function playbackModeInit()
{
	var mc = playbackModeCfg;

	getCanvasGroupContext(mc);

	fcInitCanvasArray(mc);

	playbackCreateThumbnailDiv();

	playbackCreateInfoBar();
	
	fcShowMode(mc, false);
}

function playbackModeOnResize()
{
	fcUpdateImage();
	
	fcUpdatePanWin();
	fcUpdateCropWin();
}

function playbackModeStart()
{
	var mc = playbackModeCfg;

	if ( mc.curMode == PlbToolBarType.image )
	{
		playbackImageProcess();

		fcShowDisplayCanvas(true);
		
		if ( mc.zoomRatio > 1 )
		{
			fcUpdatePanWin();
			fcUpdateCropWin();
			fcShowPanWin(true);
		}
	}
	else
	{
		mc.zoomScrollBarPos = 0;
		mc.zoomRatio = 1;
	}
	
	ScrollBarDlgSetPos('zoomDlg', mc.zoomScrollBarPos);	
}


///////////////////////////////////////////////////////////////////////////////////////////
// thumbnail
///////////////////////////////////////////////////////////////////////////////////////////

function playbackCreateThumbnailDiv()
{
	createDivElement('playbackArea', 'thumbDiv');

	var iconW = appCfg.toolBarIconW;
	var iconH = appCfg.toolBarIconH;
	var titleH = appCfg.titleIconH;

	var css =
	{
		position: 'absolute',
		left: iconW + 'px',
		top: titleH + 'px',
		width: 'calc(100% - ' + (iconW*2) + 'px)',
		height: 'calc(100% - ' + (titleH + iconH) + 'px)',
		'text-align': 'center',
		overflow: 'auto',
	};

	$('#thumbDiv').css(css);
}

function playbackShowThumbnail(mode)
{
	showElement('thumbDiv', mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// tool bar
///////////////////////////////////////////////////////////////////////////////////////////

function thumbnailToolBarOnClick(event)
{
	var eleId;
	var i;

	switch ( event.target.id )
	{
		case "btn_delete":
			
			eleId = $('img.selected,Video.selected');

			if ( eleId.length )
			{
				
				fcToogleDlg("btn_delete", playbackDeleteConfirmDlgOpen, playbackDeleteConfirmDlgClose);
			}

			break;

		case "btn_saveDisk":

			eleId = $('img.selected,Video.selected');

			lsSaveFilesToSelectedFolder(eleId);
			//lsSaveFilesToDefaultFoloer(eleId);
			
			break;

		case "btn_toggleAll":

			if ( appFC.thumbFileCnt != appFC.thumbSelectedFileCnt )
			{
				gallerySelectAll(true);

				appFC.thumbSelectedFileCnt = appFC.thumbFileCnt;
			}
			else
			{
				gallerySelectAll(false);

				appFC.thumbSelectedFileCnt = 0;
			}

			galleryUpdateFileInfo();
			
			break;
	}

	toolBarOnMouseClick(event);
}

function picturePlaybackToolBarOnClick(event)
{
	var mc = playbackModeCfg;
	
	if (event.target.id == 'ppbtn_transform' &&
		$("#cropBox").length == 0)
	{
		attachCropBox(dispW, dispH);
		fcToogleDlg("btn_delete", perspectiveImageConfirmDlgOpen, perspectiveImageConfirmDlgClose);
	}
	else
	{
		perspectiveImageConfirmDlgClose();
		removeCropBox();	
		//$("svg").remove();
	}

	switch ( event.target.id )
	{
		case "ppbtn_closeAndBackPlayback":
			
			playbackShowToolBar(false);
			playbackSetToolBarType(PlbToolBarType.thumbnail);
			playbackShowToolBar(true);

			fcShowDisplayCanvas(false);
			playbackShowThumbnail(true);

			mc.curMode = PlbToolBarType.thumbnail;

			mc.zoomRatio = 1;
			mc.zoomScrollBarPos = 0;
			ScrollBarDlgSetPos('zoomDlg', mc.zoomScrollBarPos);	
			fcShowPanWin(false);
			fcCloseActiveDlg();

			break;

		case "ppbtn_redo":

			fcCanvasCtrl(DrawState.redo);
			
			break;

		case "ppbtn_undo":
			
			fcCanvasCtrl(DrawState.undo);
			
			break;

		case "ppbtn_importPic":

			lsSelectFilesDlg();
			
			break;

		case "ppbtn_savePlayback":

			fcSaveCanvasToFile(mc['combineCanvas']);

			break;

		case "ppbtn_zoom":
			
			fcToogleDlg("ppbtn_zoom", fcZoomDlgOpen, fcZoomDlgClose);
			
			break;

		case "ppbtn_transform":			
			
			// if ($("svg").length)
			// {
			// 	perspectiveImageConfirmDlgClose();			
			// 	$("svg").remove();
			// }
			// else
			// {
			// 	attachCropBox(dispW, dispH);
			// 	fcToogleDlg("btn_delete", perspectiveImageConfirmDlgOpen, perspectiveImageConfirmDlgClose);
			// }			
			break;

		case "ppbtn_ocr":
			OCR();
			break;
	}

	toolBarOnMouseClick(event);
}

function playbackShowToolBar(mode)
{
	var mc = playbackModeCfg;

	showElement(mc.lToolBar, mode);
	showElement(mc.rToolBar, mode);
	showElement(mc.bToolBar, mode);
}

function playbackSetToolBarType(type)
{
	var mc = playbackModeCfg;

	switch ( type )
	{
		case PlbToolBarType.thumbnail:
			mc.lToolBar = 'thumbnailToolBar';
			mc.rToolBar = 'playbackRToolBar';
			mc.bToolBar = 'playbackInfoBar';
			break;
			
		case PlbToolBarType.image:
			mc.lToolBar = 'picPlaybackToolBar';
			mc.rToolBar = 'drawingToolBar';
			mc.bToolBar = 'drawingCfgArea';
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// display single image
///////////////////////////////////////////////////////////////////////////////////////////

function playbackDisplayVideoMode(image)
{
	var mc = playbackModeCfg;
	
	mc.curImage = image;

	mc.curMode = PlbToolBarType.image;

	fcChangeBaseImageSize(image.width, image.height);

	fcCloseActiveDlg();

	fcUpdatePanWin();
	fcUpdateCropWin();
}

///////////////////////////////////////////////////////////////////////////////////////////
// Perspective Image
///////////////////////////////////////////////////////////////////////////////////////////

function perspectiveImageConfirmDlgClickHandler(isYes)
{
	if ( isYes )
	{
		Perspective_Image();				
	}
	
	fcCloseActiveDlg();
}

function perspectiveImageConfirmDlgOpen()
{
	setConfirmDlgClickHandler(perspectiveImageConfirmDlgClickHandler);

	setConfirmDlgTip("btnStart", "btnCancel");

	showConfirmDlg(true);
}

function perspectiveImageConfirmDlgClose()
{
	showConfirmDlg(false);
}

function Perspective_Image()
{
	var path = document.getElementById("croppath").getAttribute("d");
	var commands = path.split(/(?=[LMC])/);
	var rectSrc = [];
	var rectTgt = [];
				
	rectSrc[0] = parseInt(commands[0].replace("M","").replace("L","").replace("Z","").split(",")[0]);
	rectSrc[1] = parseInt(commands[0].replace("M","").replace("L","").replace("Z","").split(",")[1]);
	rectSrc[2] = parseInt(commands[1].replace("M","").replace("L","").replace("Z","").split(",")[0]);
	rectSrc[3] = parseInt(commands[1].replace("M","").replace("L","").replace("Z","").split(",")[1]);
	rectSrc[4] = parseInt(commands[2].replace("M","").replace("L","").replace("Z","").split(",")[0]);
	rectSrc[5] = parseInt(commands[2].replace("M","").replace("L","").replace("Z","").split(",")[1]);
	rectSrc[6] = parseInt(commands[3].replace("M","").replace("L","").replace("Z","").split(",")[0]);
	rectSrc[7] = parseInt(commands[3].replace("M","").replace("L","").replace("Z","").split(",")[1]);

	var mc = playbackModeCfg;
	var image = mc.curImage;
	var dispW = document.getElementById("displayCanvas").width
	var dispH = document.getElementById("displayCanvas").height	

	// scale rectSrc to image size
	if (mc.zoomRatio > 1 )
	{
		rectSrc[0] = parseInt(mc.zoomX + rectSrc[0] / dispW * image.width / mc.zoomRatio);
		rectSrc[1] = parseInt(mc.zoomY + rectSrc[1] / dispH * image.height / mc.zoomRatio);
		rectSrc[2] = parseInt(mc.zoomX + rectSrc[2] / dispW * image.width / mc.zoomRatio);
		rectSrc[3] = parseInt(mc.zoomY + rectSrc[3] / dispH * image.height / mc.zoomRatio);
		rectSrc[4] = parseInt(mc.zoomX + rectSrc[4] / dispW * image.width / mc.zoomRatio);
		rectSrc[5] = parseInt(mc.zoomY + rectSrc[5] / dispH * image.height / mc.zoomRatio);
		rectSrc[6] = parseInt(mc.zoomX + rectSrc[6] / dispW * image.width / mc.zoomRatio);
		rectSrc[7] = parseInt(mc.zoomY + rectSrc[7] / dispH * image.height / mc.zoomRatio);
	}
	else
	{
		rectSrc[0] = rectSrc[0] / dispW * image.width;
		rectSrc[1] = rectSrc[1] / dispH * image.height;
		rectSrc[2] = rectSrc[2] / dispW * image.width;
		rectSrc[3] = rectSrc[3] / dispH * image.height;
		rectSrc[4] = rectSrc[4] / dispW * image.width;
		rectSrc[5] = rectSrc[5] / dispH * image.height;
		rectSrc[6] = rectSrc[6] / dispW * image.width;
		rectSrc[7] = rectSrc[7] / dispH * image.height;
	}	

	// calc rectTgt
	var startX = (rectSrc[0] + rectSrc[6]) / 2;
	var startY = (rectSrc[1] + rectSrc[3]) / 2;
	var width = Math.floor(((rectSrc[2] + rectSrc[4])/2 - startX));
	var height = Math.floor(((rectSrc[7] + rectSrc[5])/2 - startY));

	rectTgt[0] = startX;			rectTgt[1] = startY;
	rectTgt[2] = startX + width;	rectTgt[3] = startY;
	rectTgt[4] = startX + width;	rectTgt[5] = startY + height;
	rectTgt[6] = startX;			rectTgt[7] = startY + height;

	// calc matrix
	perspectiveTran = PerspT(rectSrc, rectTgt);	

	////////////////////////////////////////

	var canvas = imageToCanvas(image);
	var ctxSrc = canvas.getContext('2d');
	var pixels = ctxSrc.getImageData(0, 0, width, height);
	var srcCopy = ctxSrc.getImageData(0, 0, image.width, image.height).data.slice(0);

	///////////////////////////////////////////
	for ( var y = 0; y<height; y++ )
	{
		for ( var x = 0; x<width; x++ )
		{
			var res = perspectiveTran.transformInverse(x+startX,y+startY);
			var index = (y*width+x)*4;
			var SourceIndex = (Math.round(res[1]) * image.width + Math.round(res[0])) * 4;
			//var SourceIndex = (Math.floor(res[1]) * image.width + Math.floor(res[0])) * 4;

			pixels.data[index] = srcCopy[SourceIndex];
			pixels.data[index+1] = srcCopy[SourceIndex+1];
			pixels.data[index+2] = srcCopy[SourceIndex+2];
		}
	}
	
	canvas = document.getElementById('saveFileCanvas');
	canvas.width = width;
	canvas.height = height;

	var ctxTgt = canvas.getContext('2d');
	ctxTgt.putImageData(pixels, 0, 0);

	var img = new Image;
	img.onload = function()
	{
		playbackDisplayImgMode(this);
	};
	
	img.width = width;
	img.height = height;
	img.src = canvas.toDataURL();

	// reset zoom
	fcUpdateZoomRatio(1, 0);
	
	delete srcCopy;

	function imageToCanvas(image)
	{
		var canvas = document.getElementById('saveFileCanvas');
		var ctx = canvas.getContext('2d');

	    $('#saveFileCanvas').attr('width', image.width);
	    $('#saveFileCanvas').attr('height', image.height);
		ctx.drawImage(image, 0, 0, image.width, image.height);
		return canvas;
	}
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}


function LoadPng2Blob(blob)
{
	var CHUNK_SIZE = 256 * 1024;
	var start = 0;
	var stop = CHUNK_SIZE;      

	var remainder = blob.size % CHUNK_SIZE;
	var chunks = Math.floor(blob.size / CHUNK_SIZE);      

	var chunkIndex = 0;

	if (remainder != 0) chunks = chunks + 1;     

	var fr = new FileReader();
	var canvas = document.getElementById('displayCanvas');

	fr.onload = function() {
		var message = {
			blobAsText: fr.result,
			mimeString: "mimeString",
			chunks: chunks,
		};          
		// APP_ID was obtained elsewhere
		chrome.runtime.sendMessage(OcrAppID, message, function(result) {
			if (chrome.runtime.lastError) {				
				console.log("could not send message to app");
			} 
		});		
		processChunk();
	};

	fr.onerror = function() { appendLog("An error ocurred while reading file"); };
	processChunk();

	function processChunk() {
		chunkIndex++;         

		// exit if there are no more chunks
		if (chunkIndex > chunks) {
			return;
		}

		if (chunkIndex == chunks && remainder != 0) {
			stop = start + remainder;
		}                           

		var blobChunk = blob.slice(start, stop);

		// prepare for next chunk
		start = stop;
		stop = stop + CHUNK_SIZE;

		// convert chunk as binary string
		fr.readAsBinaryString(blobChunk);
	} 
}

function OCR()
{
	var message = {
		launch: true,
		senderName: appName
	};

	chrome.runtime.sendMessage(OcrAppID, message, function(e)
	{
		//console.log(e);		
		if (chrome.runtime.lastError)
		{
			console.log("could not send message to app");
			$("#dialog").dialog({
				autoOpen: false,
				width: 250,
                height: 140,
				show: {
					effect: "fade",
					duration: 500
				},
				hide: {
					effect: "fade",
					duration: 500
				},
				buttons: { 
					"Add it now": function() {
						var newURL = OcrAppLink;//"https://chrome.google.com/webstore/detail/smart-dc-ocr/kbepikplfggknbcgfedhldpdmncjegih?hl=en-GB&authuser=1";
						//chrome.tabs.create({ url: newURL });
						window.open(newURL);
						$(this).dialog("close"); 
					},		 
					"Later": function() { $(this).dialog("close"); }		 
				}
			});
			$("#dlgTxt").text("This feature requires installing a free OCR extension from the Chrome Web Store.");
			//$("#dlgTxt").text("This function depends on Smart DC OCR App. Please download the App from Chrome Web Store or Offical Website.");
			$("#dialog").dialog( "open" );			
			return;
		} 

		if (e != "Success")	{}

		var canvas = document.getElementById('displayCanvas');
		canvas.toBlob(function(blob)
		{
			setTimeout(function(){
				LoadPng2Blob(blob);
			},200);
		});
	});
}

function playbackDisplayImgMode(image)
{	
	var mc = playbackModeCfg;
	
	mc.curImage = image;
	
	mc.curMode = PlbToolBarType.image;
	
	fcChangeBaseImageSize(image.width, image.height);
	
	fcCloseActiveDlg();
	
	fcUpdatePanWin();
	fcUpdateCropWin();
}

function playbackChangeBaseImageCtrl(state, w, h)
{
	var mc = playbackModeCfg;
	
	switch ( state )
	{
		case BaseImgChg.before:
			break;

		case BaseImgChg.after:

			playbackShowToolBar(false);
			playbackSetToolBarType(PlbToolBarType.image);
			playbackShowToolBar(!fullPhotoMode);

			mc.imgW = mc.curImage.width;
			mc.imgH = mc.curImage.height;

			mc.baseImageContext.drawImage(mc.curImage, 0, 0, mc.curImage.width, mc.curImage.height);
			playbackImageProcess();

			playbackShowThumbnail(false);
			fcShowDisplayCanvas(true);
			
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// canvas
///////////////////////////////////////////////////////////////////////////////////////////

function playbackOverlayCanvas(dstContext, srcCanvas)
{
	var mc = playbackModeCfg;

	dstContext.drawImage(srcCanvas, 0, 0, mc.imgW, mc.imgH, 0, 0, mc.imgW, mc.imgH);
}

function playbackImageProcess()
{
	var mc = playbackModeCfg;

	playbackOverlayCanvas(mc.imageProcessContext, mc.baseImageCanvas);

	playbackRedoUndo();
}

function playbackRedoUndo()
{
	var mc = playbackModeCfg;

	if ( mc.reqUndo )
	{
		mc.reqUndo = false;

		fcDrawingUndo(mc);
	}
	else if ( mc.reqRedo )
	{
		mc.reqRedo = false;

		fcDrawingRedo(mc);
	}
	else
	{
		playbackMergeDrawing();
	}
}

function playbackMergeDrawing()
{
	var mc = playbackModeCfg;

	if ( mc.zoomRatio == 1 )
	{
		playbackOverlayCanvas(mc.combineContext, mc.imageProcessCanvas);
	}
	else
	{
		mc.combineContext.drawImage(mc.imageProcessCanvas, mc.zoomX, mc.zoomY, mc.zoomW, mc.zoomH, 0, 0, mc.imgW, mc.imgH);
	}

	if ( mc.mergeTempDraw )
	{
		mc.mergeTempDraw = false;
		
		playbackOverlayCanvas(mc.drawingContext, mc.tempDrawingCanvas);

		fcPushDrawingCanvas(mc);
	}

	playbackOverlayCanvas(mc.combineContext, mc.drawingCanvas);

	playbackMergeTempDrawing();
}

function playbackMergeTempDrawing()
{
	var mc = playbackModeCfg;

	if ( mc.hasTempDraw )
	{
		playbackOverlayCanvas(mc.combineContext, mc.tempDrawingCanvas);
	}
	
	playbackDisplay();
}

function playbackDisplay()
{
	var mc = playbackModeCfg;

	if ( fcIsModeActive(mc) )
	{
		fcUpdateImage();
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// canvas control
///////////////////////////////////////////////////////////////////////////////////////////

function playbackCanvasControl(state)
{
	var mc = playbackModeCfg;

	switch ( state )
	{
		case DrawState.start:
			return false;
			break;
			
		case DrawState.move:
			mc.hasTempDraw = true;
			break;
			
		case DrawState.end:
			if ( false == appFC.drawText )
			{
				mc.hasTempDraw = false;
				mc.mergeTempDraw = true;
			}
			break;

		case DrawState.undo:
			if ( mc.canvasStep > 0 )
			{
				mc.reqUndo = true;
			}
			break;

		case DrawState.redo:
			if ( mc.canvasStep < (mc.canvasArray.length - 1) )
			{
				mc.reqRedo = true;
			}
			break;
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////////////////
// information bar
///////////////////////////////////////////////////////////////////////////////////////////

function playbackCreateInfoBar()
{
	var id = 'playbackInfoBar';
	var eleId = '#' + id;

	// create base element

	createSpanElement('uiArea', id);

	var css =
	{
		top: 'calc(100% - ' + appCfg.toolBarIconH + 'px)',
		left: appCfg.toolBarIconW + 'px',
		height: appBaseCfg.iconH + 'px',
		width: 'calc(100% - ' + (2*appCfg.toolBarIconW) + 'px)',
		background: appCfg.toolBarColorLeave,
		opacity: appCfg.toolBarOpacityLeave,
		position: 'absolute',
	};

	$(eleId).css(css);

	showElement(id, false);

	// create info display element

	createLabelElement(id, 'pbFileInfo');

	id = 'pbFileInfo';
	eleId = '#' + id;

	css.top = (appBaseCfg.iconH - appCfg.pbInfoH)/2 + 'px';
	css.left = 'calc(50% - ' + (appCfg.pbInfoW/2) + 'px)';
	css.height = appCfg.pbInfoH;
	css.width = appCfg.pbInfoW;

	$(eleId).css(css);

	$(eleId).css('line-height', appCfg.pbInfoH + 'px');
	$(eleId).css('font-size', appCfg.pbInfoFontSize);
	$(eleId).css('font-weight', appCfg.pbInfoFontWWeight);
	$(eleId).css('color', appCfg.pbInfoColor);
	$(eleId).css('text-align', 'center');
}

function playbackUpdateFileInfoElement(text)
{
	var id;

	id = document.getElementById('pbFileInfo');

	fcUpdateElementText(id, text);
}

///////////////////////////////////////////////////////////////////////////////////////////
// delete thumbnail file
///////////////////////////////////////////////////////////////////////////////////////////

function playbackDeletConfirmDlgClickHandler(isYes)
{
	var eleId;
	
	fcCloseActiveDlg();

	if ( isYes )
	{
		eleId = $('img.selected, video.selected');

		galleryDeleteFiles(eleId);
	}
}

function playbackDeleteConfirmDlgOpen()
{
	setConfirmDlgClickHandler(playbackDeletConfirmDlgClickHandler);

	setConfirmDlgTip("btnDelete", "btnCancel");

	showConfirmDlg(true);
}

function playbackDeleteConfirmDlgClose()
{
	showConfirmDlg(false);
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// mode config
///////////////////////////////////////////////////////////////////////////////////////////

var drawingBoardModeCfg =
{
	baseId: 'drawingArea',
		
	lToolBar: 'drawingBoardToolBar',
	rToolBar: 'drawingToolBar',
	bToolBar: 'drawingCfgArea',

	taskCtrl: drawingBoardModeTaskCtrl,
	start: drawingBoardModeStart,
	init: drawingBoardModeInit,
	onResize: drawingBoardModeOnResize,
	canvasCtrl: drawingBoardCanvasControl,
	imageProcess: drawingBoardImageProcess,
	
	imgW: appCfg.defDrawingBoardW,
	imgH: appCfg.defDrawingBoardH,

	hasTempDraw: false,
	mergeTempDraw: false,
	
	reqUndo: false,
	reqRedo: false,
};


///////////////////////////////////////////////////////////////////////////////////////////
// flow control
///////////////////////////////////////////////////////////////////////////////////////////

function drawingBoardModeTaskCtrl(isEnter)
{
	if ( isEnter )
	{
		//console.log("drawingBoard load, nothing to do");
	}
	else
	{
		displayPictureWinClose();
	}
}

function drawingBoardModeInit()
{
	var mc  = drawingBoardModeCfg;

	getCanvasGroupContext(mc);

	fcInitCanvasArray(mc);

	mc.baseImageContext.rect(0, 0, mc.imgW, mc.imgH);
	mc.baseImageContext.fillStyle = appCfg.drawingBoardBgColor;
	mc.baseImageContext.fill();

	mc.combineContext.drawImage(mc.baseImageCanvas, 0, 0, mc.imgW, mc.imgH, 0, 0, mc.imgW, mc.imgH);

	fcShowMode(mc, false);
}

function drawingBoardModeStart()
{
	drawingBoardModeOnResize();
}

function drawingBoardModeOnResize()
{
	fcUpdateImage();
}

///////////////////////////////////////////////////////////////////////////////////////////
// tool bar
///////////////////////////////////////////////////////////////////////////////////////////

function drawingBoardToolBarOnClick(event)
{
	var mc  = drawingBoardModeCfg;

	switch ( event.target.id )
	{
		case "dbbtn_undo":

			fcCanvasCtrl(DrawState.undo);

			break;

		case "dbbtn_redo":

			fcCanvasCtrl(DrawState.redo);

			break;

		case "dbbtn_importPic":

			lsSelectFilesDlg();
			
			break;

		case "dbbtn_savePlayback":

			fcSaveCanvasToFile(mc['combineCanvas']);

			break;
	}

	toolBarOnMouseClick(event);
}


///////////////////////////////////////////////////////////////////////////////////////////
// canvas
///////////////////////////////////////////////////////////////////////////////////////////

function drawingBoardOverlayCanvas(dstContext, srcCanvas)
{
	var mc = drawingBoardModeCfg;

	dstContext.drawImage(srcCanvas, 0, 0, mc.imgW, mc.imgH, 0, 0, mc.imgW, mc.imgH);
}

function drawingBoardImageProcess()
{
	var mc = drawingBoardModeCfg;

	drawingBoardOverlayCanvas(mc.imageProcessContext, mc.baseImageCanvas);

	drawingBoardRedoUndo();
}

function drawingBoardRedoUndo()
{
	var mc = drawingBoardModeCfg;

	if ( mc.reqUndo )
	{
		mc.reqUndo = false;

		fcDrawingUndo(mc);
	}
	else if ( mc.reqRedo )
	{
		mc.reqRedo = false;

		fcDrawingRedo(mc);
	}
	else
	{
		drawingBoardMergeDrawing();
	}
}

function drawingBoardMergeDrawing()
{
	var mc = drawingBoardModeCfg;

	drawingBoardOverlayCanvas(mc.combineContext, mc.imageProcessCanvas);

	if ( mc.mergeTempDraw )
	{
		mc.mergeTempDraw = false;
		
		drawingBoardOverlayCanvas(mc.drawingContext, mc.tempDrawingCanvas);

		fcPushDrawingCanvas(mc);
	}

	drawingBoardOverlayCanvas(mc.combineContext, mc.drawingCanvas);

	drawingBoardMergeTempDrawing();
}

function drawingBoardMergeTempDrawing()
{
	var mc = drawingBoardModeCfg;

	if ( mc.hasTempDraw )
	{
		drawingBoardOverlayCanvas(mc.combineContext, mc.tempDrawingCanvas);
	}
	
	drawingBoardDisplay();
}

function drawingBoardDisplay()
{
	var mc = drawingBoardModeCfg;

	if ( fcIsModeActive(mc) )
	{
		fcUpdateImage();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// canvas control
///////////////////////////////////////////////////////////////////////////////////////////

function drawingBoardCanvasControl(state)
{
	var mc = drawingBoardModeCfg;

	switch ( state )
	{
		case DrawState.start:
			return false;
			break;
			
		case DrawState.move:
			mc.hasTempDraw = true;
			break;
			
		case DrawState.end:
			if ( false == appFC.drawText )
			{
				mc.hasTempDraw = false;
				mc.mergeTempDraw = true;
			}
			break;

		case DrawState.undo:
			if ( mc.canvasStep > 0 )
			{
				mc.reqUndo = true;
			}
			break;

		case DrawState.redo:
			if ( mc.canvasStep < (mc.canvasArray.length - 1) )
			{
				mc.reqRedo = true;
			}
			break;
	}

	return false;
}



'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// control data
///////////////////////////////////////////////////////////////////////////////////////////

var appFC =
{
	appVersion: '20180904-1',

	curMode: undefined,

	showToolBar: true,

	imgW: undefined,
	imgH: undefined,

	reqImgW: undefined,
	reqImgH: undefined,

	baseCanvas: undefined,
	baseContext: undefined,
	
	drawCanvas: undefined,
	drawContext: undefined,

	drawStyle: undefined,
	eraserStyle: undefined,
	clearStyle: 'rgba(0,0,0,0)',
	
	opacity: undefined,
	lineWidth: undefined,

	colorHex: undefined,
	colorR: undefined,
	colorG: undefined,
	colorB: undefined,

	drawStartX: undefined,
	drawStartY: undefined,
	drawCurX: undefined,
	drawCurY: undefined,
	drawLastX: undefined,
	drawLastY: undefined,
	drawing: undefined,
	drawingMove: false,
	fnDraw: undefined,

	activeDlg: false,
	activeDlgCloseFn: false,

	previewBrightnessAdj: 0,
	previewWhiteBalanceAdj: 0,
	previewRotate:0,

	fontFamily: "Arial",
	fontStyle: "Normal",
	fontSize: 12,

	fontBold: false,
	fontItalic: false,

	drawText: false,

	drawFontStyle: undefined,

	thumbnailCnt: 0,
	thumbnailBuild: false,

	capturing: false,
	lastSaveFilename: "",

	ctrlPress: false,
	
	showSysInfo: false,

	frameRateInit: false,
	frameCnt: 16,
	frameSlotIdx: 0,	
	frameInterval: [],
	frameLastTime: 0,
	frameRate: 0,

	thumbFileCnt: 0,
	thumbSelectedFileCnt: 0,

	capturePeriodRun: false,
	capturePeriodSec: appCfg.pcdCapturePeriodSec,
	captureIntervalHour: appCfg.pcdCaptureIntervalHour,
	capturePeriodTotalCnt: 0,
	capturePeriodCurCnt: 0,
	capturePeriodMs: 0,

	resizeDelayMs: appBaseCfg.resizeDelayMs,

	ipwRadiusExpandRatio: 1,
	ipwExtraSpace: 0,

	panMove: false,
	panMoveX: 0,
	panMoveY: 0,

	drawRegularShape: false,

	curVideoResolutionList: VideoResolutionList,
	reqVideoResolutionList: false,
	
	idVideoResolutionList: 0,

	checkVideoResolution: false,
};


///////////////////////////////////////////////////////////////////////////////////////////
// tool bar
///////////////////////////////////////////////////////////////////////////////////////////

function fcShowToolBar(mode)
{
	//var sysToolBar = document.getElementById("sysToolBar");		
	var titleBar = document.getElementById("appTitle");
	if (titleBar) showToolBar("appTitle", mode);
	
	showToolBar("sys_close", mode);
	showToolBar("sys_fullScreen", mode);
	showToolBar("sys_smallScreen", mode);
	showToolBar("bottomToolBarData", mode);
	showToolBar("modeToolBar", mode);
	if ( typeof appFC.curMode.lToolBar != "undefined" ) showToolBar(appFC.curMode.lToolBar, mode);
	if ( typeof appFC.curMode.rToolBar != "undefined" ) showToolBar(appFC.curMode.rToolBar, mode);
	if ( typeof appFC.curMode.bToolBar != "undefined" ) showToolBar(appFC.curMode.bToolBar, mode);
}

function fcToogleToolBar()
{
	appFC.showToolBar = !appFC.showToolBar;

	fcShowToolBar(appFC.showToolBar);
}


///////////////////////////////////////////////////////////////////////////////////////////
// mode
///////////////////////////////////////////////////////////////////////////////////////////

function fcSwitchMode(modeCfg)
{
	if ( appFC.curMode )
	{
		fcShowToolBar(false);

		if ( appFC.activeDlgCloseFn ) appFC.activeDlgCloseFn();
		fcResetActiveDlg();

		if ( appFC.curMode.taskCtrl ) appFC.curMode.taskCtrl(false);

		fcShowMode(appFC.curMode, false);
	}

	appFC.curMode = modeCfg;

	if ( appFC.curMode.taskCtrl ) appFC.curMode.taskCtrl(true);

	fcUpdateImgSize(appFC.curMode.imgW, appFC.curMode.imgH);

	adjImageDisplayArea();
	fcUpdateSysInfoWin();
	fcUpdateSysInfoData();

	if ( appFC.curMode.start ) appFC.curMode.start();

	appFC.baseCanvas = appFC.curMode.drawingCanvas;
	appFC.baseContext = appFC.curMode.drawingContext;
	appFC.drawCanvas = appFC.curMode.tempDrawingCanvas;
	appFC.drawContext = appFC.curMode.tempDrawingContext;
	appFC.imgW = appFC.curMode.imgW;
	appFC.imgH = appFC.curMode.imgH;

	fcShowToolBar(appFC.showToolBar);

	fcShowMode(appFC.curMode, true);
}

function fcIsModeActive(modeCfg)
{
	return modeCfg === appFC.curMode;
}

function fcShowMode(modeCfg, mode)
{
	showElement(modeCfg.baseId, mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// image
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateImgSize(w, h)
{
	appFC.imgW = w;
	appFC.imgH = h;
}

function fcUpdateImage()
{
	var cm = appFC.curMode;

	
	imageDisplayContext.drawImage(cm.combineCanvas, 0, 0, cm.imgW, cm.imgH, 0, 0, dispW, dispH);
}


///////////////////////////////////////////////////////////////////////////////////////////
// flow
///////////////////////////////////////////////////////////////////////////////////////////

function fcOnResize()
{
	if ( appFC.curMode )
	{
		if ( appFC.curMode.onResize ) appFC.curMode.onResize();
	}

	fcUpdateSysInfoWin();
}

function fcInit()
{
	$('#displayCanvas')
		.on('mousedown', fcOnMouseDown)
		.on('mousemove', fcOnMouseMove)
		.on('mouseup', fcOnMouseUp)
		.on('mouseout', fcOnMouseOut)
		.on('touchstart', fcOnTouchStart)
		.on('touchend', fcOnTouchEnd)
		.on('touchmove', fcOnTouchMove)
		.on('touchcancel', fcOnTouchCancel);

	fcInitSysInfoWin();
}


///////////////////////////////////////////////////////////////////////////////////////////
// touch event
///////////////////////////////////////////////////////////////////////////////////////////

function fcOnTouchStart(event)
{
	fcTouchHandler(event);
}

function fcOnTouchEnd(event)
{
	fcTouchHandler(event);
}

function fcOnTouchMove(event)
{
	fcTouchHandler(event);
}

function fcOnTouchCancel(event)
{
	//console.log('fcOnTouchCancel');
}

function fcTouchHandler(event)
{
	if ( !event.originalEvent )
	{
		return;
	}

	var touches = event.originalEvent.changedTouches;
	var first= touches[0];
	var type = "";

	//console.log(event);

	switch ( event.type )
	{
		case "touchstart": type = "mousedown"; break;
		case "touchmove": type = "mousemove"; break;
		case "touchend": type = "mouseup"; break;
		default: return;
	}

	// var simulatedEvent = document.createEvent("MouseEvent");
	// simulatedEvent.initMouseEvent(type, true, true, window, 1,
	// 	first.screenX, first.screenY,
	// 	first.clientX, first.clientY, false,
	// 	false, false, false, 0, null);

	var simulatedEvent = new MouseEvent(type, {
		bubbles: true,
		cancelable: true,
		view: window,
		detail: 1,
		screenX: first.screenX,
		screenY: first.screenY,
		clientX: first.clientX,
		clientY: first.clientY,
		ctrlKey: false,
		altKey: false,
		shiftKey: false,
		metaKey: false,
		button: 0,
		relatedTarget: null
	  });

	first.target.dispatchEvent(simulatedEvent);
	event.preventDefault();
}


///////////////////////////////////////////////////////////////////////////////////////////
// mouse event
///////////////////////////////////////////////////////////////////////////////////////////

var hideIconInterval = null;
var hideIconCountdown = 0;

function setHideIconCountdown()
{	
	if ( fullPhotoMode )
	{
		var btn = document.getElementById('sys_fullPhoto');
		btn.style.backgroundColor = appCfg.toolBarColorLeave;
		btn.style.opacity = appCfg.toolBarOpacityLeave;

		hideIconCountdown = 3;

		if ( !hideIconInterval )
		{
			hideIconInterval = setInterval( function() {

				hideIconCountdown -= 1;

				//console.log("hideIcondown = " + hideIconCountdown);

				if ( hideIconCountdown == 0 )
				{
					if ( fullPhotoMode )
					{
						var btn = document.getElementById('sys_fullPhoto');
						btn.style.backgroundColor = appCfg.toolBarColorHide;
						btn.style.opacity = appCfg.toolBarOpacityHide;
					}

					clearInterval(hideIconInterval);
					hideIconInterval = null;
				}
			}, 1000);
		}
	}
}

function fcOnMouseMove(event)
{
	if ( appFC.fnDraw )
	{
		if ( appFC.drawing )
		{
			fcSetDrawCurPos(event);

			appFC.drawingMove = true;

			appFC.fnDraw(DrawState.move);

			fcCanvasCtrl(DrawState.move);
		}
	}

	//setHideIconCountdown();
}

function fcOnMouseDown(event)
{
	fcMouseEventNotify(UiType.displayCanvas, MouseStatus.click, event);

	if ( appFC.fnDraw && !(IsRecording && !IsPause))
	{
		fcClearDrawingCanvas();
		
		appFC.drawing = true;
		appFC.drawingMove = false;
		
		fcSetDrawStartPos(event);
			
		appFC.fnDraw(DrawState.start);

		fcCanvasCtrl(DrawState.start);
	}
}

function fcOnMouseUp(event)
{
	if ( appFC.fnDraw )
	{
		if ( appFC.drawing )
		{
			fcSetDrawCurPos(event);

			appFC.fnDraw(DrawState.end);

			appFC.drawing = false;

			if ( appFC.drawingMove )
			{
				fcCanvasCtrl(DrawState.end);
			}
		}
	}	
}

function fcOnMouseOut(event)
{
	//console.log('fcOnMouseOut');
}


///////////////////////////////////////////////////////////////////////////////////////////
// canvas
///////////////////////////////////////////////////////////////////////////////////////////

function fcClearCanvas(canvas)
{
	//context.clearRect(0, 0, canvas.width, canvas.height);
	canvas.width += 0;
}

function fcClearDrawingCanvas()
{
	fcClearCanvas(appFC.drawCanvas);
}

function fcPushDrawingCanvas(modeCfg)
{
	if ( modeCfg.canvasArray.length )
	{
		if ( modeCfg.canvasStep != (modeCfg.canvasArray.length - 1) )
		{
			modeCfg.canvasArray.length = modeCfg.canvasStep + 1;
		}
	}
	
	modeCfg.canvasStep += 1;  

	modeCfg.canvasArray.push(modeCfg.drawingCanvas.toDataURL());
}

function fcResetCanvasArray(modeCfg)
{
	// clear backup canvas

	modeCfg.canvasArray.length = 0;

	modeCfg.canvasStep = -1;

	// clear drawing canvas

	fcClearCanvas(modeCfg.drawingCanvas);

	// save clean canvas

	fcPushDrawingCanvas(modeCfg);
}

function fcInitCanvasArray(modeCfg)
{
	modeCfg.canvasArray = new Array();

	fcResetCanvasArray(modeCfg);
}

function fcShowDisplayCanvas(mode)
{
	showElement('displayCanvas', mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// drawing
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateDrawStyle()
{
	appFC.drawStyle = 'rgba('+ appFC.colorR + ', ' + appFC.colorG + ', ' + appFC.colorB + ', ' + appFC.opacity + ')';

	appFC.eraserStyle = 'rgba('+ appFC.colorR + ', ' + appFC.colorG + ', ' + appFC.colorB + ', ' + appCfg.eraserOpacity + ')';

	//console.log(appFC.drawStyle);
}

function fcDrawConvertRatio()
{
	return appFC.imgW / dispW;
}

function fcSetDrawStartPos(event)
{
	var ratio = fcDrawConvertRatio();

	appFC.drawStartX = event.offsetX * ratio;
	appFC.drawStartY = event.offsetY * ratio;

	appFC.drawStartX = Math.floor(appFC.drawStartX*10)/10;
	appFC.drawStartY = Math.floor(appFC.drawStartY*10)/10;

	appFC.drawCurX = appFC.drawLastX = appFC.drawStartX;
	appFC.drawCurY = appFC.drawLastY = appFC.drawStartY;
}

function fcSetDrawCurPos(event)
{
	var ratio = fcDrawConvertRatio();

	appFC.drawLastX = appFC.drawCurX;
	appFC.drawLastY = appFC.drawCurY;

	appFC.drawCurX = event.offsetX * ratio;
	appFC.drawCurY = event.offsetY * ratio;

	appFC.drawCurX = Math.floor(appFC.drawCurX*10)/10;
	appFC.drawCurY = Math.floor(appFC.drawCurY*10)/10;
}

function fcDumpDrawPos()
{
	console.log("start : " + appFC.drawStartX + ", "+ appFC.drawStartY);
	console.log("cur : " + appFC.drawCurX + ", "+ appFC.drawCurY);
	console.log("last : " + appFC.drawLastX + ", "+ appFC.drawLastY);
}

///////////////////////////////////////////////////////////////////////////////////////////
// drawing canvas control 
///////////////////////////////////////////////////////////////////////////////////////////

function fcCanvasCtrl(state)
{
	var mc = appFC.curMode;
	
	if ( mc.canvasCtrl )
	{
		if ( true == mc.canvasCtrl(state) )
		{
			fcUpdateSysInfoData();
			return;
		}
	}

	switch ( state )
	{
		case DrawState.start:
			break;
			
		case DrawState.move:
			if ( mc.imageProcess ) mc.imageProcess();
			break;
			
		case DrawState.end:
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.undo:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.redo:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.mirror:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.flip:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.brightness:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.whiteBalance:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.zoom:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;

		case DrawState.pan:	
			if ( mc.imageProcess ) mc.imageProcess();
			break;
	}

	fcUpdateSysInfoData();
}

function fcLoadCanvasFromArray(mc)
{
	var storeCanvas = new Image();

	storeCanvas.src = mc.canvasArray[mc.canvasStep];	   
	storeCanvas.onload = function() {
		fcClearCanvas(mc.drawingCanvas);
			mc.drawingContext.drawImage(storeCanvas, 0, 0, mc.imgW, mc.imgH);
			if ( mc.imageProcess ) mc.imageProcess();
		}
}

function fcDrawingUndo(mc)
{
	if ( mc.canvasStep > 0 )
	{
		mc.canvasStep -= 1;

		fcLoadCanvasFromArray(mc);
	}
}

function fcDrawingRedo(mc)
{
	if ( mc.canvasStep < (mc.canvasArray.length - 1) )
	{
		mc.canvasStep += 1;

		fcLoadCanvasFromArray(mc);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// dialog control 
///////////////////////////////////////////////////////////////////////////////////////////

function fcCloseActiveDlg()
{
	if ( appFC.activeDlgCloseFn ) appFC.activeDlgCloseFn();

	fcResetActiveDlg();
}

function fcResetActiveDlg()
{
	appFC.activeDlgCloseFn = false;

	appFC.activeDlg = false;

	$("#configDlg").hide();
	removeCropBox();
	//$("svg").remove();
}

function fcToogleDlg(eleId, openFn, closeFn)
{
	if ( appFC.activeDlgCloseFn )
	{
		appFC.activeDlgCloseFn();

		appFC.activeDlgCloseFn = false;
	}
	
	if ( appFC.activeDlg != eleId )
	{
		appFC.activeDlg = eleId

		if ( closeFn ) appFC.activeDlgCloseFn = closeFn;

		if ( openFn ) openFn();
	}
	else
	{
		appFC.activeDlg = false;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// font 
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateFontCfg()
{
	var eleId;
	var idx;

	if ( 1 === appCfg.disableDrawTextSetting )
	{
		return;
	}

	eleId = document.getElementById('sdfontFamily');
	idx = eleId.selectedIndex;
	appFC.fontFamily = fontFamilyData[idx][FontField.value];

	eleId = document.getElementById('fontStyle');
	idx = eleId.selectedIndex;
	appFC.fontStyle = fontStyleData[idx][FontField.value];

	eleId = document.getElementById('fontSize');
	idx = eleId.selectedIndex;
	appFC.fontSize = fontSizeData[idx][FontField.value];
}

///////////////////////////////////////////////////////////////////////////////////////////
// base image resolution change 
///////////////////////////////////////////////////////////////////////////////////////////
function fcChangeBaseImageSize(w, h)
{
	var mc = appFC.curMode;

	if ( mc.chgBaseImgCtrl ) mc.chgBaseImgCtrl(BaseImgChg.before, w, h);

	resetCanvasGroupSize(mc, w, h);
	fcClearDrawingCanvas();
	fcResetCanvasArray(mc);

	appFC.imgW = w;
	appFC.imgH = h;
	adjImageDisplayArea();
	fcUpdateSysInfoWin();
	fcUpdateSysInfoData();
	
	if ( mc.chgBaseImgCtrl ) mc.chgBaseImgCtrl(BaseImgChg.after, w, h);

	fcUpdatePanWin();
	fcUpdateCropWin();
}

///////////////////////////////////////////////////////////////////////////////////////////
// UI mouse event notify 
///////////////////////////////////////////////////////////////////////////////////////////

function fcMouseEventNotify(uiType, mouseEventType, mouseEvent)
{
	switch ( mouseEventType )
	{
		case MouseStatus.click:

			//console.log("UI type " + uiType + " click, id = " + mouseEvent.target.id);
			displayPictureWinClose();
			
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// save canvas to internal storage   
///////////////////////////////////////////////////////////////////////////////////////////

function fcGetFilenameByDateTime(extension)
{
	var dt = new Date();
	
	var year  = dt.getFullYear();
	var month = dt.getMonth() + 1;
	var date  = dt.getDate();
	var hour  = dt.getHours();
	var min   = dt.getMinutes();
	var sec   = dt.getSeconds();
	var ms    = dt.getMilliseconds();
	
	if ( month < 10 ) month = "0" + month;
	if ( date < 10 ) date = "0" + date;	 
	if ( hour < 10 ) hour = "0" + hour;
	if ( min < 10 ) min = "0" + min;
	if ( sec < 10 ) sec = "0" + sec;

	if ( ms < 10 ) ms = "00" + ms;
	else if ( ms < 99 ) ms = "0" + ms;

	var filename;

	filename = year + month + date + '-' + hour + min + sec + '-' + ms + '.' + extension;

	return filename;
}

function fcSaveCanvasToFile(canvas)
{
	var filename = fcGetFilenameByDateTime('jpg');

	if ( appFC.capturing ) return false;
	if ( appFC.lastSaveFilename == filename ) return false;

	appFC.capturing = true;
	appFC.lastSaveFilename = filename;
	
	canvas.toBlob(
		function(blob)
		{
			appFs.root.getFile(
				filename,
				{create: true},
				function(fileEntry)
				{
					console.log(fileEntry);
					fileEntry.createWriter(
						function(fileWriter)
						{
							fileWriter.onwriteend = function()
							{
								//console.log(fileEntry);
								//reflesh all for seq
								refleshGalleryFillThumbnailDiv();
								//appFC.thumbnailCnt += 1;
								//galleryCreateThumbnailDiv(fileEntry, appFC.thumbnailCnt);

								appFC.capturing = false;
							};
							
							fileWriter.write(blob);
						}
					);
				}
			);
		},
		'image/jpeg',
		1.0
	);

	return true;	
}

///////////////////////////////////////////////////////////////////////////////////////////
// key down handler    
///////////////////////////////////////////////////////////////////////////////////////////

function fcOnKeyDown(event)
{
	if ( "Control" == event.key )
	{
		appFC.ctrlPress = true;

		return;
	}

	switch ( event.key )
	{
		case "I":
		case "i":
			if ( appFC.ctrlPress )
			{
				fcToggleSysInfo();
			}
			break;
	}

	appFC.ctrlPress = false;
}

///////////////////////////////////////////////////////////////////////////////////////////
// system info    
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateSysInfoWin()
{
	//console.log("UpdateSysInfoWin");
	var css =
	{
		position: 'absolute',
		'margin-left': '0px',
		background: appBaseCfg.ColorLeave,
		opacity: appBaseCfg.OpacityLeave,
	};

	var x, y;
	var w, h;
	
	w = appCfg.sysInfoWinW;
	h = appCfg.sysInfoWinH;

	x = Math.min((winW - appCfg.toolBarIconW), (dispX + dispW));
	x = x - w;
	y = Math.min((dispY + dispH), (winH - appCfg.drawSetH));

	css.width = w + 'px';
	css.height = h + 'px';
	css.left = (x - appCfg.sysInfoGap) + 'px';
	css.top = (y - appCfg.sysInfoGap - h) + 'px';

	$('#sysInfoWin').css(css);

	var obj = document.getElementById("bottomToolBarData");
	if (obj)
	{
		var pos = (winW / 2 - 30);
		if ( pos < 320 )
			pos = 320;
		obj.style.left = pos + 'px';
		//obj.style.width = '60px';
	}

	obj = document.getElementById("recordtime");
	if (obj && IsRecording)
		obj.style.right = (document.documentElement.clientWidth/2 - 30)+"px";
}

function fcInitSysInfoWin()
{
	var idx = 0;
	
	createSpanElement('uiArea', 'sysInfoWin');

	fcCreateSysInfoElement('appVersion', idx++);
	fcCreateSysInfoElement('cfgVersion', idx++);
	fcCreateSysInfoElement('imgSize', idx++);
	fcCreateSysInfoElement('frmRate', idx++);
	fcCreateSysInfoElement('redoundo', idx++);

	fcShowSysInfo(false);
}

function fcCreateSysInfoElement(subId, idx)
{
	var id = 'sysInfoWin' + '-' + subId;

	createLabelElement('sysInfoWin', id);

	var css =
	{
		position: 'absolute',
		'margin-left': '0px',
	};

	css.left = appCfg.sysInfoGap + 'px';
	css.top = (appCfg.sysInfoGap + idx*appCfg.sysInfoH) + 'px';
	css.height = appCfg.sysInfoH + 'px';
	css.width = (appCfg.sysInfoWinW - 2*appCfg.sysInfoGap) + 'px';

	$('#' + id).css(css);
}

function fcUpdateSysInfoElement(subId, text)
{
	var id;

	id = document.getElementById('sysInfoWin' + '-' + subId);

	fcUpdateElementText(id, text);
}

function fcUpdateSysInfoImgSize()
{
	var text = appFC.imgW + 'x' + appFC.imgH;

	fcUpdateSysInfoElement('imgSize', text);
}

function fcUpdateSysInfoFrameRate()
{
	fcUpdateSysInfoElement('frmRate', appFC.frameRate);
}

function fcUpdateSysInfoCanvasArray()
{
	var cm = appFC.curMode;

	var text = (cm.canvasStep + 1) + ' / ' + cm.canvasArray.length;

	fcUpdateSysInfoElement('redoundo', text);
}

function fcUpdateSysInfoAppVersion()
{
	fcUpdateSysInfoElement('appVersion', appFC.appVersion);
}

function fcUpdateSysInfoCfgVersion()
{
	fcUpdateSysInfoElement('cfgVersion', appCfg.cfgVersion);
}

function fcUpdateSysInfoData()
{
	fcUpdateSysInfoAppVersion();

	fcUpdateSysInfoCfgVersion();
	
	fcUpdateSysInfoImgSize();

	fcUpdateSysInfoFrameRate();

	fcUpdateSysInfoCanvasArray();
}

function fcToggleSysInfo()
{
	appFC.showSysInfo = !appFC.showSysInfo;

	fcShowSysInfo(appFC.showSysInfo);
}

function fcShowSysInfo(mode)
{
	if ( mode )
	{
		fcUpdateSysInfoWin();
		fcUpdateSysInfoData();
	}
	
	showElement('sysInfoWin', mode);
}

///////////////////////////////////////////////////////////////////////////////////////////
// icon    
///////////////////////////////////////////////////////////////////////////////////////////

function fcGetDefIconPath(pic)
{
	return "url('css/images/" + "icon" + "/" + pic + "')";
}

///////////////////////////////////////////////////////////////////////////////////////////
// element text    
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateElementText(id, text)
{
	var txt;

	txt = document.createTextNode(text);
	
	id.innerText = txt.textContent;
}

///////////////////////////////////////////////////////////////////////////////////////////
// zoom
///////////////////////////////////////////////////////////////////////////////////////////

function fcZoomDlgOpen()
{
	showZoomDlg(true);
}

function fcZoomDlgClose()
{
	showZoomDlg(false);
}

function fcUpdateZoomRatio(ratio, pos)
{
 	var startX, startY;
 	var centerX, centerY;
	var w, h;
	var mc = appFC.curMode;

	centerX = mc.zoomX + mc.zoomW/2;
	centerY = mc.zoomY + mc.zoomH/2;

	w = mc.imgW / ratio;
	h = mc.imgH / ratio;

	startX = centerX - w/2;
	startY = centerY - h/2;

	startX = Math.max(startX, 0);
	startY = Math.max(startY, 0);

	startX = Math.min(startX, (mc.imgW - w));
	startY = Math.min(startY, (mc.imgH - h));

	mc.zoomRatio = ratio;
	mc.zoomX = startX;
	mc.zoomY = startY;
	mc.zoomW = w;
	mc.zoomH = h;

	fcCanvasCtrl(DrawState.zoom);

	if ( ratio <= 1 ) fcShowPanWin(false);
	else fcShowPanWin(true);

	fcUpdateCropWin();

	mc.zoomScrollBarPos = pos;
}

///////////////////////////////////////////////////////////////////////////////////////////
// pan window 
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdatePanWin()
{
	var mc = appFC.curMode;
	
	var css =
	{
		position: 'absolute',
		'margin-left': '0px',
		background: appCfg.panWinColor,
		opacity: appCfg.panOpacity,
	};

	var x, y;

	var w = dispW/8;
	var h = dispH/8;

	x = Math.max(appCfg.toolBarIconW, dispX);
	y = Math.min((dispY + dispH), (winH - appCfg.drawSetH));

	css.width = w + 'px';
	css.height = h + 'px';
	css.left = (x + appCfg.panWinGapX) + 'px';
	css.top = (y - appCfg.panWinGapY - h) + 'px';
	
	$('#panWin').css(css);

	css.left = 0;
	css.top = 0;
	css.background = 'undefined';
	css.opacity = 1;
	$('#cropWin').css(css);
	updateCanvasSize('cropWin', w, h)

	mc.panWinW = w;
	mc.panWinH = h;
}

function fcUpdateCropWin()
{
	var mc = appFC.curMode;
	var x, y, w, h;
	var ratio = mc.panWinW / mc.imgW;
	var canvas;
	var ctx;

	x = mc.zoomX * ratio;
	y = mc.zoomY * ratio;
	w = mc.zoomW * ratio;
	h = mc.zoomH * ratio;

	canvas = document.getElementById('cropWin');
	ctx = canvas.getContext('2d');

	fcClearCanvas(canvas);

	ctx.fillStyle = appCfg.panCropColor;
	ctx.fillRect(x, y, w, h);
}

function fcShowPanWin(mode)
{
	showElement('panWin', mode);
	showElement('cropWin', mode);
}

function fcAdjCropWin(offsetX, offsetY)
{
	var mc = appFC.curMode;
	
	var ratio = mc.imgW / mc.panWinW;

	mc.zoomX += (offsetX - appFC.panMoveX)*ratio;
	mc.zoomY += (offsetY - appFC.panMoveY)*ratio;

	mc.zoomX = Math.max(mc.zoomX, 0);
	mc.zoomY = Math.max(mc.zoomY, 0);

	if ( (mc.zoomX + mc.zoomW) > mc.imgW ) mc.zoomX = mc.imgW - mc.zoomW;
	if ( (mc.zoomY + mc.zoomH) > mc.imgH ) mc.zoomY = mc.imgH - mc.zoomH;

	fcUpdateCropWin();

	fcCanvasCtrl(DrawState.pan);

	appFC.panMoveX = offsetX;
	appFC.panMoveY = offsetY;
}

function fcInitCropWinEvent()
{
	$('#cropWin')
		.on('mousedown', function (event) {
			if ( fcCropAreaDetect(event.offsetX, event.offsetY) )
			{
				appFC.panMove = true;
				appFC.panMoveX = event.offsetX;
				appFC.panMoveY = event.offsetY;
			}
		})
		.on('mousemove', function (event) {
			if ( appFC.panMove )
			{
				fcAdjCropWin(event.offsetX, event.offsetY);
			}
		})
		.on('mouseup', function (event) {
			appFC.panMove = false;
		})
		.on('mouseout', function (event) {
			appFC.panMove = false;
		})
		.on('touchstart', function(event) {
			fcTouchHandler(event);
		})
		.on('touchmove', function(event) {
			fcTouchHandler(event);
		})
		.on('touchend',function() {
			fcTouchHandler(event);
		})
		.on('touchcancel',function() {
			fcTouchHandler(event);
		});

	function fcCropAreaDetect(curX, curY)
	{
		var mc = appFC.curMode;
		var x, y, w, h;
		var ratio = mc.panWinW / mc.imgW;

		x = mc.zoomX * ratio;
		y = mc.zoomY * ratio;
		w = mc.zoomW * ratio;
		h = mc.zoomH * ratio;

		if ( ((curX >= x) && (curX <= (x+w))) && ((curY >= y) && (curY <= (y+h))) ) return true;

		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// update video resolution 
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateVideoResolution()
{
	var eleId;
	var idx;

	if ( appFC.reqVideoResolutionList !== appFC.curVideoResolutionList )
	{
		appFC.curVideoResolutionList = appFC.reqVideoResolutionList;

		appFC.reqVideoResolutionList = false;
		
		updateVideoResolutionElement(appFC.curVideoResolutionList);

		eleId = document.getElementById('sdVideoRsoScrollbar');
		idx = eleId.selectedIndex;

		videoW = appFC.curVideoResolutionList[idx][VrField.w];
		videoH = appFC.curVideoResolutionList[idx][VrField.h];

		if ( fcIsModeActive(previewModeCfg) )
		{
			appFC.checkVideoResolution = true;
		}
	}
}

function fcUpdateSelVideoResolutionIdx(idx)
{
	var i;

	if ( idx <= (appFC.curVideoResolutionList.length - 1) )
	{
		for ( i = 0; i < appFC.curVideoResolutionList.length; i++ )
		{
			appFC.curVideoResolutionList[i][VrField.data] = 0;
		}

		appFC.curVideoResolutionList[idx][VrField.data] = 1;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// multi-language 
///////////////////////////////////////////////////////////////////////////////////////////

function fcUpdateElementTitle(eleId, strId)
{
	var ele;

	ele = document.getElementById(eleId);

	if ( ele )
	{
		ele.title = chrome.i18n.getMessage(strId);
	}
}

function fcUpdateElementHtml(eleId, strId)
{
	var ele;

	ele = document.getElementById(eleId);

	if ( ele )
	{
		$('#' + eleId).html(chrome.i18n.getMessage(strId));
	}
}


'use strict';


///////////////////////////////////////////////////////////////////////////////////////////
// size change
///////////////////////////////////////////////////////////////////////////////////////////

var winW;
var winH;

var titleW;
var titleH;

window.onresize = function(event)
{
	if ( appBaseCfg.resizeDelayMs )
	{
		//console.log(appFC.resizeDelayMs);

		setTimeout(function() {
			updateSize();
			appFC.resizeDelayMs = appBaseCfg.resizeDelayMs;
		}, appFC.resizeDelayMs);
	}
	else
	{
		updateSize();
	}
};

function updateWinSize()
{
	winW = window.innerWidth;
	winH = window.innerHeight;
	
	//console.log('win size = ' + winW + 'x' + winH);
}

function updateSize()
{
	updateWinSize();

	updateTitleBarSize();

	adjImageDisplayArea();

	fcOnResize();

	resizePictureWin();

	resizeConfirmDlg();
}


///////////////////////////////////////////////////////////////////////////////////////////
// init
///////////////////////////////////////////////////////////////////////////////////////////

window.onmousemove = function()
{
	setHideIconCountdown();
}

window.onload = function()
{
	updateSize();

	cfgBody();
	
	initToolBar();
	hideAllToolBars();
	
	initColorBar();
	initScrollBar();
	initScrollBarDlg();
	initSettingDlg();
	initTextInputDlg();
	initPictureWin();
	initConfigDlg();

	preInitAboutDlg();
	initAboutDlg();
	afterInitAboutDlg();
	
	initPeriodicCaptureDlg();
	initPeriodicCaptureInfoBar();
	initConfirmDlg();
	
	cfgTitleBar();

	fcInit();

	previewModeCfg.init();
	playbackModeCfg.init();
	drawingBoardModeCfg.init();

	galleryInit();

	fcSwitchMode(previewModeCfg);

	connectVideoSrc();
	updateVideoStreamFrame();
	intVideoStreamActiveCheck();

	loadTranslatedString();
};

function loadTranslatedString()
{
	var tbl = [
		
		[ "btn_freehand",      "btnFreehand" ],
		[ "btn_arrow",         "btnArrow" ],
		[ "btn_eraser",        "btnEraser" ],
		[ "btn_line",          "btnLine" ],
		[ "btn_rectangleLine", "btnRectangle" ],
		[ "btn_rectangle",     "btnFilledRectangle" ],
		[ "btn_circleLine",    "btnEllipse" ],
		[ "btn_circle",        "btnFilledEllipse" ],
		[ "btn_erase_all",     "btnEraseAll" ],
		[ "btn_text",          "btnText" ],

	    [ "btn_setting",       "btnSetting" ],
	    [ "btn_capture",       "btnCapture" ],
	    [ "btn_freeze",        "btnFreezeLiveView" ],
	    [ "btn_timesave",      "btnIntervalShooting" ],
	    [ "btn_mirrow",        "btnFlipVertical" ],
	    [ "btn_flip",          "btnFlipHorizontal" ],
	    [ "btn_zoom",          "btnZoom" ],
		[ "btn_importPic",     "btnImportImageFile" ],
	    [ "btn_redo",          "btnRedo" ],
		[ "btn_undo",          "btnUndo" ],
		[ "btn_record",        "btnRecord" ],
		[ "btn_whitebalance",  "btnWhiteBalance" ],
		[ "btn_brightness",    "btnBrightness" ],

		[ "mode_about",        "btnAbout" ],
		[ "mode_preview",      "btnLiveView" ],
		[ "mode_playback",     "btnGallery" ],
		[ "mode_drawingBoard", "btnDrawingBoard" ],

		[ "sys_close",         "btnCloseWindow" ],
		[ "sys_fullScreen",    "btnMaxWindow" ],
		[ "sys_fullPhoto",     "btnHideFunctionIcon" ],
		[ "sys_smallScreen",   "btnMinWindow" ],

		[ "btn_saveDisk",      "btnSaveToDisk" ],
		[ "btn_delete",        "btnDelete" ],
		[ "btn_toggleAll",     "btnToogleSelectAll" ],

		[ "ppbtn_savePlayback",         "btnSaveToGallery" ],
		[ "ppbtn_closeAndBackPlayback", "btnBackToGallery" ],
		[ "ppbtn_zoom",                 "btnZoom" ],
		[ "ppbtn_importPic",            "btnImportImageFile" ],
		[ "ppbtn_redo",                 "btnRedo" ],
		[ "ppbtn_undo",                 "btnUndo" ],
		[ "ppbtn_transform",                 "ppbtn_transform" ],		

		[ "dbbtn_savePlayback",         "btnSaveToGallery" ],
		[ "dbbtn_redo",                 "btnRedo" ],
		[ "dbbtn_undo",                 "btnUndo" ],
		[ "dbbtn_importPic",            "btnImportImageFile" ],

		[ "sdVideoRsoIcon",				"textVideoResolution" ],
		[ "sdFontIcon",					"textFont" ],

		[ "periodicCaptureDlg_periodIcon",		"textInterval" ],
		[ "periodicCaptureDlg_intervalIcon",	"textDuration" ],
		[ "periodicCaptureDlg_startIcon",		"btnStart" ],

		[ "periodicCaptureInfoBar",				"btnCancel" ],

		[ "drawingOpacity",		"textTransparency" ],
		[ "drawingLineWidth",	"textWidth" ]
	];

	var i;

	for ( i = 0; i < tbl.length; i++ )
	{
		fcUpdateElementTitle(tbl[i][0], tbl[i][1]);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
// key down handler
///////////////////////////////////////////////////////////////////////////////////////////

document.body.addEventListener('keydown', function (event) {
	fcOnKeyDown(event);
});

///////////////////////////////////////////////////////////////////////////////////////////
// block context menu
///////////////////////////////////////////////////////////////////////////////////////////

(function ()
{
	function stop()
	{
		return false;
	}

	document.oncontextmenu = stop;
})();



