
SET DEL_BMP=Y


DEL app_tmp.js

copy app_org_1.js app_tmp.js

FOR %%a in (config_SmartDc, about_SmartDc, devCfg app_org_2) do copy app_tmp.js /A + %%a.js /A app_tmp.js /A

..\tool\rf app_tmp.js 1 app_org.js
IF '%DEL_BMP%'=='Y' del app_tmp.js

java -jar compiler.jar --js app_org.js --js_output_file app.js
IF '%DEL_BMP%'=='Y' del app_org.js

copy app.js ..\SmartDc\js\app.js
IF '%DEL_BMP%'=='Y' del app.js

pause